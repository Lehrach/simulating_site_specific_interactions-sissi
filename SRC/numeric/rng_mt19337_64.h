//
// Created by michael on 06.04.18.
//

#ifndef SISSI_2_0_RNG_MT19337_64_H
#define SISSI_2_0_RNG_MT19337_64_H

#include "abstract_prng.h"

class MT19937 : public ABSTRACT_PRNG {
private:
    std::mt19937_64 mt1993764;
public:
    // CONSTRUCTORS
    MT19937 () {
        generateSeed();
        mt1993764.seed(seed);
    }

    MT19937 (unsigned long seed_value) {
        Seed(seed_value);
        setSeed(seed);
    };
    // DESTRUCTORS
    virtual ~MT19937() = default;
    // OPERATOR OVERLOADING
    double operator() () {
        mt1993764();
    }
    // reseed with value
    void setSeed(unsigned long seed) {
        mt1993764.seed(seed);
    }
    //return new random number
    const double drawNumber() {
        return static_cast<double>( mt1993764() ) / MAXNUMBER;
    }

};

#endif //SISSI_2_0_RNG_MT19337_64_H
