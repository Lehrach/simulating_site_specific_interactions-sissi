//
// Created by michael on 05.04.18.
//

/*
 *  rng_kiss.c
 *
 *  This is a version of (j)kiss (in C) from:
 *
 * Good Practice in (Pseudo) Random Number Generation for
 * Bioinformatics Applications
 *
 * http://www.cs.ucl.ac.uk/staff/d.jones/GoodPracticeRNG.pdf
 *
 * David Jones, UCL Bioinformatics Group
 * (E-mail: d.jones@cs.ucl.ac.uk)
 * (Last revised May 7th 2010)
 *
 *  GSL/GPL packaged for dieharder by Robert G. Brown 01/06/11
 *
 * David has kindly agreed to make this code available under the GPL so
 * it can be integrated with the dieharder package and/or the Gnu
 * Scientific Library (GSL).
 *
 */
/* Public domain code for JKISS64 RNG - long period KISS RNG producing 64-bit results */


#ifndef SISSI_2_0_JKISS_H
#define SISSI_2_0_JKISS_H

#include <cmath>
#include "abstract_prng.h"

class JKISS2010 : public ABSTRACT_PRNG {
private:
    /* Seed variables */
    uint64_t tmp1, tmp2, z1, c1, z2, c2;
    /* State variable */
    uint64_t t;

public:
    JKISS2010() {
        generateSeed();
        setSeed(seed);
    };

    JKISS2010(unsigned long seed_value) {
        Seed(seed_value);
        setSeed(seed);
    };

    virtual ~JKISS2010() = default;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @brief Set the seed for a new set of random numbers.
     */
    void setSeed(unsigned long seed);

    /**
     * @brief Get a random number between 0.0 and 1.0 (exclusive of the end point values).
     */
    const double drawNumber();
};


#endif //SISSI_2_0_JKISS_H
