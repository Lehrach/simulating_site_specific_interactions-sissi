//
// Created by michael on 16.03.18.
//

#ifndef SISSI_2_0_MATRIX_H
#define SISSI_2_0_MATRIX_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>

#define NEARZERO 1.0e-10

class MATRIX {
private:
    int size; // cardinality of matrix
    std::vector< std::vector<double> > matrix; //the matrix itself as container in container
    double diagonal_sum = 0; // sum of diagonal entries
    double sum_of_row = 0; // ATTENTION DOUBLE USAGE !!!
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
protected:
    /*
     * correct diagonal element as first step of normalization
     * */
    void correct_diagonal_element (int row){
        for (int col=0; col < size; col++){
            matrix[row][row] = 0;
            build_rowsum(row);
            matrix[row][row] = -sum_of_row;
        }
    } // end correct_diagonal_element
    /*
     * build and safe sum of row
     * */
    void build_rowsum (int row){
        sum_of_row = 0;
        for (int col=0; col < size; col++){
            sum_of_row += matrix[row][col];
        }
    } // end build_rowsum
    /*
     * sum  diagonal entries multiplied with corresponding fgrequency
     * */
    void build_diagonal_sum_multiplied_with_frequencies (const std::vector<double> &frequency) {
        diagonal_sum = 0;
        for (int row = 0; row<size; row++) {
            diagonal_sum += std::abs(matrix[row][row] * frequency[row]);
        }
    }
    /*
     * normalize all elemnts to ensure rowsum == zero
     * */
    void normalize_elements (){
        for (int row=0; row<size; row++){
            for (int col=0; col < size; col++){
                matrix[row][col] /= diagonal_sum;
            }
        }
    } // end normalize_diagonal
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public:
    //CONSTRUCTOR DESTRUCTOR
    MATRIX(){}

    MATRIX(int i_size) {
        size = i_size;
        matrix.resize(size);
        for (int i = 0; i < size; ++i)
            matrix[i].resize(size);
    }

    MATRIX(int i_size, double value) {
        size = i_size;
        matrix.resize(size);
        for (int i = 0; i < size; ++i)
            matrix[i].resize(size);
        this->setAllValues(value);
    }

    MATRIX(const MATRIX &copy_matrix){
        MATRIX(copy_matrix.getSize() );
        for (int row=0; row<size; row++){
            for (int col=0; col<size; col++) {
                this->setElement(row, col, copy_matrix.getElement(row,col) );
            }
        }
    } // end copyconst

    virtual ~MATRIX() = default;

    // operator overloading
    double &operator() (int row, int col){
        return matrix[row][col];
    }

    /*
     * summing function to normalize the matrix
     * */
    void normalize_matrix (const std::vector<double> &frequency) {
        for (int row = 0; row<size; row++) {
            correct_diagonal_element(row);
        }
        build_diagonal_sum_multiplied_with_frequencies(frequency);
        normalize_elements(); // by dividing with diagonal sum
    } // end normalize_matrix
    /*
     * prints the matrix containing information to cout with floating precision of 3
     * */
    void print_matrix () {
        std::cout << std::setprecision(3) << std::fixed;
        for (int row=0; row<size; row++){
            for (int col=0; col < size; col++) {
                std::cout << matrix[row][col] << '\t';
            }
            std::cout << std::endl;
        }
        std::cout << std::endl << std::defaultfloat;
    } // end print_matrix

/***********************************************************************************************************************
 * SETTER & GETTER
 **********************************************************************************************************************/
    const int getSize() const{
        return size;
    }
    const double getElement (const int row, const int col) const {
        return matrix[row][col];
    }

    void setElement(int row, int col, double element) {
        matrix[row][col] = element;
    }

    void setAllValues(double element) {
        for (int row=0; row<size; row++){
            for (int col=0; col<size; col++) {
                matrix[row][col] = element;
            }
        }
    } // end setAllValues

};


#endif //SISSI_2_0_MATRIX_H
