//
// Created by michael on 05.04.18.
//


#include "rng_jkiss.h"

void JKISS2010::setSeed(unsigned long seed) {
    tmp1 = std::abs(MODSEED - std::abs(seed));
    tmp1 %= MAXNUMBER;
    tmp2 = seed;

    //numbers come from random
    z1 = 70236873;
    c1 = 134892838;
    z2 = 246989874;
    c2 = 259210234;
} // end setSeed

const double JKISS2010::drawNumber() {
//CNG Generator
    tmp1 = 1490024343005336237ULL * tmp1 + 123456789;

//XORSHIFT Generator
    tmp2 ^= tmp2 << 21;
    tmp2 ^= tmp2 >> 17;
    tmp2 ^= tmp2 << 30; /* Do not set y=0! */

//B64MWC Generator
    t = 4294584393ULL * z1 + c1; c1 = t >> 32; z1 = t;
    t = 4246477509ULL * z2 + c2; c2 = t >> 32; z2 = t;

    double r = static_cast<double>( tmp1 + tmp2 + z1 + (z2 << 32) )/ MAXNUMBER;

    return r;
} // end drawNumber