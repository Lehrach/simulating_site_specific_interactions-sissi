//
// Created by michael on 05.04.18.
//

#ifndef SISSI_2_0_ABSTRACT_PRNG_H
#define SISSI_2_0_ABSTRACT_PRNG_H

#include <chrono>
#include <thread>
#include <limits>
#include <iostream>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc
#include <boost/functional/hash.hpp>

class ABSTRACT_PRNG {
public:
    static const uint64_t MAXNUMBER = std::numeric_limits<uint64_t >::max(); //represents 18446744073709551615;
    static const long MINNUMBER = std::numeric_limits<uint64_t >::min();
    static const long MODSEED = 446744073709551615; // a random number
    uint64_t seed;

    // Constructor ans destructor
    ABSTRACT_PRNG() {
        generateSeed();
    }
    virtual ~ABSTRACT_PRNG() = default;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //operator overloading
    virtual double operator() () {
        return drawNumber();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // draw and discard first rounds of PRNG
    void warm_up(int i_rounds) {
        for (int i = 0; i < i_rounds; ++i) {
            drawNumber();
        } // end for
    }
    //predifinition of function
    virtual const double drawNumber(){};
    /*
     * generate seed value fulfilling major topics for use at cluster and multicore machines
     * */
    void generateSeed() {
    /** cases for generating seeds that are covered:
     * 1. The program is relaunched on the same machine later,                  --> time component
     * 2. Two threads are launched on the same machine at the same time,        --> thread id component
     * 3. The program is launched on two different machines at the same time    --> UUID component / number of cores
     */
        // Universally Unique Identifier (UUID)
        boost::uuids::random_generator generator;
        boost::uuids::uuid uuid = generator();
        //hash UUID
        boost::hash<boost::uuids::uuid> uuid_hasher;
        uint64_t uuid_hash_value = uuid_hasher(uuid);

        //thread ID
        std::thread::id thread_id = std::this_thread::get_id();
        //hash thread ID
        std::hash<std::thread::id> thread_hasher;
        uint64_t thread_id_hash = thread_hasher(thread_id);

        // retrieve time count
        long time = std::chrono::high_resolution_clock::now().time_since_epoch().count();

        //CONCAT BOTH and return
        seed = uuid_hash_value + time + thread_id_hash;
    }; // end generateSeed

    /*
     * writes generated and saved seed value to cout
     * */
    void PrintSeedValue(){
        std::cout << "RNG Seeding value :" << '\t' << seed << std::endl;
    }
    /*
     * returns generated and saved seed value
     * */
    const uint64_t getSeed() const {
        return seed;
    }

    void Seed(uint64_t seed) {
        ABSTRACT_PRNG::seed = seed;
    }

}; // end CLASS ABSTRACT_PRNG

#endif //SISSI_2_0_ABSTRACT_PRNG_H
