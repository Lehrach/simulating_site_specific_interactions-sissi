//
// Created by michael on 01.02.18.
//

#ifndef SISSI_2_0_NEIGHBOURHOOD_H
#define SISSI_2_0_NEIGHBOURHOOD_H

#include <unordered_map>
#include <vector>

class STRUCTURE{
private:
    unsigned int parsed_lenght; // length of structural information
    std::unordered_multimap< int,int > structuralsystem; //connection saving map
    std::vector<int> nk; // number of connections per site
public:
    //constructor
    STRUCTURE() {
        parsed_lenght = 0;
        structuralsystem.clear();
        nk.clear();
    }
    //Destructor
    virtual ~STRUCTURE() = default;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     * add a connection from one position to another, vice versa
     * */
    void add_structure(int from_position, int to_position){
        structuralsystem.insert( {from_position, to_position} );
        structuralsystem.insert( {to_position, from_position} );
    }
    /*
     * returns total number of connections
     * */
    const unsigned long get_size() const {
        return structuralsystem.size();
    }
    /*
     * generate Nk for structural system (saved connections)
     * */
    void InitNk(){
        nk.resize(parsed_lenght, 0);
        for (auto i=0; i<parsed_lenght; i++ ){
            //IF element exist add one to nk
            auto range = structuralsystem.equal_range(i);
            for (auto it = range.first; it != range.second; ++it) {
                nk[i]++;
            } // end for
        } // end for
    } // ens InitNk
    /*
     * returns connection partener for key position - may a second or third connection be asked
     * */
    const int getValue(const int key, int appearance = 1) const {
        auto match = structuralsystem.equal_range(key);
        for (auto it = match.first; it != match.second; ++it) {
            for (auto counter=1; counter<appearance; counter++) it++;
            return it->second;
        }
//        //DEBUG HELP
//        int counter = 1;
//        auto match = structuralsystem.equal_range(key);
//        for (auto it = match.first; it != match.second; ++it) {
//            if (appearance == counter) {
//                return structuralsystem.find(key)->second;
//            } else {
//                counter ++;
//            } // end if else
//        } // end for
    } // end getValue

    /******************************************************************************
 * SETTER AND GETTERS
 * */
    unsigned int getParsed_lenght() const {
        return parsed_lenght;
    }

    void setParsed_lenght(unsigned int parsed_lenght) {
        STRUCTURE::parsed_lenght = parsed_lenght;
    }

    const std::vector<int> &getNk() const {
        return nk;
    }

    const int &getNk(int index) const {
        return nk[index];
    }

    const bool existElement(int index) const {
        if (structuralsystem.count(index) == 0) {
            return false;
        } else {
            return true;
        } // end if else
    }
    /*
     * lists all connections to cout
     * */
    void PrintStructuralSystem() {
        std::cout << structuralsystem.size() << std::endl;
        std::cout << "Connections of the structuralsystem:" << std::endl;
        for (auto i = 0; i < parsed_lenght; i++) {
            std::cout << i << " " << getValue(i) << " " << getValue(i, 2) << std::endl;
        }
    } // end PrintStructuralSystem

}; // end class STRUCTURE

#endif //SISSI_2_0_NEIGHBOURHOOD_H