//
// Created by michael on 16.03.18.
//

#include "simulate.h"

void SIMULATE::count_frequencies(){
    if (collector.getSites()->getNumberOfSequences() == 1){
        collector.getModelstates().count_freuqeuncies_from_sequence( new bpp::BasicSequence( collector.getSites()->getSequence(0) ), collector.getStructure() );
    } else {
        collector.getModelstates().count_freuqeuncies_from_alignment(collector.getSites(), collector.getStructure() );
    } // end if else

    collector.getModelstates().normalize_frequencies();
    if (collector.isPrintNormalizedFrequencies() ){
        collector.getModelstates().print_frequencies();
    }

} // end count_frequencies

void SIMULATE::retrieve_Qk_sk_sk_values(bpp::Sequence* sequence, STRUCTURE &structure) {
    int first_value = 0;
    int second_position = 0;
    int second_value = 0;
    int third_position = 0;
    int third_value = 0;
    //go along sequence and query nk
    for (size_t position=0; position < sequence->size(); position++){
        if (structure.getNk( position ) == 0) {
            first_value = sequence->getValue(position); //check position from mononuk
            Qk_sk_sk_values[position] = submatrix.getMonoNucletidMatrix(first_value,first_value) ; // retrieve QK_sk_sk and pushback for summing up to q(x)

        } else if (structure.getNk( position ) == 1) {
            /*
            * | Index |  0   |  1   |  2   |  3   |||  4   |  5   |  6   |  7   |||  8   |  9   |  10  |  11  |||  12  |  13  |  14  |  15  |
            * | (k,i) |  AA  |  CA  |  GA  |  UA  |||  AC  |  CC  |  GC  |  UC  |||  AG  |  CG  |  GG  |  UG  |||  AU  |  CU  |  GU  |  UU  |
            * |-------|------|------|------|------|+|------|------|------|------|+|------|------|------|------|+|------|------|------|------|
            */
            first_value = sequence->getValue(position);
            second_position = structure.getValue( position ); //get second letter
            second_value = sequence->getValue( second_position );
            Qk_sk_sk_values[position] = submatrix.getDiNucletidMatrix( ((4*first_value) + second_value ) ,
                                                                      ((4*first_value) + second_value ) ) ; //check position from dinuk
        } else if (structure.getNk( position ) == 2) {
            //take position as extra variable for swaping later on
            int first_position = position;
            // get second and third position
            second_position = structure.getValue( position ); //get second letter
            third_position = structure.getValue( position, 2 ); //get third letter

            //sort positions so the lowest position gets first and highest is last
            if (first_position > third_position)
                std::swap(first_position, third_position);
            if (first_position > second_position)
                std::swap(first_position, second_position);
            //Now the smallest element is the first one. Just check the 2-nd and 3-rd
            if (second_position > third_position)
                std::swap(second_position, third_position);

            first_value = sequence->getValue(position);
            second_value = sequence->getValue( second_position );
            third_value = sequence->getValue( third_position );
            int index = (16*third_value) + (4*second_value) + first_value;
            Qk_sk_sk_values[position] = submatrix.getTriNucletidMatrix( index, index ) ; //check position from dinuk
        } else {
            throw std::overflow_error("nk greater than 2 can't retrieve Qk_sk_sk values");
        }// end if
    } // end for

//    // DEBUG HELP Print n Elements with their corresponding Letter Values and index positions and QK sk sk values
//    for (size_t position=0; position < 13 ; position++){
//        if (structure.getNk(position) == 1) {
//            std::cout << "NK = 1 Seq-Val\t"
//                      << sequence->getChar(position) // Sequence element
//                      << '\t' << sequence->getValue(position) // Matrix index
//                      << '\t' << submatrix.getMonoNucletidMatrix(sequence->getValue(position), sequence->getValue(position)) // Qk_sk_sk element
//                      << std::endl;
//        }
//        if (structure.getNk(position) == 2) {
//            second_position = structure.getValue(position); //get second letter
//            first_position = sequence->getValue(position);
//            int second_value = sequence->getValue(second_position);
//            std::cout << "NK = 2 Seq-Val\t"
//                      << sequence->getChar(position) // first Sequence element
//                      << sequence->getChar(second_position) // second Sequence element
//                      << '\t' << (4*second_value) + first_position // Matrix index
//                      << '\t' << first_position << '\t' << second_value // Single int for char
//                      << '\t' << submatrix.getDiNucletidMatrix( ( (4*second_value) + first_position) ,
//                                                                ( (4*second_value) + first_position ) ) // Qk_sk_sk element
//                      << std::endl;
//        }
//    }

} //end retrieve_Qk_sk_sk_values;

void SIMULATE::sum_Qk_sk_sk_values() {
    q_x_ = 0;
    for (auto i:Qk_sk_sk_values) {
        q_x_ += std::abs(i);
    }
} // end sum_Qk_sk_sk_values

void SIMULATE::calculate_Pk_values(std::vector<double> asterix_rates){
    P_k_sum_correction = 0;
    for (auto i = 0; i < asterix_rates.size() ; ++i) {
        P_k_[i] = std::abs(asterix_rates[i]) / q_x_ ;
        P_k_sum_correction += P_k_[i];
    }
} // end calculate_Pk_values

void SIMULATE::draw_site(){
    double dummy=0;
    int counter = 0;
    double randomnumber = randomfactory->drawNumber();

    randomnumber *= P_k_sum_correction; // correction to avoid length errors
    site_k = -1;

    dummy = 0;

    do {
        dummy += P_k_[counter];
        site_k = counter++;
    } while(randomnumber > dummy);

} // end draw_site

int SIMULATE::draw_position() {
    double dummy=0;
    double randomnumber = randomfactory->drawNumber();
    double sum_correction = 0;
    for (auto it = P_xk_y0_.begin(); it != P_xk_y0_.end(); ++it){
        sum_correction += it->second;
    }
    randomnumber *= sum_correction;

    for (auto it = P_xk_y0_.begin(); it != P_xk_y0_.end(); ++it){
        dummy += it->second;
        if (dummy >= randomnumber){
            return it->first;
        }
    }
} // end draw_position

void SIMULATE::replace_xk_y0(bpp::Sequence *sequence){
    if (nk[site_k] == 0) {     // independent
        // get Pvalues for other letters in row
        for (int col = 0; col < 4; col++) {
            if (col != sequence->getValue(site_k)) {
                double P_xk_y0_position = submatrix.getMonoNucletidMatrix(sequence->getValue(site_k), col) /
                                          std::abs(Qk_sk_sk_values[site_k]);
                P_xk_y0_.insert({col, std::abs(P_xk_y0_position)});// .push_back( std::abs(P_xk_y0_position) );
            }
        }
        //get mutational index
        int mutated_position = draw_position();
        if (sequence->getValue(site_k) == mutated_position ) {
            return;
        }
        double old_Qk_sk_sk_value = std::abs(Qk_sk_sk_values[site_k]);
        //set new base at position
        sequence->setElement(site_k, mutated_position);
        Qk_sk_sk_values[site_k] = submatrix.getMonoNucletidMatrix(mutated_position, mutated_position);
        double new_Qk_sk_sk_value = std::abs(Qk_sk_sk_values[site_k]);
        diff_Qk_sk_sk_value = new_Qk_sk_sk_value - old_Qk_sk_sk_value;

    } else if (nk[site_k] == 1) {
        /*
         * | Index |  0   |  1   |  2   |  3   |||  4   |  5   |  6   |  7   |||  8   |  9   |  10  |  11  |||  12  |  13  |  14  |  15  |
         * | (k,i) |  AA  |  CA  |  GA  |  UA  |||  AC  |  CC  |  GC  |  UC  |||  AG  |  CG  |  GG  |  UG  |||  AU  |  CU  |  GU  |  UU  |
         */
        //get index of both positions
        int first_value = sequence->getValue(site_k);
        int second_position = collector.getStructure().getValue(site_k); //get second letter
        int second_value = sequence->getValue(second_position);
        // get Pvalues for other letters in row
        int row = (4 * first_value) + second_value; // corresponds index of actual doublet
        for (int col = 0; col < 16; col++) {
            if (row != col) {
                if (submatrix.getDiNucletidMatrix(row, col) != 0.0){
                    double P_xk_y0_position = submatrix.getDiNucletidMatrix(row, col) / std::abs(submatrix.getDiNucletidMatrix(row, row));
                    P_xk_y0_.insert({col, P_xk_y0_position});
                } // end if not null
            } // end if row != col
        } // end for col

        //get mutational index
        int index_of_mutated_position = draw_position();
        int mutated_position = (((index_of_mutated_position + 4) / 4) - 1) % 4; // second position

        if (sequence->getValue(site_k) == mutated_position ) {
            return;
        }

        double old_Qk_sk_sk_value  = std::abs(Qk_sk_sk_values[site_k]) ;
        double old_Qk_sk_sk_second = std::abs(Qk_sk_sk_values[second_position]);

        //set new base at position
        sequence->setElement(site_k, mutated_position);
        //save Qk(sk,sk) values to exchange in update function and exchange Qk(sk,sk) for both positions

        Qk_sk_sk_values[site_k] = submatrix.getDiNucletidMatrix(index_of_mutated_position,index_of_mutated_position),
        Qk_sk_sk_values[second_position] = submatrix.getDiNucletidMatrix((4*mutated_position)+second_value,
                                                                         (4*mutated_position)+second_value);
        double new_Qk_sk_sk_value  = std::abs(Qk_sk_sk_values[site_k]);
        double new_Qk_sk_sk_second = std::abs(Qk_sk_sk_values[second_position]);
        diff_Qk_sk_sk_value = new_Qk_sk_sk_second - old_Qk_sk_sk_second + new_Qk_sk_sk_value - old_Qk_sk_sk_value;

    } else if (nk[site_k] == 2) {
        throw std::runtime_error("replace position for nk=2 actual not implemented");
        //TODO How to mutate first middle or last?
    }

} // end replace_xk_y0

void SIMULATE::update_data(bpp::Sequence *sequence, STRUCTURE structure){
//    //update QK(sk,sk) values under constraint of structure
    retrieve_Qk_sk_sk_values(sequence, structure);
    //update q(x)
    q_x_ += diff_Qk_sk_sk_value;
    // version two
    sum_Qk_sk_sk_values();

    // get new time mark and add to old
    dr = draw_from_exponential_distribution(q_x_);
    dc += dr;
    //calculate P(k) values
    calculate_Pk_values(Qk_sk_sk_values);
    //clear P_xk_y0_ (positioning) for next round
    P_xk_y0_.clear();
} // end update_data

