//
// Created by michael on 05.11.17.
//

#ifndef SISSI_2_0_REQUIREDHEADERS_H
#define SISSI_2_0_REQUIREDHEADERS_H
/*
 * * From CoreLib:
 * */
#include <Bpp/Exceptions.h>
//Rate distributions
#include<Bpp/Numeric/Prob/AbstractDiscreteDistribution.h>
#include<Bpp/Numeric/Prob/BetaDiscreteDistribution.h>
#include<Bpp/Numeric/Prob/ConstantDistribution.h>
#include<Bpp/Numeric/Prob/DirichletDiscreteDistribution.h>
#include<Bpp/Numeric/Prob/ExponentialDiscreteDistribution.h>
#include<Bpp/Numeric/Prob/GammaDiscreteDistribution.h>
#include<Bpp/Numeric/Prob/GaussianDiscreteDistribution.h>
#include<Bpp/Numeric/Prob/InvariantMixedDiscreteDistribution.h>
#include<Bpp/Numeric/Prob/MixtureOfDiscreteDistributions.h>
#include<Bpp/Numeric/Prob/SimpleDiscreteDistribution.h>
#include<Bpp/Numeric/Prob/Simplex.h>
#include<Bpp/Numeric/Prob/TruncatedExponentialDiscreteDistribution.h>
#include<Bpp/Numeric/Prob/UniformDiscreteDistribution.h>
//For the Shannon entropy
#include <Bpp/Numeric/VectorTools.h>

/*
 * * From SeqLib:
 * */
#include <Bpp/Seq/DistanceMatrix.h>
//Alphabet
#include <Bpp/Seq/Alphabet/AbstractAlphabet.h>
#include <Bpp/Seq/Alphabet/Alphabet.h>
#include <Bpp/Seq/Alphabet/AlphabetExceptions.h>
#include <Bpp/Seq/Alphabet/AlphabetNumericState.h>
#include <Bpp/Seq/Alphabet/AlphabetState.h>
#include <Bpp/Seq/Alphabet/AlphabetTools.h>
#include <Bpp/Seq/Alphabet/BinaryAlphabet.h>
#include <Bpp/Seq/Alphabet/CaseMaskedAlphabet.h>
#include <Bpp/Seq/Alphabet/CodonAlphabet.h>
#include <Bpp/Seq/Alphabet/DefaultAlphabet.h>
#include <Bpp/Seq/Alphabet/DNA.h>
#include <Bpp/Seq/Alphabet/IntegerAlphabet.h>
#include <Bpp/Seq/Alphabet/LetterAlphabet.h>
#include <Bpp/Seq/Alphabet/LexicalAlphabet.h>
#include <Bpp/Seq/Alphabet/NucleicAlphabet.h>
#include <Bpp/Seq/Alphabet/NumericAlphabet.h>
#include <Bpp/Seq/Alphabet/ProteicAlphabet.h>
#include <Bpp/Seq/Alphabet/ProteicAlphabetState.h>
#include <Bpp/Seq/Alphabet/RNA.h>
#include <Bpp/Seq/Alphabet/RNY.h>
#include <Bpp/Seq/Alphabet/WordAlphabet.h>

#include <Bpp/Seq/Sequence.h>               /* this include the definition of the Sequence object */
#include <Bpp/Seq/SequenceTools.h>          /* this include some tools to deal with sequences */
#include <Bpp/Seq/DNAToRNA.h>               /* A few translators here... */
#include <Bpp/Seq/NucleicAcidsReplication.h>
#include <Bpp/Seq/GeneticCode/StandardGeneticCode.h>

#include <Bpp/Seq/Io/Fasta.h>
#include <Bpp/Seq/Io/Mase.h>
#include <Bpp/Seq/Io/Clustal.h>
#include <Bpp/Seq/Io/Phylip.h>
#include <Bpp/Seq/Io/GenBank.h>
#include <Bpp/Seq/Io/Stockholm.h>
#include <Bpp/Seq/Io/NexusIoSequence.h>
#include <Bpp/Seq/Io/NexusTools.h>
#include <Bpp/Seq/Io/AbstractISequence.h>
//Sequence containers
#include <Bpp/Seq/Container/AbstractSequenceContainer.h>
#include <Bpp/Seq/Container/AlignedSequenceContainer.h>
#include <Bpp/Seq/Container/CompressedVectorSiteContainer.h>
#include <Bpp/Seq/Container/MapSequenceContainer.h>
#include <Bpp/Seq/Container/OrderedSequenceContainer.h>
#include <Bpp/Seq/Container/SequenceContainer.h>
#include <Bpp/Seq/Container/SequenceContainerExceptions.h>
#include <Bpp/Seq/Container/SequenceContainerIterator.h>
#include <Bpp/Seq/Container/SequenceContainerTools.h>
#include <Bpp/Seq/Container/SiteContainer.h>
#include <Bpp/Seq/Container/SiteContainerExceptions.h>
#include <Bpp/Seq/Container/SiteContainerIterator.h>
#include <Bpp/Seq/Container/SiteContainerTools.h>
#include <Bpp/Seq/Container/VectorSequenceContainer.h>
#include <Bpp/Seq/Container/VectorSiteContainer.h>

#include <Bpp/Seq/AlphabetIndex/DefaultNucleotideScore.h>

/*
 * * From PhyLib:
 * */
//Phylogenetic trees
#include <Bpp/Phyl/Tree.h>
#include <Bpp/Phyl/TreeTools.h>
#include <Bpp/Phyl/TreeTemplate.h>
#include <Bpp/Phyl/TreeExceptions.h>
#include <Bpp/Phyl/Node.h>

#include <Bpp/Phyl/Io/IoTree.h>
#include <Bpp/Phyl/Io/Newick.h>
#include <Bpp/Phyl/Io/NexusIoTree.h>
#include <Bpp/Phyl/Io/Nhx.h>
//Sequence evolution models
#include <Bpp/Phyl/Model/RateDistribution/GammaDiscreteRateDistribution.h>
#include <Bpp/Phyl/Model/AbstractBiblioMixedSubstitutionModel.h>
#include <Bpp/Phyl/Model/AbstractBiblioSubstitutionModel.h>
#include <Bpp/Phyl/Model/AbstractFromSubstitutionModelTransitionModel.h>
#include <Bpp/Phyl/Model/AbstractKroneckerWordSubstitutionModel.h>
#include <Bpp/Phyl/Model/AbstractMixedSubstitutionModel.h>
#include <Bpp/Phyl/Model/AbstractSubstitutionModel.h>
#include <Bpp/Phyl/Model/AbstractWordSubstitutionModel.h>
#include <Bpp/Phyl/Model/BinarySubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/AbstractCodonCpGSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/AbstractCodonDistanceSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/AbstractCodonFitnessSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/AbstractCodonFrequenciesSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/AbstractCodonPhaseFrequenciesSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/AbstractCodonSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/AbstractKroneckerCodonSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/CodonDistanceCpGSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/CodonDistanceFrequenciesSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/CodonDistancePhaseFrequenciesSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/CodonDistanceSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/CodonRateFrequenciesSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/CodonRateSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/GY94.h>
#include <Bpp/Phyl/Model/Codon/KCM.h>
#include <Bpp/Phyl/Model/Codon/KroneckerCodonDistanceFrequenciesSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/KroneckerCodonDistanceSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/MG94.h>
#include <Bpp/Phyl/Model/Codon/SENCA.h>
#include <Bpp/Phyl/Model/Codon/TripletSubstitutionModel.h>
#include <Bpp/Phyl/Model/Codon/YN98.h>
#include <Bpp/Phyl/Model/Codon/YNGP_M1.h>
#include <Bpp/Phyl/Model/Codon/YNGP_M10.h>
#include <Bpp/Phyl/Model/Codon/YNGP_M2.h>
#include <Bpp/Phyl/Model/Codon/YNGP_M3.h>
#include <Bpp/Phyl/Model/Codon/YNGP_M7.h>
#include <Bpp/Phyl/Model/Codon/YNGP_M8.h>
#include <Bpp/Phyl/Model/Codon/YNGP_M9.h>
#include <Bpp/Phyl/Model/FrequenciesSet/CodonFrequenciesSet.h>
#include <Bpp/Phyl/Model/FrequenciesSet/FrequenciesSet.h>
#include <Bpp/Phyl/Model/FrequenciesSet/MvaFrequenciesSet.h>
#include <Bpp/Phyl/Model/FrequenciesSet/NucleotideFrequenciesSet.h>
#include <Bpp/Phyl/Model/FrequenciesSet/WordFrequenciesSet.h>
#include <Bpp/Phyl/Model/FromMixtureSubstitutionModel.h>
#include <Bpp/Phyl/Model/InMixedSubstitutionModel.h>
#include <Bpp/Phyl/Model/KroneckerWordSubstitutionModel.h>
#include <Bpp/Phyl/Model/MarkovModulatedSubstitutionModel.h>
#include <Bpp/Phyl/Model/MixedSubstitutionModelSet.h>
#include <Bpp/Phyl/Model/MixtureOfASubstitutionModel.h>
#include <Bpp/Phyl/Model/MixtureOfSubstitutionModels.h>
#include <Bpp/Phyl/Model/Nucleotide/F84.h>
#include <Bpp/Phyl/Model/Nucleotide/GTR.h>
#include <Bpp/Phyl/Model/Nucleotide/HKY85.h>
#include <Bpp/Phyl/Model/Nucleotide/JCnuc.h>
#include <Bpp/Phyl/Model/Nucleotide/K80.h>
#include <Bpp/Phyl/Model/Nucleotide/L95.h>
#include <Bpp/Phyl/Model/Nucleotide/RN95.h>
#include <Bpp/Phyl/Model/Nucleotide/RN95s.h>
#include <Bpp/Phyl/Model/Nucleotide/SSR.h>
#include <Bpp/Phyl/Model/Nucleotide/T92.h>
#include <Bpp/Phyl/Model/Nucleotide/TN93.h>
#include <Bpp/Phyl/Model/Nucleotide/YpR.h>
#include <Bpp/Phyl/Model/Nucleotide/gBGC.h>
#include <Bpp/Phyl/Model/OneChangeTransitionModel.h>
#include <Bpp/Phyl/Model/OneChangeRegisterTransitionModel.h>
#include <Bpp/Phyl/Model/Protein/Coala.h>
#include <Bpp/Phyl/Model/Protein/CoalaCore.h>
#include <Bpp/Phyl/Model/Protein/DSO78.h>
#include <Bpp/Phyl/Model/Protein/JCprot.h>
#include <Bpp/Phyl/Model/Protein/JTT92.h>
#include <Bpp/Phyl/Model/Protein/LG08.h>
#include <Bpp/Phyl/Model/Protein/LG10_EX_EHO.h>
#include <Bpp/Phyl/Model/Protein/LGL08_CAT.h>
#include <Bpp/Phyl/Model/Protein/LLG08_EHO.h>
#include <Bpp/Phyl/Model/Protein/LLG08_EX2.h>
#include <Bpp/Phyl/Model/Protein/LLG08_EX3.h>
#include <Bpp/Phyl/Model/Protein/LLG08_UL2.h>
#include <Bpp/Phyl/Model/Protein/LLG08_UL3.h>
#include <Bpp/Phyl/Model/Protein/UserProteinSubstitutionModel.h>
#include <Bpp/Phyl/Model/Protein/WAG01.h>
#include <Bpp/Phyl/Model/RE08.h>
#include <Bpp/Phyl/Model/RegisterRatesSubstitutionModel.h>
#include <Bpp/Phyl/Model/StateMap.h>
#include <Bpp/Phyl/Model/SubstitutionModelSet.h>
#include <Bpp/Phyl/Model/SubstitutionModelSetTools.h>
#include <Bpp/Phyl/Model/WordSubstitutionModel.h>
//Likelihood methods, including ancestral states reconstruction
#include <Bpp/Phyl/Likelihood/AbstractDiscreteRatesAcrossSitesTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/AbstractHomogeneousTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/AbstractNonHomogeneousTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/AbstractTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/DRASDRTreeLikelihoodData.h>
#include <Bpp/Phyl/Likelihood/DRASRTreeLikelihoodData.h>
#include <Bpp/Phyl/Likelihood/DRHomogeneousMixedTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/DRHomogeneousTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/DRNonHomogeneousTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/DRTreeLikelihoodTools.h>
#include <Bpp/Phyl/Likelihood/GlobalClockTreeLikelihoodFunctionWrapper.h>
#include <Bpp/Phyl/Likelihood/MarginalAncestralStateReconstruction.h>
#include <Bpp/Phyl/Likelihood/NNIHomogeneousTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/PairedSiteLikelihoods.h>
#include <Bpp/Phyl/Likelihood/PseudoNewtonOptimizer.h>
#include <Bpp/Phyl/Likelihood/RASTools.h>
#include <Bpp/Phyl/Likelihood/RHomogeneousClockTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/RHomogeneousMixedTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/RHomogeneousTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/RNonHomogeneousMixedTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/RNonHomogeneousTreeLikelihood.h>
#include <Bpp/Phyl/Likelihood/TreeLikelihoodTools.h>

#include <Bpp/Phyl/Distance/DistanceEstimation.h>
#include <Bpp/Phyl/Distance/NeighborJoining.h>

#include <Bpp/Phyl/OptimizationTools.h>

#endif //SISSI_2_0_REQUIREDHEADERS_H
