//
// Created by michael on 19.03.18.
//

#ifndef SISSI_2_0_MODELS_H
#define SISSI_2_0_MODELS_H

#include <vector>
#include <string>
#include <math.h> // for calculating the power of - needed for correct indexing

#include "../Required_Bio++_Headers.h"
#include "../structure.h"

class MODEL{
private:
    std::string name;
    //parameters
    double alpha;
    double beta;
    double gamma;

    double kappa1;
    double kappa2;

    double theta;
    double GTRa;
    double GTRb;
    double GTRc;
    double GTRd;
    double GTRe;

    // single nucleotid frequencies
    /* int to char for
     *     RNA & DNA
     * 0	A 	  A
     * 1	C	  C
     * 2	G	  G
     * 3	U	  T
     */
    std::vector<double> PiSingleFrequencies;

    /*
     * double nucleotid frequencies (index = 4 * 1.char as int + 2.char as int)
     * | Index |  0   |  1   |  2   |  3   |||  4   |  5   |  6   |  7   |||  8   |  9   |  10  |  11  |||  12  |  13  |  14  |  15  |
     * | (k,i) |  AA  |  CA  |  GA  |  UA  |||  AC  |  CC  |  GC  |  UC  |||  AG  |  CG  |  GA  |  UG  |||  AU  |  CU  |  GU  |  UU  |
     * |-------|------|------|------|------|+|------|------|------|------|+|------|------|------|------|+|------|------|------|------|
     */
    std::vector<double> PiDoubleFrequencies;

    // refering same behaviour than double nuc. freq.;
    // triple nucleotid frequencies (index = 16* 1.char as int + 4 * 2.char as int + 3.char as int)
    std::vector<double> PiTripleFrequencies;

    // Count of single, double, triple elements in sequence
    double SingleFrequencyCount = 0;
    double DoubleFrequencyCount = 0;
    double TripleFrequencyCount = 0;
    // bool to check for counting positions if matrix already reseted
    bool vectors_filled_with_zeros = false;

public:
    MODEL(){
        kappa1 = 1;
        kappa2 = 1;

        GTRa = 1;
        GTRb = 1;
        GTRc = 1;
        GTRd = 1;
        GTRe = 1;
        // by default uniform distributed
        PiSingleFrequencies = { 0.25, // PiA
                                0.25, // PiC
                                0.25, // PiG
                                0.25 }; // PiT / U

        PiDoubleFrequencies = {0.0625, 0.0625, 0.0625, 0.0625,
                               0.0625, 0.0625, 0.0625, 0.0625,
                               0.0625, 0.0625, 0.0625, 0.0625,
                               0.0625, 0.0625, 0.0625, 0.0625};

        PiTripleFrequencies = {0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                               0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                               0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                               0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                               0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                               0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                               0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                               0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625};

    }
    virtual ~MODEL() = default;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     * Count nucleotid positions from whole alignment
     * */
    void count_freuqeuncies_from_alignment(const bpp::VectorSiteContainer *sites, const STRUCTURE &structure) {
        if (!vectors_filled_with_zeros ){
            PiSingleFrequencies.resize(4, 0);
            PiDoubleFrequencies.resize(16,0);
            PiTripleFrequencies.resize(64,0);
            vectors_filled_with_zeros = true;
        } // end if

        for (std::size_t i = 0; i < sites->getNumberOfSequences(); i++){
            count_freuqeuncies_from_sequence( &sites->getSequence(i), structure );
        } // end for

    } // end count_freuqeuncies_from_alignment
    /*
     * Count nucleotid positions from single sequence
     * */
    void count_freuqeuncies_from_sequence(const bpp::Sequence *sequence, const STRUCTURE &structure){
        if (!vectors_filled_with_zeros ){
            PiSingleFrequencies.resize(4, 0);
            PiDoubleFrequencies.resize(16,0);
            PiTripleFrequencies.resize(64,0);
            vectors_filled_with_zeros = true;
        } // end if

        // count out of given sequence
        for (std::size_t i=0; i<sequence->size(); i++){
            if (structure.getNk()[i] == 0 ) {
                PiSingleFrequencies[sequence->getValue(i)] ++;
                SingleFrequencyCount++;
            } else if (structure.getNk()[i] == 1 ) {
                if (i + 1 < sequence->size() ) {
                    PiDoubleFrequencies[4 * sequence->getValue(i) +
                                            sequence->getValue( structure.getValue(i) ) ]++;
                    DoubleFrequencyCount++;
                } // end if
            } else  if (structure.getNk()[i] == 2 ) {
                if ( i+2 < sequence->size() ){
                    PiTripleFrequencies[ 16 * sequence->getValue(i) +
                                          4 * sequence->getValue(structure.getValue(i)) +
                                              sequence->getValue(structure.getValue(i, 2)) - 1] ++;
                    TripleFrequencyCount++;
                } // end if
            } else {
                throw std::invalid_argument("Actually not more than 2 neighbor connections are actually allowed. Mismatch @ Position: "+i);
            }
        } // end for
        vectors_filled_with_zeros = false;
/*    // DEBUG HELP
    // check if there are triple connections
    for (auto i =0 ; i<structure.getNk().size(); i++ ){
        if (structure.getNk()[i] >= 3){
            std::cout << structure.getNk()[i] << std::endl;
        }
    }
    // print frequencies of the nucleotides
    for  (int i=0; i<PiSingleFrequencies.size(); i++){
        std::cout << PiSingleFrequencies[i] << '\t';
    } std::cout << std::endl;

    for  (int i=0; i<PiDoubleFrequencies.size(); i++){
        std::cout << PiDoubleFrequencies[i] << '\t';
    } std::cout << std::endl;

    for  (int i=0; i<PiTripleFrequencies.size(); i++){
        std::cout << PiTripleFrequencies[i] << '\t';
    } std::cout << std::endl;
*/
    } // end count_freuqeuncies_from_sequence
    /*
     * Update count of frequency needed for normalization
     * */
    void update_FrequnecyCount(char Frequnecytype) {
        if (Frequnecytype == 's'){
            SingleFrequencyCount = 0;
            for (int i = 0; i < PiSingleFrequencies.size(); i++) {
                SingleFrequencyCount += PiSingleFrequencies[i];
            }
        } else if (Frequnecytype == 'd') {
            DoubleFrequencyCount = 0;
            for (int i = 0; i < PiDoubleFrequencies.size(); i++) {
                DoubleFrequencyCount += PiDoubleFrequencies[i];
            }
        } else if (Frequnecytype == 't') {
            TripleFrequencyCount = 0;
            for (int i = 0; i < PiTripleFrequencies.size(); i++) {
                TripleFrequencyCount += PiTripleFrequencies[i];
            }
        } // end if else
    } // end update_FrequnecyCount
    /*
     * normalize frequencies so that their sum is one
     * */
    void normalize_frequencies() {
        //normalize frequency vectors
        for (int i = 0; i < PiSingleFrequencies.size(); i++) {
            PiSingleFrequencies[i] = PiSingleFrequencies[i] / SingleFrequencyCount;
        }
        update_FrequnecyCount('s');

        for (int i = 0; i < PiDoubleFrequencies.size(); i++) {
            if (PiDoubleFrequencies[i] != 0) {
                PiDoubleFrequencies[i] = PiDoubleFrequencies[i] / DoubleFrequencyCount;
            } else {
                PiDoubleFrequencies[i] = 0;
                std::cerr << "Error while normalizing frequencies! Position was hold to zero to avoid zero division." << std::endl;
            }
        } // end for
        update_FrequnecyCount('d');

        for (int i = 0; i < PiTripleFrequencies.size(); i++) {
            if (PiTripleFrequencies[i] != 0) {
                PiTripleFrequencies[i] = PiTripleFrequencies[i] / TripleFrequencyCount;
            } else {
                PiTripleFrequencies[i] = 0;
                std::cerr << "Error while normalizing frequencies! Position was hold to zero to avoid zero division." << std::endl;
            }
        } // end for
        update_FrequnecyCount('t');
    } // end normalize_frequencies
    /*
     * prints the frequencies to cout
     * */
    void print_frequencies(){
        std::cout << "Single Nucletid Frequencies" << std::endl;
        for  (int i=0; i<PiSingleFrequencies.size(); i++){
            std::cout << PiSingleFrequencies[i] << '\t';
        } std::cout << std::endl;
        std::cout << "Double Nucletid Frequencies" << std::endl;
        for  (int i=0; i<PiDoubleFrequencies.size(); i++){
                std::cout << PiDoubleFrequencies[i] << '\t';
        } std::cout << std::endl;
        std::cout << "Triple Nucletid Frequencies" << std::endl;
        for  (int i=0; i<PiTripleFrequencies.size(); i++){
                std::cout << PiTripleFrequencies[i] << '\t';
        } std::cout << std::endl;
    } // end print_frequencies
    /*
     * prints modelparamters to cout
     * */
    void print_modelparameter(){
        std::cout << "alpha\t" << alpha << std::endl
                  << "beta\t" << beta << std::endl
                  << "gamma\t" << gamma << std::endl
                  << "GTRa\t" << GTRa << std::endl
                  << "GTRb\t" << GTRb << std::endl
                  << "GTRc\t" << GTRc << std::endl
                  << "GTRd\t" << GTRd << std::endl
                  << "GTRe\t" << GTRe << std::endl
                  << "kappa1\t" << kappa1 << std::endl
                  << "kappa2\t" << kappa2 << std::endl
                  << "theta\t" << theta << std::endl
                  ;
    } // end print_modelparameter

/******************************************************************************
 * SETTER AND GETTERS
 * */
    const std::string &getName() const {
        return name;
    }

    void setName(const std::string &name) {
        MODEL::name = name;
    }

    double getAlpha() const {
        return alpha;
    }

    void setAlpha(double alpha) {
        MODEL::alpha = alpha;
    }

    double getBeta() const {
        return beta;
    }

    void setBeta(double beta) {
        MODEL::beta = beta;
    }

    double getGamma() const {
        return gamma;
    }

    void setGamma(double gamma) {
        MODEL::gamma = gamma;
    }

    double getKappa1() const {
        return kappa1;
    }

    void setKappa1(double kappa1) {
        MODEL::kappa1 = kappa1;
    }

    double getKappa2() const {
        return kappa2;
    }

    void setKappa2(double kappa2) {
        MODEL::kappa2 = kappa2;
    }

    double getKappa() const {
        return kappa1+kappa2;
    }

    void setKappa(double kappa) {
        MODEL::kappa1 = kappa/2;
        MODEL::kappa2 = kappa/2;
    }

    double getTheta() const {
        return (PiSingleFrequencies[1] + PiSingleFrequencies[2]);
    }

    double getGTRa() const {
        return GTRa;
    }

    void setGTRa(double GTRa) {
        MODEL::GTRa = GTRa;
    }

    double getGTRb() const {
        return GTRb;
    }

    void setGTRb(double GTRb) {
        MODEL::GTRb = GTRb;
    }

    double getGTRc() const {
        return GTRc;
    }

    void setGTRc(double GTRc) {
        MODEL::GTRc = GTRc;
    }

    double getGTRd() const {
        return GTRd;
    }

    void setGTRd(double GTRd) {
        MODEL::GTRd = GTRd;
    }

    double getGTRe() const {
        return GTRe;
    }

    void setGTRe(double GTRe) {
        MODEL::GTRe = GTRe;
    }

    double getPiA() const {
        return PiSingleFrequencies[0];
    }

    void setPiA(double PiA) {
        MODEL::PiSingleFrequencies[0] = PiA;
        update_FrequnecyCount('s');
        vectors_filled_with_zeros = false;
    }

    double getPiT() const {
        return PiSingleFrequencies[3];
    }

    void setPiT(double PiT) {
        MODEL::PiSingleFrequencies[3] = PiT;
        update_FrequnecyCount('s');
        vectors_filled_with_zeros = false;
    }

    double getPiG() const {
        return PiSingleFrequencies[2];
    }

    void setPiG(double PiG) {
        MODEL::PiSingleFrequencies[2] = PiG;
        update_FrequnecyCount('s');
        vectors_filled_with_zeros = false;
    }

    double getPiC() const {
        return PiSingleFrequencies[1];
    }

    void setPiC(double PiC) {
        MODEL::PiSingleFrequencies[1] = PiC;
        update_FrequnecyCount('s');
        vectors_filled_with_zeros = false;
    }

    const std::vector<double> &getPiSingleFrequencies() const {
        return PiSingleFrequencies;
    }

    const double &getPiSingleFrequencies(int index) const {
        return PiSingleFrequencies[index];
    }

    void setPiSingleFrequencies(const std::vector<double> &PiSingleFrequencies) {
        MODEL::PiSingleFrequencies = PiSingleFrequencies;
        update_FrequnecyCount('s');
        vectors_filled_with_zeros = false;
    }

    const std::vector<double> &getPiDoubleFrequencies() const {
        return PiDoubleFrequencies;
    }

    const double &getPiDoubleFrequencies(int index) const {
        return PiDoubleFrequencies[index];
    }

    void setPiDoubleFrequencies(const std::vector<double> &PiDoubleFrequencies) {
        MODEL::PiDoubleFrequencies = PiDoubleFrequencies;
        update_FrequnecyCount('d');
        vectors_filled_with_zeros = false;
    }

    const std::vector<double> &getPiTripleFrequencies() const {
        return PiTripleFrequencies;
    }

    const double &getPiTripleFrequencies(int index) const {
        return PiTripleFrequencies[index];
    }

    void setPiTripleFrequencies(const std::vector<double> &PiTripleFrequencies) {
        MODEL::PiTripleFrequencies = PiTripleFrequencies;
        update_FrequnecyCount('t');
        vectors_filled_with_zeros = false;
    }

}; // end class MODEL

#endif //SISSI_2_0_MODELS_H
