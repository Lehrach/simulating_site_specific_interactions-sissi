//
// Created by michael on 09.03.18.
//

#ifndef SISSI_2_0_ESTIMATION_RECONSTRUCTION
#define SISSI_2_0_ESTIMATION_RECONSTRUCTION

#include <iostream>

#include "../Required_Bio++_Headers.h"
#include "../structure.h"


class SEQ_ESTIMATION{
private:
    //details can be found in cookbook and sample section from Bpp - here "http://biopp.univ-montp2.fr/wiki/index.php/Reconstructing_ancestral_sequences"
    std::vector< std::vector<double> > *probs; // probabilites
    bpp::DiscreteDistribution* rDist; // uniform discrete distribution
    bpp::DRTreeLikelihood* drtl; //double-recursive likelihood class

public:
    // CONSTRUTORS DESTRUCTOR
    SEQ_ESTIMATION() = default;
    virtual ~SEQ_ESTIMATION() = default;
    /*
     * estimates model parameters from alignment with use of tree likelyhood
     * */
    void estimate_model_parameter(bpp::Alphabet *alpha, bpp::Tree *tree, bpp::SiteContainer *CompleteSites, bpp::SubstitutionModel *model, bool ModelOptimizationMethod);
    /*
     * reconstruct ancestral sequence of an alignment where two different methods can be used
     * */
    bpp::Sequence *reconstruct_ancestral(bpp::Tree *tree, bpp::SiteContainer *CompleteSites, bpp::SubstitutionModel *model, bool SeqRecoMeth);

/******************************************************************************
 * SETTER AND GETTERS
 * */

}; // end class SEQ_ESTIMATION

#endif //SISSI_2_0_ESTIMATION_RECONSTRUCTION
