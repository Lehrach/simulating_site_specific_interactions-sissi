//
// Created by michael on 27.03.18.
//

#ifndef SISSI_2_0_SUBSTITUTIONMATRICES_H
#define SISSI_2_0_SUBSTITUTIONMATRICES_H

#include <stdexcept>
#include <iostream>
#include "../numeric/matrix.h"
#include "modelstates.h"

class SUBSTITUTIONMATRIX {
private:
    MATRIX MonoNucletidMatrix;
    MATRIX DiNucletidMatrix;
    MATRIX TriNucletidMatrix;

public:
    SUBSTITUTIONMATRIX() : MonoNucletidMatrix(4),
                           DiNucletidMatrix(16, 0), TriNucletidMatrix(64, 0) {}

    virtual ~SUBSTITUTIONMATRIX () = default;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     * setting mono frequ. to matrix positions considering model parameters
     * */
    void set_mononuc_matrix_parameters(MODEL &model);
/*
     * setting di frequ. to matrix positions considering model parameters
     * */
    void set_dinuc_matrix_parameters(MODEL &model);
    /*
     * setting tri frequ. to matrix positions considering model parameters
     * TODO go along with implementation
     * */
    void set_trinuc_matrix_parameters(MODEL &model){};
    /*
     * summing function to set up all matrices
     */
    void set_matrix_parameters(MODEL &model){
        set_mononuc_matrix_parameters(model);
        set_dinuc_matrix_parameters(model);
//        //TODO IMPLEMENT
//        set_trinuc_matrix_parameters(model);

    }; // end set_matrix_parameters
    /*
     * Prints substitutinomatrix to cout
    * */
    void PrintSubstitutionMatrix();


/***********************************************************************************************************************
 * SETTER & GETTER
 **********************************************************************************************************************/
    //TODO return pointer instead or reference?
    double getMonoNucletidMatrix(const int row, const int col) {
        return MonoNucletidMatrix.getElement(row, col);
    }

    double getDiNucletidMatrix(const int row, const int col) {
        return DiNucletidMatrix.getElement(row, col);
    }

    double getTriNucletidMatrix(const int row, const int col) {
        throw std::runtime_error("Trinucletid Substitutionmatrix is not implemented");
        return TriNucletidMatrix.getElement(row, col);
    }

}; // end SUBSTITUTIONMATRIX


#endif //SISSI_2_0_SUBSTITUTIONMATRICES_H
