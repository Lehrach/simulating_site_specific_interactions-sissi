//
// Created by michael on 09.03.18.
//

#include "estimation_reconstruction.h"

void SEQ_ESTIMATION::estimate_model_parameter(bpp::Alphabet *alpha, bpp::Tree *tree, bpp::SiteContainer *CompleteSites, bpp::SubstitutionModel *model, bool ModelOptimizationMethod) {

    rDist = new bpp::GammaDiscreteDistribution(alpha->getSize());

    std::cout << "Estemating model parameters..." << std::endl;
    drtl = new bpp::DRHomogeneousTreeLikelihood(*tree, *CompleteSites,model, rDist, true);
    drtl->initialize();

    // redirect to stringstream instead of file
    std::stringstream ss;
    std::ostream *profiler(&ss);
    std::ostream *messenger(&ss);
//    std::ofstream *profiler = new std::ofstream("profile.txt", ios::out);
//    std::ofstream *messenger = new std::ofstream("messages.txt", ios::out);

    if ( ModelOptimizationMethod == true ){
        std::cout << "Using Newton's optimization method" << std::endl;
        bpp::OptimizationTools::optimizeNumericalParameters2(
                drtl,//DiscreteRatesAcrossSitesTreeLikelihood *  	tl,
                model->getIndependentParameters(),//const ParameterList &  	parameters,
                0,//OptimizationListener *  	listener = 0,
                0.000001,//Precision on log likelihood  //double  	tolerance = 0.000001,
                10000,  //Max # of evaluations //unsigned int  	tlEvalMax = 1000000,
                new bpp::StlOutputStreamWrapper(messenger), //OutputStream *  	messageHandler = ApplicationTools::message,
                new bpp::StlOutputStreamWrapper(profiler), //OutputStream *  	profiler = ApplicationTools::message,
                true, //bool  	reparametrization = false,
                false, //bool useClock=false,
                3 //unsigned int  	verbose = 1,
                //const std::string &  	optMethodDeriv = OPTIMIZATION_NEWTON,
                //const std::string &  	optMethodModel = OPTIMIZATION_BRENT
        );
    } else {
        std::cout << "Using Brent's optimization method" << std::endl;
        bpp::OptimizationTools::optimizeNumericalParameters(
                drtl,//DiscreteRatesAcrossSitesTreeLikelihood *  	tl,
                model->getIndependentParameters(),//const ParameterList &  	parameters,
                0,//OptimizationListener *  	listener = 0,
                5,//unsigned int  	nstep = 1,
                0.000001,//Precision on log likelihood  //double  	tolerance = 0.000001,
                10000,  //Max # of evaluations //unsigned int  	tlEvalMax = 1000000,
                new bpp::StlOutputStreamWrapper(messenger), //OutputStream *  	messageHandler = ApplicationTools::message,
                new bpp::StlOutputStreamWrapper(profiler), //OutputStream *  	profiler = ApplicationTools::message,
                true, //bool  	reparametrization = false,
                3 //unsigned int  	verbose = 1,
                //const std::string &  	optMethodDeriv = OPTIMIZATION_NEWTON,
                //const std::string &  	optMethodModel = OPTIMIZATION_BRENT
        );
    } // end else

//    //DEBUG HELP
//    for (size_t i=0; i< init_class.getModel()->getExchangeabilityMatrix().getNumberOfRows(); i++){
//        for (size_t j=0; j< init_class.getModel()->getExchangeabilityMatrix().getNumberOfColumns(); j++){
//            std::cout << init_class.getModel()->getExchangeabilityMatrix()(i,j) << '\t';
//        }
//        std::cout << std::endl;
//    }
//    std::cout << std::endl;

} //end estimate_model_parameter

bpp::Sequence *SEQ_ESTIMATION::reconstruct_ancestral(bpp::Tree *tree, bpp::SiteContainer *CompleteSites, bpp::SubstitutionModel *model, bool SeqRecoMeth) {
    try{
        // can be found in cookbook and sample section from Bpp - here "http://biopp.univ-montp2.fr/wiki/index.php/Reconstructing_ancestral_sequences"

        /* Define a sequence evolution model and fit it
         * You can chose any homogeneous or non-homogeneous model to reconstruct ancestral sequences.
         * Currently mixed models are not supported. In order to use the reconstruction method, we will need a double-recursive likelihood class:
         */

        std::cout << "Estemating ancestor sequence..." << std::endl;
        //bpp::SiteContainer ungaped = bpp::SiteContainerTools::changeGapsToUnknownCharacters(CompleteSites);
        // then use ungapped for estiamtion...
        drtl = new bpp::DRHomogeneousTreeLikelihood(*tree, *CompleteSites, model, rDist, true);
        drtl->initialize();

        bpp::MarginalAncestralStateReconstruction mar(drtl);

        if (SeqRecoMeth == true){
            std::cout << "Using marginal reconstruction method" << std::endl;
            /* Reconstruct ancestral sequences using the marginal method
             * This method implements the marginal reconstruction of Yang (ref). It reconstructs sequences at requested nodes independently,
             * and therefore can be inaccurate when several ancestral sequences are to be reconstructed.
             */
            bpp::Sequence *ancestralSeq =  mar.getAncestralSequenceForNode( tree->getRootId() );
            ancestralSeq->setName("AncestralSequence");
            return ancestralSeq;
        } else {
            std::cout << "Using maximum posterior probability reconstruction method" << std::endl;
            /* In this case, the state with maximum posterior probability for each site will be used.
             * In order to assess the uncertainty in the ancestral reconstruction, it is possible to retrieve
             * the posterior probabilities for each state at each site:
             */
            bpp::Sequence *ancestralSeq = mar.getAncestralSequenceForNode(tree->getRootId(), probs, false);
            ancestralSeq->setName("AncestralSequence");
            return ancestralSeq;
        }

//NOTE: both methods actually not implemented, but would be also available
//        //It is also possible to sample the ancestral sequence from the posterior probabilities:
//        bpp::Sequence* seq2 = mar.getAncestralSequenceForNode(nodeId, 0, true);
//        //Reconstruct Full set of ancestral Sequences
//        bool sample = true;
//        bpp::SiteContainer* ancSites;
//        ancSites = mar.getAncestralSequences(sample);

    } catch (bpp::Exception & e) {
        std::cerr << "ERROR! " << e.what() << std::endl;
        std::exit(1);
    } catch(std::exception &e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    } catch (...) {
        std::cerr << "Undefined errors occurred while reconstruction of ancestral sequences." << std::endl;
        std::exit(1);
    } // end try
}; // end estimate