//
// Created by michael on 27.03.18.
//


#include "substitutionmatrices.h"

void SUBSTITUTIONMATRIX::set_mononuc_matrix_parameters(MODEL &model) {
    if (model.getName() == "JC69"){
        MonoNucletidMatrix.setAllValues(0.25);
    } else if (model.getName() == "K80"){
        MonoNucletidMatrix.setAllValues(1);
        //set Kappa values to Matrix (ratio)
        MonoNucletidMatrix(0,2) *= model.getKappa();
        MonoNucletidMatrix(2,0) *= model.getKappa();
        MonoNucletidMatrix(1,3) *= model.getKappa();
        MonoNucletidMatrix(3,1) *= model.getKappa();
    } else if (model.getName() == "F81"){
        //set Pi values to Matrix
        for (int row=0; row<MonoNucletidMatrix.getSize(); row++){
            MonoNucletidMatrix(row,0) = model.getPiA();
            MonoNucletidMatrix(row,1) = model.getPiC();
            MonoNucletidMatrix(row,2) = model.getPiG();
            MonoNucletidMatrix(row,3) = model.getPiT();
        } // end for
    } else if (model.getName() == "HKY85") {
        //set Pi values to Matrix
        for (int row=0; row<MonoNucletidMatrix.getSize(); row++) {
            MonoNucletidMatrix(row, 0) = model.getPiA();
            MonoNucletidMatrix(row, 1) = model.getPiC();
            MonoNucletidMatrix(row, 2) = model.getPiG();
            MonoNucletidMatrix(row, 3) = model.getPiT();
        }
        // multiply values by kappa
        MonoNucletidMatrix(0,2) *= model.getKappa();
        MonoNucletidMatrix(2,0) *= model.getKappa();
        MonoNucletidMatrix(1,3) *= model.getKappa();
        MonoNucletidMatrix(3,1) *= model.getKappa();
    } else if (model.getName() == "T92") {
        //set Pi values to Matrix
        for (int row=0; row<MonoNucletidMatrix.getSize(); row++) {
            MonoNucletidMatrix(row, 0) = (1-model.getTheta())/2;
            MonoNucletidMatrix(row, 1) = model.getTheta()/2;
            MonoNucletidMatrix(row, 2) = model.getTheta()/2;
            MonoNucletidMatrix(row, 3) = (1-model.getTheta())/2;
        }
        // multiply values by kappa
        MonoNucletidMatrix(0,2) *= model.getKappa();
        MonoNucletidMatrix(2,0) *= model.getKappa();
        MonoNucletidMatrix(1,3) *= model.getKappa();
        MonoNucletidMatrix(3,1) *= model.getKappa();
    } else if (model.getName() == "TN93") {
        //set Pi values to Matrix
        for (int row=0; row<MonoNucletidMatrix.getSize(); row++) {
            MonoNucletidMatrix(row, 0) = model.getPiA();
            MonoNucletidMatrix(row, 1) = model.getPiC();
            MonoNucletidMatrix(row, 2) = model.getPiG();
            MonoNucletidMatrix(row, 3) = model.getPiT();
        }
        // multiply values by kappa
        MonoNucletidMatrix(0,2) *= model.getKappa1();
        MonoNucletidMatrix(2,0) *= model.getKappa1();
        MonoNucletidMatrix(1,3) *= model.getKappa2();
        MonoNucletidMatrix(3,1) *= model.getKappa2();

    } else if (model.getName() == "GTR") {
        /*
         * | Index |  0   |  1   |  2   |  3   |||
         * | (k,i) |  A   |  C   |  G   |  U   |||
         * |-------|------|------|------|------|+|
         * | A     | *    | dPiC | fPiG | bPiU |||
         * | C     | dPiA | *    | ePiG | aPiU |||
         * | G     | fPiA | ePiC | *    | cPiU |||
         * | U     | bPiA | aPiC | cPiG | *    |||
         * | ----  | ---- | ---- | ---- | ---- |+|
         * NOTE: f = 1 by normalization in datacollector
         */

        //set Pi values to Matrix
        for (int row=0; row<MonoNucletidMatrix.getSize(); row++) {
            MonoNucletidMatrix(row, 0) = model.getPiA();
            MonoNucletidMatrix(row, 1) = model.getPiC();
            MonoNucletidMatrix(row, 2) = model.getPiG();
            MonoNucletidMatrix(row, 3) = model.getPiT();
        }
        // multiply by correction values
        // parameter description following that from Bio++ documentation
        MonoNucletidMatrix(0,1) *= model.getGTRd();
        MonoNucletidMatrix(1,0) *= model.getGTRd();

        MonoNucletidMatrix(0,3) *= model.getGTRb();
        MonoNucletidMatrix(3,0) *= model.getGTRb();

        MonoNucletidMatrix(2,1) *= model.getGTRe();
        MonoNucletidMatrix(1,2) *= model.getGTRe();

        MonoNucletidMatrix(2,3) *= model.getGTRc();
        MonoNucletidMatrix(3,2) *= model.getGTRc();

        MonoNucletidMatrix(3,1) *= model.getGTRa();
        MonoNucletidMatrix(1,3) *= model.getGTRa();

    } // end if else models

    // correct diagonal entry and normalize
    MonoNucletidMatrix.normalize_matrix(model.getPiSingleFrequencies());

}; // end set_mononuc_matrix_parameters

void SUBSTITUTIONMATRIX::set_dinuc_matrix_parameters(MODEL &model) {
/* int to char for
 *     RNA & DNA
 * 0	A 	  A
 * 1	C	  C
 * 2	G	  G
 * 3	U	  T
 *
 *
 * | Index |  0   |  1   |  2   |  3   |||  4   |  5   |  6   |  7   |||  8   |  9   |  10  |  11  |||  12  |  13  |  14  |  15  |
 * | (k,i) |  AA  |  CA  |  GA  |  UA  |||  AC  |  CC  |  GC  |  UC  |||  AG  |  CG  |  GG  |  UG  |||  AU  |  CU  |  GU  |  UU  |
 * |-------|------|------|------|------|+|------|------|------|------|+|------|------|------|------|+|------|------|------|------|
 * | AA    | *    | PiCA | PiGA | PiUA |||      |      |      |      |||      |      |      |      |||      |      |      |      |
 * | CA    | PiAA | *    | PiGA | PiUA |||      |      |      |      |||      |      |      |      |||      |      |      |      |
 * | GA    | PiAA | PiCA | *    | PiUA |||      |      |      |      |||      |      |      |      |||      |      |      |      |
 * | UA    | PiAA | PiCA | PiGA | *    |||      |      |      |      |||      |      |      |      |||      |      |      |      |
 * | ----  | ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |
 * | AC    |      |      |      |      ||| *    |      |      |      |||      |      |      |      |||      |      |      |      |
 * | CC    |      |      |      |      |||      | *    |      |      |||      |      |      |      |||      |      |      |      |
 * | GC    |      |      |      |      |||      |      | *    |      |||      |      |      |      |||      |      |      |      |
 * | UC    |      |      |      |      |||      |      |      | *    |||      |      |      |      |||      |      |      |      |
 * | ----  | ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |
 * | AG    |      |      |      |      |||      |      |      |      ||| *    |      |      |      |||      |      |      |      |
 * | CG    |      |      |      |      |||      |      |      |      |||      | *    |      |      |||      |      |      |      |
 * | GA    |      |      |      |      |||      |      |      |      |||      |      | *    |      |||      |      |      |      |
 * | UG    |      |      |      |      |||      |      |      |      |||      |      |      | *    |||      |      |      |      |
 * | ----  | ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |
 * | AU    |      |      |      |      |||      |      |      |      |||      |      |      |      ||| *    |      |      |      |
 * | CU    |      |      |      |      |||      |      |      |      |||      |      |      |      |||      | *    |      |      |
 * | GU    |      |      |      |      |||      |      |      |      |||      |      |      |      |||      |      | *    |      |
 * | UU    |      |      |      |      |||      |      |      |      |||      |      |      |      |||      |      |      | *    |
 * | ----  | ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |+| ---- | ---- | ---- | ---- |
 *
 *
 * MATHEMATICAL REQUIREMENT
 * SUM of row = 0
 * SUM of (diagonal*pi) = 1
 *
 */

    if (model.getName() == "JC69"){
        DiNucletidMatrix.setAllValues(0.0625);
        // set zeros to unallowed mutation conditions
        for(int row=0; row<16; row++) {
            int subrow =(row+4)/4; // equal first letter in Matrix (A)C
            for(int col=0; col<16; col++) {
                int subcol = (col + 4) / 4; // equal second letter in Matrix A(C)
                if ((subrow != subcol) && (row - 4 * (subrow - 1) != col - 4 * (subcol - 1))) { // hammingdistance==2
                    DiNucletidMatrix(row, col) = 0;
                } else if ((subrow == subcol)) {
                    DiNucletidMatrix(row, col) = 0;
                } // end if else
            } // end for col
        } // end for row
    } else if (model.getName() == "K80"){
        DiNucletidMatrix.setAllValues(1.0);
        for(int row=0; row<16; row++) {
            int subrow =(row+4)/4; // equal first letter in Matrix (A)C
            for(int col=0; col<16; col++) {
                int subcol = (col + 4) / 4; // equal second letter in Matrix A(C)
                //multiply with kappa
                if (row==0 && col==8) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==1  && col==9 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==2  && col==10) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==3  && col==11) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==4  && col==12) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==5  && col==13) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==6  && col==14) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==7  && col==15) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==8  && col==0 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==9  && col==1 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==10 && col==2 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==11 && col==3 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==12 && col==4 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==13 && col==5 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==14 && col==6 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==15 && col==7 ) DiNucletidMatrix(row, col) *= model.getKappa();

                // multiply frequency into matrix
                DiNucletidMatrix(row, col) *= 0.0625;
                // set zeros to unallowed mutation conditions
                if( (subrow != subcol) &&  (row-4*(subrow-1) != col-4*(subcol-1)) ) { // hammingdistance==2
                    DiNucletidMatrix(row, col) = 0;
                }else if((subrow == subcol)) {
                    DiNucletidMatrix(row, col) = 0;
                } // end if else
            } // end for col
        } // end for row
    } else if (model.getName() == "F81"){
        for(int row=0; row<16; row++) {
            int subrow =(row+4)/4; // equal first letter in Matrix (A)C
            for(int col=0; col<16; col++) {
                int subcol = (col + 4) / 4; // equal second letter in Matrix A(C)
                // set all Frequencies into matrix
                DiNucletidMatrix(row, col) = model.getPiDoubleFrequencies(col);

                // set zeros to unallowed mutation conditions
                if( (subrow != subcol) &&  (row-4*(subrow-1) != col-4*(subcol-1)) ) { // hammingdistance==2
                    DiNucletidMatrix(row, col) = 0;
                }else if((subrow == subcol)) {
                    DiNucletidMatrix(row, col) = 0;
                } // end if else
            } // end for col
        } // end for row
    } else if (model.getName() == "HKY85") {
        DiNucletidMatrix.setAllValues(1.0);
        for(int row=0; row<16; row++) {
            int subrow =(row+4)/4; // equal first letter in Matrix (A)C
            for(int col=0; col<16; col++) {
                int subcol = (col + 4) / 4; // equal second letter in Matrix A(C)
                //multiply with kappa
                if (row==0 && col==8) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==1  && col==9 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==2  && col==10) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==3  && col==11) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==4  && col==12) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==5  && col==13) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==6  && col==14) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==7  && col==15) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==8  && col==0 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==9  && col==1 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==10 && col==2 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==11 && col==3 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==12 && col==4 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==13 && col==5 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==14 && col==6 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==15 && col==7 ) DiNucletidMatrix(row, col) *= model.getKappa();

                // multiply frequencies into matrix
                DiNucletidMatrix(row, col) *= model.getPiDoubleFrequencies(col);

                // set zeros to unallowed mutation conditions
                if( (subrow != subcol) &&  (row-4*(subrow-1) != col-4*(subcol-1)) ) { // hammingdistance==2
                    DiNucletidMatrix(row, col) = 0;
                }else if((subrow == subcol)) {
                    DiNucletidMatrix(row, col) = 0;
                } // end if else
            } // end for col
        } // end for row

    } else if (model.getName() == "T92") {
        //calculate Theta from frequencies
        double theta = model.getPiDoubleFrequencies(1)/2 + model.getPiDoubleFrequencies(2)/2 +
                       model.getPiDoubleFrequencies(4)/2 + model.getPiDoubleFrequencies(5) +
                       model.getPiDoubleFrequencies(6)   + model.getPiDoubleFrequencies(7)/2 +
                       model.getPiDoubleFrequencies(8)/2 + model.getPiDoubleFrequencies(9) +
                       model.getPiDoubleFrequencies(10)  + model.getPiDoubleFrequencies(11)/2+
                       model.getPiDoubleFrequencies(13)/2+ model.getPiDoubleFrequencies(14)/2;
        double mean_pi_theta = theta/16;
        double mean_pi_1_theta = (1-theta)/16;

        for(int row=0; row<16; row++) {
            int subrow = (row + 4) / 4; // equal first letter in Matrix (A)C
            // set Pi values
            DiNucletidMatrix(row, 0) = mean_pi_1_theta;
            DiNucletidMatrix(row, 1) = mean_pi_1_theta;
            DiNucletidMatrix(row, 2) = mean_pi_1_theta;
            DiNucletidMatrix(row, 3) = mean_pi_1_theta;
            DiNucletidMatrix(row, 4) = mean_pi_theta;
            DiNucletidMatrix(row, 5) = mean_pi_theta;
            DiNucletidMatrix(row, 6) = mean_pi_theta;
            DiNucletidMatrix(row, 7) = mean_pi_theta;
            DiNucletidMatrix(row, 8) = mean_pi_theta;
            DiNucletidMatrix(row, 9) = mean_pi_theta;
            DiNucletidMatrix(row, 10)= mean_pi_theta;
            DiNucletidMatrix(row, 11)= mean_pi_theta;
            DiNucletidMatrix(row, 12)= mean_pi_1_theta;
            DiNucletidMatrix(row, 13)= mean_pi_1_theta;
            DiNucletidMatrix(row, 14)= mean_pi_1_theta;
            DiNucletidMatrix(row, 15)= mean_pi_1_theta;
            for(int col=0; col<16; col++) {
                int subcol=(col+4)/4; // equal second letter in Matrix A(C)
                if (row==0 && col==8) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==1  && col==9 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==2  && col==10) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==3  && col==11) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==4  && col==12) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==5  && col==13) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==6  && col==14) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==7  && col==15) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==8  && col==0 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==9  && col==1 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==10 && col==2 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==11 && col==3 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==12 && col==4 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==13 && col==5 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==14 && col==6 ) DiNucletidMatrix(row, col) *= model.getKappa();
                else if (row==15 && col==7 ) DiNucletidMatrix(row, col) *= model.getKappa();

                // set zeros to unallowed mutation conditions
                if( (subrow != subcol) &&  (row-4*(subrow-1) != col-4*(subcol-1)) ) { // hammingdistance==2
                    DiNucletidMatrix(row, col) = 0;
                }else if((subrow == subcol)) {
                    DiNucletidMatrix(row, col) = 0;
                } // end if else
            } // end for col
        } // end for
    } else if (model.getName() == "TN93") {
        DiNucletidMatrix.setAllValues(1.0);
        for(int row=0; row<16; row++) {
            int subrow =(row+4)/4; // equal first letter in Matrix (A)C
            for(int col=0; col<16; col++) {
                int subcol = (col + 4) / 4; // equal second letter in Matrix A(C)
                //multiply with kappa
                if (row == 0 && col == 8) DiNucletidMatrix(row, col) *= model.getKappa1();
                else if (row == 1 && col == 9)  DiNucletidMatrix(row, col) *= model.getKappa1();
                else if (row == 2 && col == 10) DiNucletidMatrix(row, col) *= model.getKappa1();
                else if (row == 3 && col == 11) DiNucletidMatrix(row, col) *= model.getKappa1();
                else if (row == 4 && col == 12) DiNucletidMatrix(row, col) *= model.getKappa2();
                else if (row == 5 && col == 13) DiNucletidMatrix(row, col) *= model.getKappa2();
                else if (row == 6 && col == 14) DiNucletidMatrix(row, col) *= model.getKappa2();
                else if (row == 7 && col == 15) DiNucletidMatrix(row, col) *= model.getKappa2();
                else if (row == 8 && col == 0)  DiNucletidMatrix(row, col) *= model.getKappa1();
                else if (row == 9 && col == 1)  DiNucletidMatrix(row, col) *= model.getKappa1();
                else if (row == 10 && col == 2) DiNucletidMatrix(row, col) *= model.getKappa1();
                else if (row == 11 && col == 3) DiNucletidMatrix(row, col) *= model.getKappa1();
                else if (row == 12 && col == 4) DiNucletidMatrix(row, col) *= model.getKappa2();
                else if (row == 13 && col == 5) DiNucletidMatrix(row, col) *= model.getKappa2();
                else if (row == 14 && col == 6) DiNucletidMatrix(row, col) *= model.getKappa2();
                else if (row == 15 && col == 7) DiNucletidMatrix(row, col) *= model.getKappa2();

                // multiply frequencies into matrix
                DiNucletidMatrix(row, col) *= model.getPiDoubleFrequencies(col);

                // set zeros to unallowed mutation conditions
                if( (subrow != subcol) &&  (row-4*(subrow-1) != col-4*(subcol-1)) ) { // hammingdistance==2
                    DiNucletidMatrix(row, col) = 0;
                }else if((subrow == subcol)) {
                    DiNucletidMatrix(row, col) = 0;
                } // end if else
            } // end for col
        } // end for row

    } else if (model.getName() == "GTR") {
        /*
         * | Index |  0   |  1   |  2   |  3   |||
         * | (k,i) |  A   |  C   |  G   |  U   |||
         * |-------|------|------|------|------|+|
         * | A     | *    | dPiC | fPiG | bPiU |||
         * | C     | dPiA | *    | ePiG | aPiU |||
         * | G     | fPiA | ePiC | *    | cPiU |||
         * | U     | bPiA | aPiC | cPiG | *    |||
         * | ----  | ---- | ---- | ---- | ---- |+|
         * f = 1 by normalization in datacollector
         *
         * | Index |  0   |  1   |  2   |  3   |||  4   |  5   |  6   |  7   |||  8   |  9   |  10  |  11  |||  12  |  13  |  14  |  15  |
         * | (k,i) |  AA  |  CA  |  GA  |  UA  |||  AC  |  CC  |  GC  |  UC  |||  AG  |  CG  |  GG  |  UG  |||  AU  |  CU  |  GU  |  UU  |
         */

        for(int row=0; row<16; row++) {
            int subrow =(row+4)/4; // equal first letter in Matrix (A)C
            for(int col=0; col<16; col++) {
                int subcol = (col + 4) / 4; // equal second letter in Matrix A(C)
                // set frequency into matrix
                DiNucletidMatrix(row, col) = model.getPiDoubleFrequencies(col);

                //multiply with GTR values
                // fill fisrt submatrix with elements
                if((subrow-1)%4==0 && (subcol-1)%4==1) DiNucletidMatrix(row, col) *= model.getGTRd();
//                if((subrow-1)%4==0 && (subcol-1)%4==2) DiNucletidMatrix(row, col) *= model.getGTRf(); <<== GTRf == 1 not neccessary
                if((subrow-1)%4==0 && (subcol-1)%4==3) DiNucletidMatrix(row, col) *= model.getGTRb();
                // fill second submatrix
                if((subrow-1)%4==1 && (subcol-1)%4==0) DiNucletidMatrix(row, col) *= model.getGTRd();
                if((subrow-1)%4==1 && (subcol-1)%4==2) DiNucletidMatrix(row, col) *= model.getGTRe();
                if((subrow-1)%4==1 && (subcol-1)%4==3) DiNucletidMatrix(row, col) *= model.getGTRa();
                //fill third submatrix
//                if((subrow-1)%4==2 && (subcol-1)%4==0) DiNucletidMatrix(row, col) *= model.getGTRf(); <<== GTRf == 1 not neccessary
                if((subrow-1)%4==2 && (subcol-1)%4==1) DiNucletidMatrix(row, col) *= model.getGTRe();
                if((subrow-1)%4==2 && (subcol-1)%4==3) DiNucletidMatrix(row, col) *= model.getGTRc();
                //fill fourth submatrix
                if((subrow-1)%4==3 && (subcol-1)%4==0) DiNucletidMatrix(row, col) *= model.getGTRb();
                if((subrow-1)%4==3 && (subcol-1)%4==1) DiNucletidMatrix(row, col) *= model.getGTRa();
                if((subrow-1)%4==3 && (subcol-1)%4==2) DiNucletidMatrix(row, col) *= model.getGTRc();

                // set zeros to unallowed mutation conditions
                if( (subrow != subcol) &&  (row-4*(subrow-1) != col-4*(subcol-1)) ) { // hammingdistance==2
                    DiNucletidMatrix(row, col) = 0;
                }else if((subrow == subcol)) {
                    DiNucletidMatrix(row, col) = 0;
                } // end if else
            } // end for col
        } // end for row
    } // end if else models

    // correct diagonal entry and normalize
    DiNucletidMatrix.normalize_matrix(model.getPiDoubleFrequencies());


}; // end set_dinuc_matrix_parameters

void SUBSTITUTIONMATRIX::PrintSubstitutionMatrix() {
    std::cout << "Mono nucleotid substitutionmatrix" << std::endl;
    MonoNucletidMatrix.print_matrix();
    std::cout << std::endl;
    std::cout << "Double nucleotid substitutionmatrix" << std::endl;
    DiNucletidMatrix.print_matrix();
    std::cout << std::endl;
    // comment out as not implemented
//    std::cout << "Triple nucleotid substitutionmatrix" << std::endl;
//    TriNucletidMatrix.print_matrix();
//    std::cout << std::endl;
//    std::cout << std::endl;

} // end PrintSubstitutionMatrix