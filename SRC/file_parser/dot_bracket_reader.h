//
// Created by michael on 01.02.18.
//

#ifndef SISSI_2_0_DOT_BRACKET_READER_H
#define SISSI_2_0_DOT_BRACKET_READER_H


#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <exception>
#include <vector>

#include "../structure.h"
#include "../Required_Bio++_Headers.h"


class DOTBRACKED_READER {
private:
    bpp::Alphabet *alpha;
    std::string line;
    long int linenumber = 0;
    int found_elements = 0;

    struct {
        char begin_comment;
        std::string energy;
        float structur_energy;
        std::string temp;
        std::string desription;
    } HEADER;

    bool basic_format_ok = false;
    std::string dbn_structural;

    std::vector<std::string> comment;
    std::vector<std::string> sequence;
    std::vector<STRUCTURE> structure;


public:
    DOTBRACKED_READER(bpp::Alphabet *i_alpha) : alpha(i_alpha) {};
    virtual ~DOTBRACKED_READER() = default;

    void read_dbn_file(const std::string &path);

    void parse_brackets();

    bool check_bracket_format_count(); //need adoption for extendet format

    void parse_basic_brackets();

    /******************************************************************************
 * SETTER AND GETTERS
 * */
    const std::string &getComment(int index) const {
        return comment[index];
    }

    const std::string &getSequence(int index) const {
        return sequence[index];
    }

    const STRUCTURE &getStructure(int index) const {
        return structure[index];
    }

    const int getFound_elements() const {
        return found_elements;
    }

    const unsigned long getSequence_size() const {
        return sequence.size();
    }

}; //end class DOTBRACKET_READER;

#endif //SISSI_2_0_DOT_BRACKET_READER_H
