//
// Created by michael on 07.02.18.
//

#include "nei_cnei_reader.h"

void NEI_CNEI_READER::read_nei_file(const std::string &path) {
    try {
        std::ifstream infile(path);
        if (!infile.good() ) {
            std::string errorMessage = "File can't be opened or read. May it don't exist?";
            throw std::runtime_error(errorMessage);
        }

        //while not end of file do
        while(!infile.eof() ) {
            getline(infile, line);
            if (line.length() == 0 || line[0] == '\n' || line[0] == ' '){
                break;
            } // empty line

            if (cnei){
                parse_cnei_lines();
            }else {
                parse_nei_lines();
            };

            //count parsed lines
            counter++;
        }

    }catch(std::exception &e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    }catch (...) {
        std::cerr << "Errors occurred while parsing: " << path << std::endl;
        std::exit(1);
    } // end try
} // end read_file

void NEI_CNEI_READER::parse_nei_lines() {
    size_t pos = 0;
    std::string token;
    std::string token2;
    std::string delimiter = "|";
    std::string delimiter2 = ":";

    //parse position
    pos = line.find(delimiter);
    token = line.substr(0, pos);
    token.erase(0,3);
    std::istringstream stream(token);
    stream >> position;
//    //DEBUG HELP
//    std::cout << token << '\t'<< position << std::endl;
    line.erase(0, pos + delimiter.length());

    if (position != counter) {
        std::cout << "counter: " << counter << '\t' << "position: " << position << std::endl;
        std::string errorMessage = "Parsing Error caused by unequal index in nei file";
        throw std::runtime_error(errorMessage);
    }

    //parse connections to position
    while ((pos = line.find(delimiter2)) != std::string::npos) {
        token2 = line.substr(0, pos);
        std::istringstream stream(token2);
        stream >> connecting_position;
        line.erase(0, pos + delimiter2.length() + 1);
        line.pop_back();
        std::istringstream stream2(line);
        stream2 >> number;
//        //DEBUG HELP
//        std::cout << token2 << '\t'<< connecting_position << std::endl;
//        std::cout << line << '\t' << number << std::endl;
        if (!structure.existElement(position) )
            structure.add_structure(position,connecting_position);
    }
    // determine if sequence and structure have the same lenght
    if (counter+1 > structure.getParsed_lenght()){
        structure.setParsed_lenght(counter+1);
    }
}

void NEI_CNEI_READER::parse_cnei_lines() {
    size_t pos = 0;
    std::string token;
    std::string token2;
    std::string delimiter = "|";
    std::string delimiter2 = ":";

    //parse position
    pos = line.find(delimiter);
    token = line.substr(0, pos);
    token.erase(0,3);
    std::istringstream stream(token);
    stream >> position;
    stream >> model;
    model = model*-1; //correction of '-' delimiter
//    //DEBUG HELP
//    std::cout << token << '\t'<< position <<  '\t' << model*-1 <<std::endl;
    line.erase(0, pos + delimiter.length());

    if (position != counter) {
        std::cout << "counter: " << counter << '\t' << "position: " << position << std::endl;
        std::string errorMessage = "Parsing Error caused by unequal index in cnei file";
        throw std::runtime_error(errorMessage);
    }

    //parse connections to position
    pos = line.find(delimiter2);
    token2 = line.substr(0, pos);
    std::istringstream stream3(token2);
    stream3 >> connecting_position;
    line.erase(0, pos + delimiter2.length() + 1);


    std::istringstream stream2(line);
    stream2 >> number;
    stream2 >> dump;
    stream2 >> second_connecting_position;
    stream2 >> dump;
    dump.pop_back();
    second_number = std::stoi( dump.substr(2) );
//    //DEBUG HELP
//    std::cout << line << std::endl;
//    std::cout << number << '\t' << second_connecting_position << '\t' << second_number << std::endl;

//    std::cout << position << '\t'<< connecting_position << '\t' << second_connecting_position << std::endl;

    // determine if sequence and structure have the same lenght
    if (counter+1 > structure.getParsed_lenght()){
        structure.setParsed_lenght(counter+1);
    }
    if (!structure.existElement(position) ) {
        structure.add_structure(position, connecting_position);
        structure.add_structure(position,second_connecting_position);
    } else {
//        if (structure.getValue(position) != connecting_position) {
//            structure.add_structure(position, connecting_position);
//        }
        if (structure.getValue(position) != second_connecting_position) {
            structure.add_structure(position,second_connecting_position);
        }
    }
}

/*
 * TODO
 * Check position - check if connection is possible in case of length
 * don't allow new connection between same elements
 */