//
// Created by michael on 28.02.18.
//

#ifndef SISSI_2_0_STOCKHOLM_READER_H
#define SISSI_2_0_STOCKHOLM_READER_H

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <vector>
#include <map>

#include "../Required_Bio++_Headers.h"
#include "../structure.h"


class STOCKHOLM_READER {
private:
    int linenumber = 1;
    bool bool_fisrtline = true;
    std::string line;
    std::string tag_found;
    std::string dump;
    std::string name;
    std::string sequence;

    std::string header_version = "# STOCKHOLM 1.0";
    std::string Stockholm_alignment_separator = "//";

    std::string Stockholm_file_annotation = "#=GF";
    std::string Stockholm_sequence_column_annotation = "#=GR";
    std::string Stockholm_column_annotation = "#=GC";
    std::string Stockholm_sequence_annotation = "#=GS";

    std::string Stockholm_structure_tag = "SS";
    std::string Stockholm_structure_consensus_tag = "SS_cons";

    std::unordered_map<std::string,std::string> GF_tags;
    std::unordered_map<std::string,std::string> GR_tags;
    std::unordered_map<std::string,std::string> GC_tags;
    std::unordered_map<std::string,std::string> GS_tags;

    bpp::Alphabet *alpha;

    bool found_sequences = false;
    bpp::VectorSiteContainer *SiteContainer;

    bool found_tree = false;
    bpp::Tree *tree;

    STRUCTURE structure;

/*
 * possible tags for each mark up line
 *   #=GF tags
 *   Compulsory fields
 *   AC   Accession number:           Accession number in form PFxxxxx (Pfam) or RFxxxxx (Rfam).
 *   ID   Identification:             One word name for family.
 *   DE   Definition:                 Short description of family.
 *   AU   Author:                     Authors of the entry.
 *   SE   Source of seed:             The source suggesting the seed members belong to one family.
 *   SS   Source of structure:        The source (prediction or publication) of the consensus RNA secondary structure used by Rfam.
 *   BM   Build method:               Command line used to generate the model
 *   SM   Search method:              Command line used to perform the search
 *   GA   Gathering threshold:        Search threshold to build the full alignment.
 *   TC   Trusted Cutoff:             Lowest sequence score (and domain score for Pfam) of match in the full alignment.
 *   NC   Noise Cutoff:               Highest sequence score (and domain score for Pfam) of match not in full alignment.
 *   TP   Type:                       Type of family -- presently Family, Domain, Motif or Repeat for Pfam.
 *                                    -- a tree with roots Gene, Intron or Cis-reg for Rfam.
 *   SQ   Sequence:                   Number of sequences in alignment.
 *
 *   Optional fields
 *   DC   Database Comment:           Comment about database reference.
 *   DR   Database Reference:         Reference to external database.
 *   RC   Reference Comment:          Comment about literature reference.
 *   RN   Reference Number:           Reference Number.
 *   RM   Reference Medline:          Eight digit medline UI number.
 *   RT   Reference Title:            Reference Title.
 *   RA   Reference Author:           Reference Author
 *           RL   Reference Location:         Journal location.
 *   PI   Previous identifier:        Record of all previous ID lines.
 *   KW   Keywords:                   Keywords.
 *           CC   Comment:                    Comments.
 *   NE   Pfam accession:             Indicates a nested domain.
 *   NL   Location:                   Location of nested domains - sequence ID, start and end of insert.
 *   WK   Wikipedia link:             Wikipedia page
 *   CL   Clan:                       Clan accession
 *   MB   Membership:                 Used for listing Clan membership
 *
 *   For embedding trees:
 *   NH  New Hampshire                A tree in New Hampshire eXtended format.
 *   TN  Tree ID                      A unique identifier for the next tree.
 *
 *   Other:
 *   FR False discovery Rate:         A method used to set the bit score threshold based on the ratio of
 *           expected false positives to true positives. Floating point number between 0 and 1.
 *   CB Calibration method:           Command line used to calibrate the model (Rfam only, release 12.0 and later)
 *
 *   #=GR tags
 *   SS        Secondary Structure    For RNA [.,;<>(){}[]AaBb.-_] --supports pseudoknot and further structure markup (see WUSS documentation)
 *   For protein [HGIEBTSCX]
 *   SA        Surface Accessibility  [0-9X]
 *                   (0=0%-10%; ...; 9=90%-100%)
 *   TM        TransMembrane          [Mio]
 *   PP        Posterior Probability  [0-9*]
 *                   (0=0.00-0.05; 1=0.05-0.15; *=0.95-1.00)
 *   LI        LIgand binding         [*]
 *   AS        Active Site            [*]
 *   pAS        AS - Pfam predicted    [*]
 *   sAS        AS - from SwissProt    [*]
 *   IN        INtron (in or after)   [0-2]
 *
 *    For RNA tertiary interactions:
 *   tWW       WC/WC        in trans   For basepairs: [<>AaBb...Zz]  For unpaired: [.]
 *   cWH       WC/Hoogsteen in cis
 *   cWS       WC/SugarEdge in cis
 *   tWS       WC/SugarEdge in trans
 *   notes:  (1) {c,t}{W,H,S}{W,H,S} for general format.
 *           (2) cWW is equivalent to SS.
 *
 *   #=GC tags
 *   RF        ReFerence annotation   Often the consensus RNA or protein sequence is used as a reference
 *                                    Any non-gap character (e.g. x's) can indicate consensus/conserved/match columns
 *                                    .'s or -'s indicate insert columns
 *                                    ~'s indicate unaligned insertions
 *                                    Upper and lower case can be used to discriminate strong and weakly conserved
 *                                    residues respectively
 *   MM        Model Mask             Indicates which columns in an alignment should be masked, such
 *                                    that the emission probabilities for match states corresponding to
 *                                    those columns will be the background distribution.
 *
 *   #=GS tags
 *   AC <accession>             ACcession number
 *   DE <freetext>              DEscription
 *   DR <db>; <accession>;      Database Reference
 *   OS <organism>              Organism (species)
 *   OC <clade>                 Organism Classification (clade, etc.)
 *   LO <look>                  Look (Color, etc.)
 */

public:
    STOCKHOLM_READER() = default;
    virtual ~STOCKHOLM_READER() = default;

    void read_file(const std::string &path);

    void parse_mark_up_lines();

    void parse_tag(std::istringstream &stream, std::unordered_map<std::string,std::string> &map);

    void check_which_structure();

    bool check_bracket_format_count(std::string &SS_notation);

    void parse_structure_stream(std::istringstream &stream);

/******************************************************************************
 * SETTER AND GETTERS
 * */
    bpp::VectorSiteContainer *getSiteContainer() const {
        return SiteContainer;
    }

    const STRUCTURE &getStructure() const {
        return structure;
    }

    bpp::Tree *getTree() const {
        return tree;
    }

    bool isFound_sequences() const {
        return found_sequences;
    }

    bool isFound_tree() const {
        return found_tree;
    }

    void setSiteContainer(bpp::VectorSiteContainer *SiteContainer) {
        STOCKHOLM_READER::SiteContainer = SiteContainer;
    }

    void setAlpha(bpp::Alphabet *alpha) {
        STOCKHOLM_READER::alpha = alpha;
    }

}; // end class STOCKHOLM_READER

#endif //SISSI_2_0_STOCKHOLM_READER_H