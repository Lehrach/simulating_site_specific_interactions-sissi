//
// Created by michael on 04.02.18.
//


#include "ct_reader.h"

void CT_READER::read_ct_file(const std::string &path){
    try {
        std::ifstream infile(path);
        if (!infile.good() ) {
            throw std::runtime_error("File can't be opened or read. May it don't exist?");
        }
//
//        //while not end of file do
        while( infile.good() ) {
            getline(infile, line);
            if (line.length() == 0 || line[0] == '\n'){
                break;
            } // empty line
            std::istringstream stream(line);
            parse_ct_headerline(stream);
//            //DEBUG HELP
//            for (unsigned int i=1; i<=header.sequence_length; i++){
//                getline(infile, line);
//            }
            parse_ct_lines(infile);
            run_through++;
//            std::cout << run_through << '\t' << sequence.size() << '\n';
        }

    }catch(std::exception &e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    }catch (...) {
        std::cerr << "Errors occurred while parsing: " << path << std::endl;
        std::exit(1);
    } // end try

}; // end read_ct_file

void CT_READER::parse_ct_headerline(std::istringstream &infile){
    // Read the first line, which is the header information.
    infile>>header.sequence_length;
    infile>>temp_line;
    infile>>equal;
    temp_line = temp_line + ' ' + equal;
    infile>>header.structur_energy;
    while( !infile.eof() ) {
        infile >> temp_line_2;
        header.desription += temp_line_2 + ' ';
    }

//    //DEBUG HELP
//    std::cout << "Header line:" << '\n' << header.sequence_length << '\t' << temp_line << '\t'
//              << header.structur_energy << '\t' << header.desription << '\t' << std::endl;

        //check right header conditions
    if (header.sequence_length == 0 || !str_ignoreCase_cmp(temp_line, "ENERGY =") || header.desription.length() == 0 ) {
        throw std::runtime_error("It seems the file does not contain adequate header information.");
    };

}; // end parse_ct_lines

void CT_READER::parse_ct_lines(std::ifstream &infile){
    STRUCTURE blank_structure;
    for (unsigned int i=1; i<=header.sequence_length; i++){
        //get line and append it
        infile>> int_index;
        infile>> char_seq;
        infile>> temp_int;
        infile>> temp_int;
        infile>> connection;
        getline(infile, rest_of_line);

        if (!int_index == i+1) {
            throw std::runtime_error("Parsing Error caused by unequal index in ct file");
        };

        parsed_seq += char_seq;

        if (connection != 0) // zero indicates unpaired position
            if ( !blank_structure.existElement(i) ) // structure.add_structure adds both directions of connectivity
                if ( blank_structure.getValue(int_index-1) != connection-1 ) // suppress multi occurrences of same connection
                    //corrected structure index to fit with sequence indexing
                    blank_structure.add_structure( (int_index-1) ,(connection-1) );

    } // end for

    blank_structure.setParsed_lenght(int_index);

    sequence.push_back(parsed_seq);
    parsed_seq.clear(); // reset for next sequence
    comment.push_back(header.desription);
    header.desription = "";
    structure.push_back(blank_structure);

}; // end parse_ct_lines
