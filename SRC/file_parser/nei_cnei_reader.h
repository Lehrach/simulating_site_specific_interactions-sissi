//
// Created by michael on 07.02.18.
//

#ifndef SISSI_2_0_NEI_READER_H
#define SISSI_2_0_NEI_READER_H

#include <fstream>
#include <iostream>
#include <sstream> // stringstream
#include <string>
#include <exception>

#include "../structure.h"

class NEI_CNEI_READER {
private:
    bool cnei = false;

    unsigned int counter = 0;
    std::string line;
    std::string dump;

    int position;
    int model;
    int connecting_position;
    float number;
    int second_connecting_position;
    float second_number;

    STRUCTURE structure;

public:
    NEI_CNEI_READER() = default;
    virtual ~NEI_CNEI_READER() = default;

    void read_nei_file(const std::string &path);

    void parse_nei_lines();

    void parse_cnei_lines();

/******************************************************************************
 * SETTER AND GETTERS
 * */
    bool isCnei() const {
        return cnei;
    }

    void setCnei(bool cnei) {
        NEI_CNEI_READER::cnei = cnei;
    }

    const STRUCTURE &getStructure() const {
        return structure;
    }
};

#endif //SISSI_2_0_NEI_READER_H
