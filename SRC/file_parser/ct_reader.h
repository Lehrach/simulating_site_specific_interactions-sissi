//
// Created by michael on 04.02.18.
//

#ifndef SISSI_2_0_CT_READER_H
#define SISSI_2_0_CT_READER_H

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cstring> //same as sting.h
#include <exception>
#include <vector>

#include "../structure.h"

class CT_READER {
private:
    int run_through = 0; // number of structures found in file
    std::string line;

    // parse_ct_headerline
    char equal;
    std::string temp_line;
    std::string temp_line_2;
    struct HEADER {
        unsigned int sequence_length;
        float structur_energy;
        std::string desription;
    } header;

    //parse_ct_lines
    unsigned int int_index = 0;
    char char_seq;
    int temp_int;
    unsigned int connection;
    std::string rest_of_line;
    std::string parsed_seq;

    std::vector<std::string> comment;
    std::vector<std::string> sequence;

    std::vector<STRUCTURE> structure;


public:
    CT_READER() = default;
    virtual ~CT_READER() = default;

    void read_ct_file(const std::string &path);

    void parse_ct_headerline(std::istringstream &infile);

    void parse_ct_lines(std::ifstream &infile);

    bool str_ignoreCase_cmp(std::string const& s1, std::string const& s2) {
        if(s1.length() != s2.length())
            return false;  // optimization since std::string holds length in variable.
        return strcasecmp(s1.c_str(), s2.c_str()) == 0;
    }

    /******************************************************************************
 * SETTER AND GETTERS
 * */
    const std::string &getComment(int index) const {
        return comment[index];
    }

    const std::string &getSequence(int index) const {
        return sequence[index];
    }

    const STRUCTURE &getStructure(int index) const {
        return structure[index];
    }

    int get_int_found_seqs() const {
        return run_through;
    }

};


#endif //SISSI_2_0_CT_READER_H

