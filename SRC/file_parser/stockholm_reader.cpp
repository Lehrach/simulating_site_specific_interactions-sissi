//
// Created by michael on 28.02.18.
//

#include "stockholm_reader.h"
// TODO get all structural lines - actually these with sequencename in front are not parsed (more or less only consenus)
void STOCKHOLM_READER::read_file(const std::string &path) {
    try {
        std::ifstream infile(path);
        if (!infile.good() ) {
            std::string errorMessage = "File can't be opened or read. May it don't exist?";
            throw std::runtime_error(errorMessage);
        }

        getline(infile, line);
        // get first line and check if file is STOCKHOLM  version 1.0 format
        if ( line != header_version && bool_fisrtline) {
            bool_fisrtline = false;
            throw std::runtime_error("Fileheader is not proper - expected '# STOCKHOLM 1.0'. May wrong format version used?");
        }

        //while not end of file do
        while(!infile.eof() ) {
            getline(infile, line);
            linenumber++;
            // if empty or seperator line continue
            if (line.length() == 0 || line[0] == '\n' || line[0] == ' ' || line == Stockholm_alignment_separator){
                continue;
            }if ( line[0] == '#') {
                parse_mark_up_lines();
            } else { // all unmarked lines
                std::istringstream stream(line);
                stream >> name;
                stream >> sequence;
//                DEBUG HELP
//                std::cout << line << '\n' << name << '\t' << sequence << std::endl;

                // get bpp::Sequences from parsed unmarked line
                try {
                    bool test_alpha = true;
                    // check if chars in sequence allowed / range of alpha
                    for (int i = 0; i<sequence.size(); i++){
                        std::string char_for_test(1, sequence[i]);
                        test_alpha = alpha->isCharInAlphabet(char_for_test);
                        if (!test_alpha){
                            break;
                        }
                    }
                    if (test_alpha) {
                        found_sequences = true;
                        bpp::BasicSequence bpp_sequence(name, sequence, alpha);
                        SiteContainer->addSequence( bpp_sequence, true);
                    } else {
                        std::cerr << "Found Sequence with other alphabet used - This is NOT parsed and NOT used for simulation!" << std::endl;
                    }
                } catch (bpp::Exception & e) {
                    std::cerr << "ERROR! " << e.what() << std::endl;
                } catch(std::exception &e) {
                    std::cerr << e.what() << std::endl;
                } catch (...) {
                    std::cerr << "Errors occurred while parsing unmarked line with number: " << linenumber << std::endl;
                } // end try
            } // end else
        } // end while

        //check ich key in unorderdmap is found
        if (!GF_tags["SQ"].empty()){
            // check if given Sequenences are all found
            if ( std::stoi(GF_tags["SQ"]) != SiteContainer->getNumberOfSequences() ) {
                std::string errorMessage = "Unmatching amount of sequencens - file complains: " + GF_tags["SQ"] +
                                           " while " + std::to_string( SiteContainer->getNumberOfSequences() ) + " could be parsed.";
                throw std::runtime_error(errorMessage);
            }
        }

        //get structure
        check_which_structure();

        //check if a tree can be found - search tag
        if (!GF_tags["NH"].empty()){
            std::istringstream stringstream(GF_tags["NH"]);
            bpp::Nhx *NhxReader = new bpp::Nhx();
            tree = NhxReader->read(stringstream);
            found_tree = true;
        }
    } catch (bpp::Exception & e) {
        std::cerr << "ERROR! " << e.what() << std::endl;
        std::exit(1);
    } catch(std::exception &e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    } catch (...) {
        std::cerr << "Errors occurred while parsing: " << path << std::endl;
        std::exit(1);
    } // end try
}; // end read_file

void STOCKHOLM_READER::parse_mark_up_lines() {
    //find #=GF
    if (line.find(Stockholm_file_annotation) == 0 ){
        line.erase(0,Stockholm_file_annotation.length()+1);
        std::istringstream stream(line);
        parse_tag(stream, GF_tags);

    //find #=GR
    }if (line.find(Stockholm_sequence_column_annotation) == 0 ){
        line.erase(0,Stockholm_sequence_column_annotation.length()+1);
        std::istringstream stream(line);
        parse_tag(stream, GR_tags);

    //find #=GC
    }if (line.find(Stockholm_column_annotation) == 0 ){
        line.erase(0,Stockholm_column_annotation.length()+1);
        std::istringstream stream(line);
        parse_tag(stream, GC_tags);

    //find #=GS
    }if (line.find(Stockholm_sequence_annotation) == 0 ){
    line.erase(0,Stockholm_sequence_annotation.length()+1);
    std::istringstream stream(line);
    parse_tag(stream, GS_tags);
    }
} // end parse_mark_up_lines

void STOCKHOLM_READER::parse_tag(std::istringstream &stream, std::unordered_map<std::string,std::string> &map){
    stream >> tag_found;
    while (stream.good() ) {
        stream >> dump;
        map[tag_found] += " " + dump;
    }
//    DEBUG HELP
//    std::cout << tag_found << "\t\t" << map[tag_found] << "\n\n" << line << std::endl;
} // end parse_tag

void STOCKHOLM_READER::check_which_structure() {
    bool WUSS_notetated = false;

    //search for SS_cons tag and call structure parser
    if (!GC_tags[Stockholm_structure_consensus_tag].empty()) {
        WUSS_notetated = check_bracket_format_count(GC_tags[Stockholm_structure_consensus_tag]);
        if (WUSS_notetated) {
            std::istringstream stream(GC_tags[Stockholm_structure_consensus_tag]);
            parse_structure_stream(stream);
        }
    } else if (!GF_tags[Stockholm_structure_consensus_tag].empty() ) {
        WUSS_notetated = check_bracket_format_count(GF_tags[Stockholm_structure_consensus_tag]);
        if (WUSS_notetated) {
            std::istringstream stream(GF_tags[Stockholm_structure_consensus_tag]);
            parse_structure_stream(stream);
        }
    } else if (!GC_tags[Stockholm_structure_tag].empty() ) {
        //search for SS tag and call structure parser
        WUSS_notetated = check_bracket_format_count(GC_tags[Stockholm_structure_tag]);
        if (WUSS_notetated) {
            std::istringstream stream(GC_tags[Stockholm_structure_tag]);
            parse_structure_stream(stream);
        }
    } else if (!GF_tags[Stockholm_structure_tag].empty()) {
        WUSS_notetated = check_bracket_format_count(GF_tags[Stockholm_structure_tag]);
        if (WUSS_notetated) {
            std::istringstream stream(GF_tags[Stockholm_structure_tag]);
            parse_structure_stream(stream);
        }
    } else {
        throw std::runtime_error("No matching option to get structural information from Stockholm file");
    }
} // end check_which_structure

bool STOCKHOLM_READER::check_bracket_format_count(std::string &stream) {
    int brackets = 0;
    unsigned int parsable_length = 0;


    //check if bracket count is ok
    for (auto i = 0; i<stream.size(); i++ ) {
        if (stream[i] == '(' || stream[i] == '[' || stream[i] == '{' || stream[i] == '<' ) {
            brackets++;
            parsable_length++;
        } else if (stream[i] == ')' || stream[i] == ']' || stream[i] == '}' || stream[i] == '>' ) {
            brackets++;
            parsable_length++;
        } else if (stream[i] == '~' || stream[i] == '.' || stream[i] == ':' || stream[i] == ',' || stream[i] == '-' || stream[i] == '_' ) {
            parsable_length++;
            continue;
        } else if (stream[i] == '\n' || stream[i] == ' ' ) {
            continue;
        } else {
            std::cerr << "@ position: " << i << " found char: '" << stream[i] << "'" << std::endl;
            throw std::logic_error("line with " + Stockholm_file_annotation + ' ' + Stockholm_structure_consensus_tag +
                                   " is not notated in expected WUSS notation");
        }
    } // end for bracket count

    if (brackets %2 != 0 ){
        throw std::logic_error("Unmatching amount of openening and closing brackets in Stockholm file");
    } else {
        structure.setParsed_lenght( parsable_length );
        return true;
    }
} //end check_bracket_format_count;

void STOCKHOLM_READER::parse_structure_stream(std::istringstream &stream) {
    std::vector<unsigned int> leftbracket;
    char position;
    unsigned int position_index = 0;

    while ( stream.good() ){
        stream >> position;
        // add element for pair matching
        if (position == '(' || position == '[' || position == '{' || position == '<' ){
            position_index++;
            leftbracket.push_back(position_index);

        }
        if (position == ')' || position == ']' || position == '}' || position == '>' ){
            position_index++;
            structure.add_structure( position_index-1 , leftbracket.back()-1 );
            leftbracket.pop_back(); // remove last opener element
        }
        if (position == '~' || position == '.' || position == ':' || position == ',' || position == '-' || position == '_' ){
            position_index++;
            continue;
        }
        if (position == '\n' || position == ' ' ) {
            continue;
        }
    } // end while
} // end parse_structure_steam