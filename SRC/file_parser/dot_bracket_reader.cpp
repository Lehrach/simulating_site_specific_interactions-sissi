
#include "dot_bracket_reader.h"

void DOTBRACKED_READER::read_dbn_file(const std::string &path) {
    try {
        std::ifstream infile(path);
        if (!infile.good() ) {
            std::string errorMessage = "File can't be opened or read. May it don't exist?";
            throw std::runtime_error(errorMessage);
            }

        ///while not end of file do
        while (!getline(infile,line).fail()) {
            found_elements++;
            linenumber++;
            // Comment name line. check if a valid line can be found.
            if (line.length() == 0 || line[0] == '\n'){  // empty line
                break;
            } else {
                std::istringstream stream(line);
                stream >> HEADER.begin_comment;

                if (HEADER.begin_comment == '>') {
                    stream >> HEADER.energy;
                    stream >> HEADER.temp;
                    stream >> HEADER.structur_energy;
                    while (stream.good()) {
                        stream >> HEADER.temp;
                        HEADER.desription += HEADER.temp + ' ';
                    }
                    comment.push_back(HEADER.desription);
//                //DEBUG HELP
//                std::cout << "Header: " << header.energy << " = " << header.structur_energy << " "
//                          << header.desription << std::endl;
                }else if (HEADER.energy != "ENERGY"){
                    throw std::runtime_error("The dot-bracket file does not contain a adequate label line.");
                }else {
                    throw std::runtime_error("The dot-bracket file does not contain a sequence label starting with '>'.");
                }
            } // end else

            //second line == sequence line
            if (getline(infile, line).fail()) {
                throw std::runtime_error("It seems the file did not contain a sequence.");
            } else {
                linenumber++;
//                //DEBUG HELP
//                std::cout << "Sequence  :" << line << std::endl;
                bool test_alpha = true;
                // check if chars in sequence allowed / range of alpha
                for (int i = 0; i<line.size(); i++){
                    std::string char_for_test(1, line[i]);
                    test_alpha = alpha->isCharInAlphabet(char_for_test);
                    if (!test_alpha){
                        break;
                    }
                }
                if (test_alpha) {
                    sequence.push_back(line);
                } else {
                    std::cerr << "Found Sequence with other alphabet used - This is NOT parsed and NOT used for simulation!" << std::endl;
                }

            }

            //third line which is the structural
            if (getline(infile, line).fail()) {
                throw std::runtime_error("It seems the file did not contain structural information.");
            } else {
                linenumber++;
//                //DEBUG HELP
//                std::cout << "Structural:" << line << std::endl;
                dbn_structural = std::move(line);
                parse_brackets();
            }

        } // end while

    }catch(std::exception &e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    }catch (...) {
        std::cerr << "Errors occurred while parsing: " << path << std::endl;
        std::exit(1);
    } // end try
};// end read_dbn_file

void DOTBRACKED_READER::parse_brackets() {
    // Multiple bracket types can be used. Pseudoknots can be encoded by using different types of brackets: e.g.  ..<<<..(...>>>..)..
    try {
        check_bracket_format_count();

//        //DEBUG Help
//        std::cout<< basic_format_ok << std::endl;
        if (basic_format_ok){
            parse_basic_brackets();
        } else {
            throw std::runtime_error("Brackets are in extended Dot Bracket Notation which can't be parsed");
        }
    }catch(std::exception &e) {
        std::cerr << e.what() << std::endl;
    }catch (...) {
        std::cerr << "undefined errors occurred while parsing brackets" << std::endl;
    } // end try
}; //end parse_brackets

bool DOTBRACKED_READER::check_bracket_format_count(){
    //check if format is basic dbn
    int basicbrackets = 0;

    for (unsigned int i = 0; i<dbn_structural.size(); i++){
        //check elements of string if something else than basic signs
        if (dbn_structural[i] == '.' ) {
            continue;
        }
        if (dbn_structural[i] == '(' || dbn_structural[i] == ')' ){
            basicbrackets++;
            continue;
        }
        else{
            basic_format_ok = false;
        }
    }
//    //DEBUG Help
//    std::cout<< basicbrackets << '\t' << basicbrackets % 2 <<std::endl;

    // check if enough brackets are available for pairing
    if (basicbrackets % 2 == 0) {
        basic_format_ok = true;
    } else {
        basic_format_ok = false;
    }
};

void DOTBRACKED_READER::parse_basic_brackets(){
    std::vector<int> leftbracket;
    STRUCTURE dbn_structure;

    // determine if sequence and structure have the same lenght
    dbn_structure.setParsed_lenght(dbn_structural.size());
    for (auto i = 0; i<dbn_structural.size(); i++){

        if (dbn_structural[i] == '.' ) {
            continue;
        }
        // add element for pair matching
        if (dbn_structural[i] == '(' ){
            leftbracket.push_back(i);
        }
        if (dbn_structural[i] == ')' ){
//            //DEBUG HELP
//            std::cout<< leftbracket.back() <<  '\t' << i <<std::endl;
            dbn_structure.add_structure(leftbracket.back(),i);
            leftbracket.pop_back(); // remove last opener element
        }
    } //end for
    structure.push_back(dbn_structure);

}; //end parse_basic_brackets


/*
 * https://users.cs.duke.edu/~reif/courses/molcomplectures/DNA.Modeling/DotBracketNotationForRNA&DNAnanostructures/DotBracketNotationForRNA&DNAnanostructures.pdf
 * is not implemented actually - follows the logic of
 * An Extended Dot-Bracket-Notation for Functional Nucleic Acids
 * https://www.researchgate.net/publication/39997644_An_Extended_Dot-Bracket-Notation_for_Functional_Nucleic_Acids
 * Looks like:
 * Standard notation           Extended notation
 * ...(((((....))))).))))))))  .3(5.4)5.)8 3A
 * (((....))).....((((((((     (3.4)3.5(8 3B
 * (((((....))))).)))))))).    (5.4)5.)8. 4A
 * ..)).)))........            .2)2.)3.8 4C
 */

/*
void DOTBRACKED_READER::parse_extended_brackets(int index){
    std::vector<int> leftbracket;
    unsigned int temp_int = 0;

    for (unsigned int i = 0; i<dnb_structural[index].size(); i++){
        //continue anmatched pair
        if (dnb_structural[index][i] == '.' ) {
            continue;
        }
        // add element for pair matching
        if (dnb_structural[index][i] == '(' ){
            leftbracket.push_back(i);
        }
        if (dnb_structural[index][i] == ')' ){
//            //DEBUG HELP
//            std::cout<< leftbracket.back() <<  '\t' << i <<std::endl;
            structure.add_structure(leftbracket.back(),i);
            leftbracket.pop_back(); // remove last opener element
        }
    } //end for
}; //end parse_extended_brackets


*/