//
// Created by michael on 19.12.17.
//

#ifndef SISSI_2_0_ARGUEMENTS_H
#define SISSI_2_0_ARGUEMENTS_H


#include <vector>
#include <boost/program_options.hpp>


namespace po = boost::program_options;

class ARGUMENTS {
private:
    po::variables_map vm; // boosts variable map contain all given arguments
    int alphabet = 0; // alphabet state
    std::string modelname; //holds parsed name of substitutionmodel
    std::string randomname; //holds name of PRNG to be used
    unsigned long seed_value = 0; // parsed seed value for PRNG

public:
    ARGUMENTS() = default ;

    virtual ~ARGUMENTS() = default;

    po::variables_map &storeVM() {
        return vm;
    }

    /*
     * Reads modelargument from container and sets class var modelname
     * */
    void check_modelname(){
        if (vm.count("DS078") && vm["DS078"].as<bool>() == true)
            modelname = "DS078";
        else if (vm.count("JTT92") && vm["JTT92"].as<bool>() == true)
            modelname = "JTT92";
        else if (vm.count("WAG01") && vm["WAG01"].as<bool>() == true)
            modelname = "WAG01";

        else if (vm.count("GY94") && vm["GY94"].as<bool>() == true)
            modelname = "GY94";
        else if (vm.count("MG94") && vm["MG94"].as<bool>() == true)
            modelname = "MG94";
        else if (vm.count("YN98") && vm["YN98"].as<bool>() == true)
            modelname = "YN98";

        else if (vm.count("K80") && vm["K80"].as<bool>() == true)
            modelname = "K80";
        else if (vm.count("F81") && vm["F81"].as<bool>() == true)
            modelname = "F81";
        else if (vm.count("HKY85") && vm["HKY85"].as<bool>() == true)
            modelname = "HKY85";
        else if (vm.count("T92") && vm["T92"].as<bool>() == true)
            modelname = "T92";
        else if (vm.count("TN93") && vm["TN93"].as<bool>() == true)
            modelname = "TN93";
        else if (vm.count("GTR") && vm["GTR"].as<bool>() == true)
            modelname = "GTR";
        else if (vm.count("RN95") && vm["RN95"].as<bool>() == true)
            modelname = "RN95";
        else if (vm.count("SSR") && vm["SSR"].as<bool>() == true)
            modelname = "SSR";
        //default true that for must be checked last
        else if (vm.count("JC69") && vm["JC69"].as<bool>() == true)
            modelname = "JC69";

        else
            throw std::runtime_error("Errors occoured while parsing model state");
    }; //end check_modelname

    /*
     * reads parsed container and saves parsed option for PRNG in class member var randomname
     * */
    void check_random(){
        if (vm.count("JKISS") && vm["JKISS"].as<bool>() == true)
            randomname = "JKISS";
        //default true that for must be checked last
        else if (vm.count("MT19937") && vm["MT19937"].as<bool>() == true)
            randomname = "MT19937";
        else
            throw std::runtime_error("Errors occoured while setting random state");
    };

    /*
     * do both functions to save lines
     * as well as save seeding value itself
     * */
    void check_all(){
        check_modelname();
        check_random();
        //check and set seed value
        if (vm.count("SEED") ) {
            seed_value = vm["SEED"].as<unsigned long>();
        }

    };


/******************************************************************************
 * SETTER AND GETTERS
 * */
    const double &getdouble(const std::string &keyvalue) const {
        if (vm.count(keyvalue)) {
            return vm[keyvalue].as<double>();
        } else {
            throw std::logic_error("Argument doesn't exist: " + keyvalue);
        }
    }

    const bool &getbool(const std::string &keyvalue) const {
        if (vm.count(keyvalue)) {
            return vm[keyvalue].as<bool>();
        } else {
            throw std::logic_error("Argument doesn't exist: " + keyvalue);
        }
    }

    const std::string &get_std_string(const std::string &keyvalue) const {
        if (vm.count(keyvalue)) {
            return vm[keyvalue].as<std::string>();
        } else {
            throw std::logic_error("Argument doesn't exist: " + keyvalue);
        }
    }

    const std::vector<double> &get_Nukleotidfrequencies(const std::string &keyvalue, const int &size) const {
        if (vm.count(keyvalue) ){
            if (!vm[keyvalue].empty() && ( vm[keyvalue].as<std::vector<double> >()).size() == size) {
                return vm[keyvalue].as<std::vector<double> >();
            } else {
                throw std::runtime_error ("The amount of given " + keyvalue + " nucleotide frequency options is not equal to " + std::to_string(size) );
            }
        } // end if count
    } // end get_Nukleotidfrequencies

    const po::variables_map &getVm() const {
        return vm;
    }

    const int &getAlphabet() const {
        return alphabet;
    }

    void setAlphabet(const int &alphabet) {
        ARGUMENTS::alphabet = alphabet;
    }

    const std::string &getModelname() const {
        return modelname;
    }

    const std::string &getRandomname() const {
        return randomname;
    }

    uint64_t getSeed_value() const {
        return seed_value;
    }
/******************************************************************************/
}; //end class


#endif //SISSI_2_0_ARGUEMENTS_H
