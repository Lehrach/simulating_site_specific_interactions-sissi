
#ifndef SISSI_2_0_BOOST_PROGRAMM_OPTIONS_H
#define SISSI_2_0_BOOST_PROGRAMM_OPTIONS_H

#define VERSION 2.0

#include <boost/program_options.hpp>
#include <iostream>
#include <iterator> // for read configfile
#include <fstream>  // for read configfile

#include "arguments.h"

namespace po = boost::program_options;

/*
 * reads arguments from cmd or config file and saves them in ARGUMETNS container
 * */
void parse_arguements(int , char** , ARGUMENTS&);
/*
 * throws error if 2 options are conflicting
 * */
void conflicting_options(const ARGUMENTS& , const char* , const char* );
/*
 * throws error if 2 options are conflicting
 * */
void conflicting_options(const po::variables_map& vm, const std::string &opt1, const std::string &opt2);
/*
 * throws error if 2 options are not given or readable
 * */
void option_dependency(const ARGUMENTS& , const char* , const char* );


#endif //SISSI_2_0_BOOST_PROGRAMM_OPTIONS_H