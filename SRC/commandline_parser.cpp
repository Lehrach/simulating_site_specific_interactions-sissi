
#include "commandline_parser.h"

/*
 * Function used to check that 'opt1' and 'opt2' are not specified
 * at the same time.
 * */
void conflicting_options(const po::variables_map& vm, const char* opt1, const char* opt2) {
    if (vm.count(opt1) && !vm[opt1].defaulted()
        && vm.count(opt2) && !vm[opt2].defaulted())
        throw std::logic_error(std::string("Conflicting options '")
                          + opt1 + "' and '" + opt2 + "'.");
} // end conflicting_options

void conflicting_options(const po::variables_map& vm, const std::string &opt1, const std::string &opt2) {
    if (vm.count(opt1) && !vm[opt1].defaulted()
        && vm.count(opt2) && !vm[opt2].defaulted())
        throw std::logic_error(std::string("Conflicting options '")
                               + opt1 + "' and '" + opt2 + "'.");
} // end conflicting_options

/*
 * Function used to check that of 'for_what' is specified, then
 * 'required_option' is specified too.
 * */
void option_dependency(const po::variables_map& vm, const char* for_what, const char* required_option) {
    if (vm.count(for_what) && !vm[for_what].defaulted())
        if (vm.count(required_option) == 0 || vm[required_option].defaulted())
            throw std::logic_error(std::string("Option '") + for_what
                              + "' requires option '" + required_option + "'.");
} // end option_dependency

void parse_arguements(int argc, char** argv, ARGUMENTS& argue_container) {
    try {
    //Generic group
        std::string config_file;
    // Basic group
        std::string ifile;
        std::string ofile;
        std::string treefile;
        std::string neighbourhoodfile;
        std::string alphabet;
        std::vector<double> singlenukfrequencies;
        std::vector<double> doublenukfrequencies;
        std::vector<double> triplenukfrequencies;

    //Allowed group
        //lists all avaiable options for confliction check
        std::vector<std::string> available_alphabet = {"alphabet", "ar","ad","ap","ab","ac"};
        bool rna_alphabet;
        bool dna_alphabet;
        bool protein_alphabet;
        bool codon_alphabet;
        bool binary_alphabet;
        bool modelparameter_optimization_method;
        bool ancester_reconstruction_method;
        bool random_ancester_sequence;
    //Substitution models
        //lists all avaiable options for confliction check
        std::vector<std::string> available_submodels = {"JC69", "K80", "F81", "HKY85", "T92", "TN93", "GTR", "DS078", "JTT92", "WAG01", "GY94", "MG94", "YN98"};
        //Nucleic models
        bool JC69, K80, F81, HKY85, T92, TN93, GTR;
        //Protein models
        bool DS078, JTT92, WAG01;
        //Codon models
        bool GY94, MG94, YN98;
        // model parameter
        double kappa1, kappa2, GTRa, GTRb, GTRc, GTRd, GTRe, GTRf;

    //random group
        bool JKISS, MT19937;
        unsigned long seed_value;

    //printing options
        bool testmode,frequency, matrix, sequence, nodes, seed, ancester, substitution, time;


        /*
         * generate group of commandline only options
         * */
        po::options_description generic("Generic Options");
        generic.add_options()
                ("version,v", "print version string")
                ("help,h", "produce help message")
                ("configfile,c", po::value(&config_file)->value_name("path")
                        , "set filepath to configuration file\n"
                        "!!! Remeber that command line arguements overwrite configfile values !!!")
                ;

        /*
        * generate group of Required Options
        * */
        po::options_description basic("Basic Options");
        basic.add_options()
                 /*
                  * First parameter describes option name/short name
                  * The second is parameter to option
                  * The third is description
                  *
                  * useful addons...
                  * ->required()
                  * ->value_name("path")
                  * ->default_value("RNA")
                  * ->zero_tokens()
                  * ->multitoken()
                  * ->min_tokens() vs ->max_tokens()
                  */
                ("input,i", po::value(&ifile)->value_name("path"), "sets path to the input sequence file")
                ("alphabet,a", po::value(&alphabet), "set alphabetcode for sequence "
                        "\n[RNA , DNA , PROTEIN , BINARY, CODON]\n"
                        "may also the two letter setters chosen\n"
                        "RNA = DEFAULT")
                ("output,o", po::value(&ofile)->value_name("path"), "pathname for output")
                ("treefile,t",po::value(&treefile)->value_name("path"), "sets the path to tree file")
                ("neighbourhood,n",po::value(&neighbourhoodfile)->value_name("path")->required(), "sets the path to neighbourhood file")
                ("single", po::value(&singlenukfrequencies)->value_name("frequencies")->multitoken(),
                        "#A #C #G #U =  4 'single'  nucleotide frequencies")
                ("double", po::value(&doublenukfrequencies)->value_name("frequencies")->multitoken(),
                        "#AA ... #UU = 16 'doublet' nucleotide frequencies")
                ("triple", po::value(&triplenukfrequencies)->value_name("frequencies")->multitoken(),
                        "#AAA...#UUU = 64 'triplet' nucleotide frequencies")
                ;

        /*
         * generate group of allowed option
         * */
        po::options_description allowed("Allowed options");
        allowed.add_options()
                ("ar", po::value(&rna_alphabet)->value_name("bool")->default_value(true)
                        ->zero_tokens(), "sets RNA Alphabet for Sequence = DEFAULT")
                ("ad", po::value(&dna_alphabet)->value_name("bool")->default_value(false)
                        ->zero_tokens(), "sets DNA Alphabet for Sequence")
                ("ap", po::value(&protein_alphabet)->value_name("bool")->default_value(false)
                        ->zero_tokens(), "sets Protein Alphabet for Sequence")
                ("ab", po::value(&binary_alphabet)->value_name("bool")->default_value(false)
                        ->zero_tokens(), "sets Binary Alphabet for Sequence")
                ("ac", po::value(&binary_alphabet)->value_name("bool")->default_value(false)
                        ->zero_tokens(), "sets Codon Alphabet for Sequence")
                ("paom", po::value(&modelparameter_optimization_method)->value_name("bool")->default_value(false)
                        ->zero_tokens(), "sets the optimization method of model parameter\n"
                         "Default = Brent or BFGS (1-dimensional) \nset flag = Newton (numerical)")
                ("remeth", po::value(&ancester_reconstruction_method)->value_name("bool")->default_value(true)
                        ->zero_tokens(), "sets the reconstruction method for ancestral Sequence \n"
                        "Default = marginal reconstruction \nset flag = maximum posterior probability")
                ("ras", po::value(&random_ancester_sequence)->value_name("bool")->default_value(true)
                        ->zero_tokens(), "generated random ancester sequence from frequencies\n"
                         "Default = true \nNOTE: Frequencies may come from input or alignment data")
                ;

        /*
         * generate groups of Substitution Models
        * */
        po::options_description NucModels("Nucleotid Substitution model options for reconstruction purpose");
        NucModels.add_options()
                ("JC69", po::value(&JC69)->zero_tokens()->default_value(true), "Jukes & Cantor model (1969) "
                        "\nThis is also available with proteic alphabet \n= DEFAULT")
                ("K80", po::value(&K80)->zero_tokens(), "Kimura (1980)")
                ("F81", po::value(&F81)->zero_tokens(), "Felsenstein (1981)")
                ("HKY85", po::value(&HKY85)->zero_tokens(), "Hasegawa, Kishino & Yano (1985)")
                ("T92", po::value(&T92)->zero_tokens(), "Tamura (1992)")
                ("TN93", po::value(&TN93)->zero_tokens(), "Tamura & Nei (1993)")
                ("GTR", po::value(&GTR)->zero_tokens(), "General Time-Reversible substitution model")
                ;
        po::options_description ProteinModels("Protein Substitution model options for reconstruction purpose");
        ProteinModels.add_options()
                ("DS078", po::value(&DS078)->zero_tokens(), "Dayhoff, Schwartz & Orcutt")
                ("JTT92", po::value(&JTT92)->zero_tokens(), "Jones, Taylor & Thornton (1992)")
                ("WAG01", po::value(&WAG01)->zero_tokens(), "Whelan & Goldman (2001)")
                ;
        po::options_description CodonModels("Codon Substitution model options for reconstruction purpose");
        CodonModels.add_options()
                ("GY94", po::value(&GY94)->zero_tokens(), "Goldman & Yang (1994)")
                ("MG94", po::value(&MG94)->zero_tokens(), "Muse & Gaut (1994)")
                ("YN98", po::value(&YN98)->zero_tokens(), "Yang & Nielsen (1998)")
                ;
        po::options_description Modelparameter("Substitution model parameters");
        Modelparameter.add_options()
                ("kappa1", po::value(&kappa1)->value_name("value"), "parameter kappa1")
                ("kappa2", po::value(&kappa2)->value_name("value"), "parameter kappa2 - \n"
                        "NOTE: some models use only kappa in that case kappa1 and / or kappa2 can be used (sum of both)")
                ("GTRa",  po::value(&GTRa)->value_name("value"),  "parameter a for GTR model")
                ("GTRb",  po::value(&GTRb)->value_name("value"),  "parameter b for GTR model")
                ("GTRc",  po::value(&GTRc)->value_name("value"),  "parameter c for GTR model")
                ("GTRd",  po::value(&GTRd)->value_name("value"),  "parameter d for GTR model")
                ("GTRe",  po::value(&GTRe)->value_name("value"),  "parameter e for GTR model")
                ("GTRf",  po::value(&GTRf)->value_name("value"),  "parameter f for GTR model")
                ;

        po::options_description Random("Random Generators");
        Random.add_options()
                ("JKISS,j", po::value(&JKISS)->value_name("bool")->default_value(false)
                        ->zero_tokens(), "JKISS 64bit (Marsaglia and Zamann, 2011)")
                ("MT19937,m", po::value(&MT19937)->value_name("bool")->default_value(true)
                        ->zero_tokens(), "Mersenne Twister 64bit (Matsumoto and Nishimura, 2000) = DEFAULT")
                ("SEED", po::value(&seed_value)->value_name("value"), "64bit long seed value \n"
                        "DEFAULT = system generated (UUID based)")
                ;
        po::options_description PrintOptions("Additional print out options");
        PrintOptions.add_options()
                ("test_mode",po::value(&testmode)->value_name("bool")->default_value(false)->zero_tokens(),
                 "print various information on screen \nDEFAULT = false")
                ("frequency",po::value(&frequency)->value_name("bool")->default_value(false)->zero_tokens(),
                 ("prints normalized frequencies \nDEFAULT = false"))
                ("matrix",po::value(&frequency)->value_name("matrix")->default_value(false)->zero_tokens(),
                 ("prints normalized substitutionmatrix \nDEFAULT = false"))
                ("sequence",po::value(&sequence)->value_name("bool")->default_value(false)->zero_tokens(),
                 ("prints ancestral sequence \nDEFAULT = false"))
                ("nodes",po::value(&nodes)->value_name("bool")->default_value(false)->zero_tokens(),
                 ("prints internal tree nodes in set output \nDEFAULT = false"))
                ("print_seed",po::value(&seed)->value_name("bool")->default_value(false)->zero_tokens(),
                 ("prints seed value \nDEFAULT = false"))
                ("count_ancester",po::value(&ancester)->value_name("bool")->default_value(false)->zero_tokens(),
                 ("count frequencies from single given sequence \nDEFAULT = false"))
                ("substitution",po::value(&substitution)->value_name("bool")->default_value(false)->zero_tokens(),
                 ("prints number of substitution and Hamming distance per node\nDEFAULT = false"))
                ("time",po::value(&time)->value_name("bool")->default_value(false)->zero_tokens(),
                 ("prints runtime in milliseconds \nDEFAULT = false"))
                ;
        /*
         * Join CMDLINE groups together
         * */
        po::options_description cmdline_options;
        cmdline_options.add(generic).add(basic).add(allowed).add(NucModels).add(ProteinModels)
                .add(CodonModels).add(Modelparameter).add(Random).add(PrintOptions);

        /*
         * Join CONFIGFILE groups together
         * */
        po::options_description config_file_options;
        config_file_options.add(basic).add(allowed).add(NucModels).add(ProteinModels).add(CodonModels)
                .add(Modelparameter).add(Random).add(PrintOptions);

        /*
         * parse given options from command line and store it in po::variables_map which ist set as class member of ARGUMENTS
         * */
        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, cmdline_options), argue_container.storeVM());      // Parse Command line arguments
        if ( argue_container.getVm().count("configfile") ) {      // if configfile is parsed from commandline - configfile will be parsed
            std::ifstream ifs( argue_container.getVm()["configfile"].as<std::string>() );
            if (!ifs) {
                std::string errorMessage = "can not open configuration file: " + config_file;
                throw std::runtime_error(errorMessage);
            }
            else {
                po::store(po::parse_config_file(ifs, config_file_options), argue_container.storeVM());
            }
        } // end if (argue_container.getVm().count("configfile"))


        /*
         * Define all conflict options
         * */
        //check alphabet for conflicts
        for (auto &opt1 : available_alphabet){
            for (auto &opt2 : available_alphabet) {
                if (opt1 != opt2) {
                    conflicting_options(argue_container.getVm(), opt1, opt2);
                }
            }
        }
        //check models for conflicts
        for (auto &opt1 : available_submodels){
            for (auto &opt2 : available_submodels) {
                if (opt1 != opt2) {
                    conflicting_options(argue_container.getVm(), opt1, opt2);
                }
            }
        }
        // Nucleic models conflict with non nucleic alphabet
        for (auto opt1 : {"JC69", "K80", "F81", "HKY85", "T92", "TN93", "GTR", "RN95", "SSR"} ) {
            for (auto opt2 : {"ap","ab","ac"} ) {
                conflicting_options(argue_container.getVm(), opt1, opt2);
            }
        }
        // conflict Random Generator
        conflicting_options(argue_container.getVm(), "XORO", "MT19937");
        conflicting_options(argue_container.getVm(), "XORO", "JKISS");
        conflicting_options(argue_container.getVm(), "JKISS", "MT19937");

/***********************************************************************************************************************
 * First Revision of parsed Arguments
 * all other  than alphabet is done later caused by complexity and depencies
 **********************************************************************************************************************/
        //check if Alphabet is in readable form and set ARGUMENTS::alphabet in state
        if ( argue_container.getVm().count("alphabet") ){
            argue_container.setAlphabet(-1); // set to impossible state
            if ( argue_container.getVm()["alphabet"].as<std::string>() == "RNA" ) {
                argue_container.setAlphabet(1);
//                std::cout << "RNA Alphabet found" << std::endl;
            } else if ( argue_container.getVm()["alphabet"].as<std::string>() == "DNA") {
                argue_container.setAlphabet(2);
//                std::cout << "DNA Alphabet found" << std::endl;
            } else if ( argue_container.getVm()["alphabet"].as<std::string>() == "PROTEIN") {
                argue_container.setAlphabet(3);
//                std::cout << "Protein Alphabet found" << std::endl;
            } else if ( argue_container.getVm()["alphabet"].as<std::string>() == "BINARY" ) {
                argue_container.setAlphabet(4);
//                std::cout << "Binary Alphabet found" << std::endl;
            } else if ( argue_container.getVm()["alphabet"].as<std::string>() == "CODON" ) {
                argue_container.setAlphabet(5);
//                std::cout << "Binary Alphabet found" << std::endl;
            } else {
                argue_container.setAlphabet(-1); // set to impossible state
                throw std::invalid_argument("Alphabet option not readable / allowed");
            }
        } // end if argue_container.getVm().count("alphabet")

        // get short alpha options and set them in ARGUMENTS as state
        if (argue_container.getAlphabet() == 0) {
            if ( argue_container.getVm().count("ad") &&  argue_container.getVm()["ad"].as<bool>() == true ) {
                argue_container.setAlphabet(2);
            } else if ( argue_container.getVm().count("ap") &&  argue_container.getVm()["ap"].as<bool>() == true ) {
                argue_container.setAlphabet(3);
            } else if ( argue_container.getVm().count("ab") &&  argue_container.getVm()["ab"].as<bool>() == true ) {
                argue_container.setAlphabet(4);
            } else if ( argue_container.getVm().count("ac") &&  argue_container.getVm()["ac"].as<bool>() == true ) {
                argue_container.setAlphabet(5);
            } else if ( argue_container.getVm().count("ar") &&  argue_container.getVm()["ar"].as<bool>() == true ) {
                argue_container.setAlphabet(1);
            } else {
                argue_container.setAlphabet(-1);
                std::invalid_argument("short alphabet option can not be processed");
            }
        }

        // check if alphabet state is set correctly
        if (argue_container.getAlphabet() == -1) {
            throw std::runtime_error("Errors occoured while setting alphabet state");
        }

        /*
         * Define all options which are depending on another one
         * from logic this "here" requires "that" one
         * */
        if ( argue_container.getVm().count("input") && !argue_container.getVm()["input"].defaulted() ) // if input exists
            if ( argue_container.getAlphabet() == 0 || argue_container.getAlphabet() == -1 ) // if alphabet is 0 or -1 - not or wrong set
                throw std::logic_error("Input needs a correct alphabet option to be set");
//        option_dependency(argue_container.getVm(), "input", "alphabet");

//comment out as these models throw implementation Error later on error
//        for (auto opt1 : {"DS078", "JTT92", "WAG01"} ) {
//            option_dependency(argue_container.getVm(), opt1, "ap");
//            option_dependency(argue_container.getVm(), opt1, "alphabet", "PROTEIN");
//        }
//        for (auto opt1 : {"GY94", "MG94", "YN98"} ) {
//            option_dependency(argue_container.getVm(), opt1, "ac");
//            option_dependency(argue_container.getVm(), opt1, "alphabet", "CODON");
//        }


        /*
         * Print help options into cmd line
         * */
        if (argue_container.getVm().count("help")) {
            std::cout << cmdline_options << std::endl;      //print group cmdline_options to std::cout
            std::exit(0);
        }

        if (argue_container.getVm().count("version")) {
            std::cout << "SISSI version : " << VERSION << std::endl
                      << std::endl << "Possible usages listed underneath"  << std::endl << std::endl
                      << cmdline_options << std::endl;      //print group all to std::cout
            std::exit(0);
        }

    } // end try
    catch(std::exception& e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    } // end catch

} // end parse_arguements