
#ifndef SISSI_2_0_INITIALIZE_H
#define SISSI_2_0_INITIALIZE_H

#include <iostream>
#include <string>
#include <vector>
#include <exception>

#include "Required_Bio++_Headers.h"
#include "commandline_parser.h"
#include "file_parser/dot_bracket_reader.h"
#include "file_parser/ct_reader.h"
#include "file_parser/stockholm_reader.h"
#include "file_parser/nei_cnei_reader.h"
#include "models/modelstates.h"
#include "models/estimation_reconstruction.h"
#include "numeric/abstract_prng.h"
#include "numeric/rng_jkiss.h"
#include "numeric/rng_mt19337_64.h"

class DATA_COLLECTOR{
private:
    /*
     * bool values to check wether steps are correct initialized
     * */
    bool neighborhood_inited = false;
    bool tree_inited = false;
    bool sequence_inited = false;
    bool model_inited = false;
    bool model_parameter_inited = false;

    bpp::Alphabet *alpha; //initialized in init_alpha()
    int alpha_state_int = -1; // needed for init_model alpha switch
    bpp::VectorSiteContainer *sites; //initialized in init_alpha()
    bpp::VectorSiteContainer *simulatedsites; //initialized in init_alpha()
    bpp::Sequence *ancestralSequence; // holds the ancestral sequence which is needed later for simulation start

    ABSTRACT_PRNG *randomfactory; // PRNG

    bpp::Tree *tree; // hold tree data
    STRUCTURE structure; // saved connections from and to site
    bool ModelOptimizationMethod = false; // false = Brent (default) true = Newton
    bool SeqRecoMeth = true; //Sequence Reconstruction Method  // (marginal reconstruction [=true] or maximum posterior probability [false] )
    bool random_ancester_secquence = true; //produces a random ancester sequence from frequencies

    MODEL modelstates; // holds matrices and modelparameter
    bpp::SubstitutionModel *model; // needed for estimation and reconstruction
    std::string randomname; // PRNG name from Arguments
    std::string outputpath; // path to which output should be written

    /*
     * sets flags for printing additional output
     * */
    bool TEST_MODE = false;
    bool PrintNormalizedFrequencies = false;
    bool PrintSubstitutioMatrices = false;
    bool PrintAncestralSequnece = false;
    bool PrintInternalNodes = true;
    bool PrintSeedValue = false;
    bool CountFromAncester = false;
    bool PrintSubstitution = true;
    bool PrintTime = false;

public:
    DATA_COLLECTOR() = default;
    virtual ~DATA_COLLECTOR() = default;

/******************************************************************************
 * INITIALIZING STATES
 * */
    /*
     * reads frequencies from ARGUMENTS container normalizes and stores them in MODEL class
     * */
    void init_frequencies(ARGUMENTS &argues);
    /*
     * reads alphabet from ARGUMENTS container generates corresponding bpp::Alphabet and stores it
     * */
    void init_alphabet();
    /*
     * reads neighbourhood file from disc and store parsable information
     * */
    void init_neighborhood(const std::string &neighpath);
    /*
     * reads tree file from disc and store parsable information
     * */
    void init_tree(const std::string &path);
    /*
     * reads sequence file from disc and store readable information
     * */
    bool init_sequence(const std::string&);
    /*
     * creates new PRNG corresponding to member var randomname, creates seed value and starts generator,
     * or takes parsed seed value if non zero
     * */
    void init_random_factory(uint64_t seed);
    /*
     * reads model parameter information from ARGUMTES and store them in MODEL class
     * */
    bool init_model_parameter(ARGUMENTS &argues);
    /*
     * instantiate a bpp::substitutionmodel if needed for estimation / reconstruction purposes
     * */
    void init_SubstitutionModel();
    /*
     * creates a random sequence with frequencies from MODEL class, by multipling shuffling and positional assignment
     * */
    bpp::Sequence *create_random_ancester_sequence ();
    /*
     * checks if path is writable
     * */
    void init_output_writeable(const std::string &outpath);
    /*
     * sets estimated model parameter from bpp::substitutionmodel back to MODEL class
     * */
    void set_model_parameter_in_modelstates();
    /*
     * cumulative init function is the major functin of class which calls all other and
     * sets corresponding member var's
     * */
    void init_variablestates(ARGUMENTS &argues){
        // set additional print options
        TEST_MODE = argues.getbool("test_mode");
        PrintNormalizedFrequencies = argues.getbool("frequency");
        PrintSubstitutioMatrices = argues.getbool("matrix");
        PrintAncestralSequnece = argues.getbool("sequence");
        PrintInternalNodes = argues.getbool("nodes");
        PrintSeedValue = argues.getbool("print_seed");
        CountFromAncester = argues.getbool("count_ancester");
        PrintSubstitution = argues.getbool("substitution");
        PrintTime = argues.getbool("time");
        random_ancester_secquence = argues.getbool("ras");

        // get checked states from ARGUMENTS and set them in shape
        randomname = argues.getRandomname();
        init_random_factory( argues.getSeed_value() );

        alpha_state_int = argues.getAlphabet(); // need for switch in init_model
        // set Model name in class
        modelstates.setName( argues.getModelname() );
        //set Model Optimization Method
        ModelOptimizationMethod = argues.getbool("paom");
        //set Sequence Reconstruction Method
        SeqRecoMeth = argues.getbool("remeth");
                    if (TEST_MODE){
                        std::cout << "Found Randomgenerator option for: " << randomname << std::endl
                                  << "Found Alphabetstate option (-1 - not allowed; 1 - RNA; 2 - DNA, 3 - Protein; 4 - Binary; 5 - Codon): "
                                  << alpha_state_int << std::endl
                                  << "Found Modelname option for: " << modelstates.getName() << std::endl
                                  << "Found Modeloptimazation method option: " << ModelOptimizationMethod << std::endl
                                  << "Found Sequence Reconstruction Method option: " << SeqRecoMeth << std::endl;
                    }
        // instantiate correct alphabet
        init_alphabet();
                    if (TEST_MODE){
                        std::cout << "Initialized alphabet-type: " << alpha->getAlphabetType() << std::endl
                                  << std::endl;
                    }
        // if neighbourhood option is given
        if (!argues.getVm()["neighbourhood"].empty() ) {
            init_neighborhood(argues.get_std_string("neighbourhood"));
        } else {
            throw std::underflow_error("Neighbourhood option not found");
        }
                    if (TEST_MODE){
                        std::cout << "Initialize neighbourhood..." << std::endl << std::endl
                                  << "Fileformat to read: " << argues.get_std_string("neighbourhood").substr(argues.get_std_string("neighbourhood").find_last_of('.') + 1) << std::endl
                                  << "Is neighborhood initialized? : " << neighborhood_inited << std::endl
                                  << "Parsed structure length of: " << structure.getParsed_lenght() << std::endl
                                  << "In total connections are found: " << structure.get_size() << std::endl
                                  << "Position 3 is connected to: " << structure.getValue(3) << std::endl
                                  << "Position 22 is connected to: " << structure.getValue(22) << std::endl
                                  << "Is Tree initialized? : " << tree_inited << std::endl;
                        if (tree_inited)
                            std::cout << "Number of Nodes / number of leaves are found: " << tree->getNumberOfNodes() << " / " << tree->getNumberOfLeaves() << std::endl;

                        std::cout << "Is Sequence initialized? : " << sequence_inited << std::endl;
                        if (sequence_inited) {
                            std::cout << "Number of Sequences read: " << sites->getNumberOfSequences() << std::endl
                                      << "Sequncelength: " << sites->getNumberOfSites() << std::endl;
                            for (auto i= 0; i < sites->getNumberOfSequences(); i++){
                                std::cout << sites->getSequence(i).toString() << std::endl;
                            }
                        }
                    } // end if TESTMODE
        //read tree
        if ( tree_inited && !argues.getVm()["treefile"].empty() ) {
            throw std::overflow_error("A second tree option was found but already parsed one from file");
        }
        if ( !tree_inited && !argues.getVm()["treefile"].empty() ) {
            init_tree(argues.get_std_string("treefile") );
        }
        if ( !tree_inited && argues.getVm()["treefile"].empty() ) {
            throw std::logic_error("Treefile option not found");
        }
                    if (TEST_MODE){
                        if ( !argues.getVm()["treefile"].empty() ) {
                            std::cout << "Initialize tree..." << std::endl << std::endl
                                      << "Fileformat to read: " << argues.get_std_string("treefile").substr(argues.get_std_string("treefile").find_last_of('.') + 1) << std::endl
                                      << "Is Tree initialized? : " << tree_inited << std::endl;
                            if (tree_inited)
                                std::cout << "Number of Nodes / number of leaves are found: " << tree->getNumberOfNodes() << " / " << tree->getNumberOfLeaves() << std::endl;

                            std::cout << "Is Sequence initialized? : " << sequence_inited << std::endl;
                            if (sequence_inited) {
                                std::cout << "Number of Sequences read: " << sites->getNumberOfSequences() << std::endl
                                          << "Sequncelength: " << sites->getNumberOfSites() << std::endl;
                                for (auto i= 0; i < sites->getNumberOfSequences(); i++){
                                    std::cout << sites->getSequence(i).toString() << std::endl;
                                }
                            }
                        } // end if treefile
                    } // end if TESTMODE
        // read sequence data
        if ( sequence_inited && !argues.getVm()["input"].empty() ) {
            throw std::overflow_error("A second sequence set was found - already parsed one from file");
        }
        if ( !sequence_inited && !argues.getVm()["input"].empty() ) {
            sequence_inited = init_sequence(argues.get_std_string("input") );
            init_frequencies(argues);
        }
                    if (TEST_MODE){
                        if ( argues.getVm().count("input") ) {
                            std::cout << "Initialize input..." << std::endl << std::endl;
                            std::cout << "Fileformat to read: " << argues.get_std_string("input").substr(argues.get_std_string("input").find_last_of('.') + 1) << std::endl;
                            if (sequence_inited) {
                                std::cout << "Number of Sequences read: " << sites->getNumberOfSequences() << std::endl
                                          << "Sequncelength: " << sites->getNumberOfSites() << std::endl;
                                for (auto i= 0; i < sites->getNumberOfSequences(); i++){
                                    std::cout << sites->getSequence(i).toString() << std::endl;
                                }
                            }
                        } // end if input
                    } // end if TESTMODE
        //init modelparamter if one sequence was read from file
        if ( sequence_inited && sites->getNumberOfSequences() == 1 ) {
            // Can't estimate modelparameters from one sequence
            model_parameter_inited = init_model_parameter(argues);
        }
        // if no sequence create random
        if (!sequence_inited) {
            init_frequencies(argues);
                    if (TEST_MODE){
                        modelstates.print_frequencies();
                    } // end if TESTMODE
            modelstates.normalize_frequencies();
            if (PrintNormalizedFrequencies) {
                modelstates.print_frequencies();
            }
                    if (TEST_MODE){
                        modelstates.print_frequencies();
                    } // end if TESTMODE

            // not checked to existance as to many cases
            //read model parameter from container and set them
            model_parameter_inited = init_model_parameter(argues);
                    if (TEST_MODE){
                        modelstates.print_modelparameter();
                    }
            structure.InitNk(); // create vector with nk values for each position
                    if (TEST_MODE){
                        std::map<int, int> n;
                        for (auto i:structure.getNk()){
                            ++n[i];
                        }
                        for(auto p : n) {
                            std::cout << "nk" << p.first << "\t" << p.second << "times\n";
                        }
                    } // end if testmode
            ancestralSequence = create_random_ancester_sequence();
            if (PrintAncestralSequnece) {
                PrintAncestralSequence();
            }
        } // end if !sequence_inited

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Hopfully all states are set for simulation continue checking output
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //set output member variable and test if path is writeable else clear variable to ensure writing to cout
        if ( !argues.getVm()["output"].empty() ) {
            init_output_writeable(argues.get_std_string("output") );
        } else {
            // clear string so it can be checked to emptiness in write_output() regarding option of std::cout
            outputpath.clear();
        }
                    if (TEST_MODE){
                        std::cout << "Initialize output..." << std::endl << std::endl;
                        if (outputpath.empty()) {
                            std::cout << "Filepath to write in : std::cout " << std::endl << std::endl;
                        } else {
                        std::cout << "Filepath to write in : " << outputpath << std::endl << std::endl;
                        }
                    }

        //check init states are all correct
        if (neighborhood_inited && sequence_inited && tree_inited) {
            // check if sequence and structure have the same length
            if (sites->getNumberOfSites() == structure.getParsed_lenght()) {
                structure.InitNk();
                    if (TEST_MODE){
                        std::map<int, int> n;
                        for (auto i:structure.getNk()){
                            ++n[i];
                        }
                        for(auto p : n) {
                            std::cout << "nk" << p.first << "\t" << p.second << "times\n";
                        }
                    } // end if testmode
            } else {
                throw std::logic_error("Structure and Sequence don't have the same length!");
            }
        } else if (neighborhood_inited && !sequence_inited && tree_inited) {
            if (ancestralSequence->size() == structure.getParsed_lenght()) {
            } else {
                throw std::logic_error("Structure and ancestral sequence don't have the same length!");
            }
        } else {
            if (!neighborhood_inited)
                throw std::logic_error("ERRORS occurred while data initializing neighborhoodsystem");
            else if (!tree_inited)
                throw std::logic_error("ERRORS occurred while data initializing tree");
            else
                throw std::logic_error("ERRORS occurred while data colletion");
        }
    }; //end init_everything

/******************************************************************************/
    /*
     * write simulated sequences to cout or file
     * */
    void write_output();
    /*
     * prints constructed ancestral to cout
     * */
    void PrintAncestralSequence(){
        std::cout << "Ancestral Sequence:" << '\t' << ancestralSequence->toString() << std::endl;
    };
/******************************************************************************
 * SETTER AND GETTERS
 * */
    bpp::Alphabet *getAlpha() const {
        return alpha;
    }

    bpp::VectorSiteContainer *getSites() const {
        return sites;
    }

    bpp::SiteContainer *getCompleteSites() const {
        bpp::SiteContainer *completeSites = bpp::SiteContainerTools::getSitesWithoutGaps( *sites );
        return completeSites;
    }

    bpp::Sequence *getAncestralSequence() const {
        return ancestralSequence;
    }

    void setAncestralSequence(bpp::Sequence *ancestralSequence) {
        DATA_COLLECTOR::ancestralSequence = ancestralSequence;
    }

    void addSimulatedSequence(bpp::Sequence *sequence) {
        DATA_COLLECTOR::simulatedsites->addSequence(*sequence, false);
    }

    bpp::Tree *getTree() const {
        return tree;
    }

    bpp::SubstitutionModel *getModel() const {
        return model;
    }

    MODEL &getModelstates() { //not const as called to set parameter in object
        return modelstates;
    }

    STRUCTURE &getStructure() { //not const as called to set parameter in object
        return structure;
    }

    ABSTRACT_PRNG *getRandomfactory() const {
        return randomfactory;
    }

    bool isSequence_inited() const {
        return sequence_inited;
    }

    bool isModel_parameter_inited() const {
        return model_parameter_inited;
    }

    bool isModelOptimizationMethod() const {
        return ModelOptimizationMethod;
    }

    bool isSeqRecoMeth() const {
        return SeqRecoMeth;
    }

    bool isRandom_ancester_secquence() const {
        return random_ancester_secquence;
    }

    bool isPrintNormalizedFrequencies() const {
        return PrintNormalizedFrequencies;
    }

    bool isPrintSubstitutioMatrices() const {
        return PrintSubstitutioMatrices;
    }

    bool isPrintAncestralSequnece() const {
        return PrintAncestralSequnece;
    }

    bool isPrintInternalNodes() const {
        return PrintInternalNodes;
    }

    bool isTEST_MODE() const {
        return TEST_MODE;
    }

    bool isCountFromAncester() const {
        return CountFromAncester;
    }

    bool isPrintSubstitution() const {
        return PrintSubstitution;
    }

    bool isPrintTime() const {
        return PrintTime;
    }
}; //end class DATA_COLLECTOR

#endif //SISSI_2_0_INITIALIZE_H