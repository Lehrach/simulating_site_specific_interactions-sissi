//
// Created by michael on 16.03.18.
//

#ifndef SISSI_2_0_SIMULATE_H
#define SISSI_2_0_SIMULATE_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include <random>
#include "models/estimation_reconstruction.h"
#include "models/substitutionmatrices.h"
#include "numeric/abstract_prng.h"
#include "numeric/rng_jkiss.h"
#include "numeric/rng_mt19337_64.h"
#include "data_collector.h"


class SIMULATE {
private:
    DATA_COLLECTOR collector; // information container
    SEQ_ESTIMATION seq_estimation; // container with methods for parameter estimating and sequence reconstruction
    SUBSTITUTIONMATRIX submatrix; //substitutionmatrices

    ABSTRACT_PRNG *randomfactory; // PRNG from DATA_COLLECTOR class

    std::vector<double> Qk_sk_sk_values; // Qk(sk,sk) values for each position
    double diff_Qk_sk_sk_value = 0; // difference from old to new QK(sk,sk) values for update q_x_
    double q_x_; // sum of Qk(sk,sk) values
    std::vector<double> P_k_; // site probabilities for mutation
    double P_k_sum_correction = 0; // correction value for randomness as P(k) sum may be a small bit less or more than one
    std::map<int, double> P_xk_y0_; // probability of change to letter saved as matrix index and P value
    double dc = 0; // current time
    double dr = 0; // time spend on actual mutation
    long int site_k = -1; // site for actual mutation
    size_t seq_length; // length of sequence
    std::vector<int> nk; // number of connection for each site

public:
    // constructor
    explicit SIMULATE(DATA_COLLECTOR *inited) : collector(*inited), q_x_(0) {
        randomfactory = collector.getRandomfactory();
    }
    // destructor
    virtual ~SIMULATE() = default;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     * counts the frequencies from alignment and sets them in MODEL class which is stored in DATA_COLLECTOR
     * */
    void count_frequencies();
    /*
     * query Qk values along actual sequence considering structure
     * */
    void retrieve_Qk_sk_sk_values(bpp::Sequence* sequence, STRUCTURE &structure);
    /*
     * sum Qk(sk,sk) up to retrieve q(x)
     * */
    void sum_Qk_sk_sk_values();
    /*
     * Calculate P(k) values for each position
     * */
    void calculate_Pk_values(std::vector<double> asterix_rates);
    /*
     * draw a random site with P(k) weighted positions
     * */
    void draw_site();
    /*
     * draw a random position with P(xk->y0) weight
     * */
    int draw_position();
    /*
     * retrieve new time by drawing randimly from exponential distribution with lambda = q(x)
     * */
    double draw_from_exponential_distribution(double lambda) {
        /*
         * for x >= 0
         * F(x) = Integral 0 -> x f lambda (t) dt = 1 - e^-lambda*x
         * for x < 0
         * F(x) = 0
         */
        double F_x_ = 1 - randomfactory->drawNumber();
        double x = std::log(F_x_) / -lambda;
        return x;
    } // end draw_from_exponential_distribution
    /*
     * replaces position site_k of the sequence
     * */
    void replace_xk_y0(bpp::Sequence *sequence);
    /*
     * update data for next run in while loop
     * */
    void update_data(bpp::Sequence *sequence, STRUCTURE structure);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     * after all information gather in DATA_COLLECTOR - data is checked if parameter estimaion is needed or not
     * if so also reconstruction of ancestral sequence is done
     * */
    void check_matter_of_estimiation(){
        if (collector.isSequence_inited() ) {
            if ( collector.getSites()->getNumberOfSequences() == 1 && collector.isModel_parameter_inited()){
                        if (collector.isTEST_MODE()){
                            std::cout << "ONE SEQUENCE and MODEL PARAMETER found" << std::endl;
                        }
                if (collector.isCountFromAncester()){
                    count_frequencies();
                }
                if (collector.isRandom_ancester_secquence()){
                    collector.setAncestralSequence(collector.create_random_ancester_sequence());
                } else {
                    collector.setAncestralSequence( new bpp::BasicSequence( collector.getSites()->getSequence(0) ) );
                }
                if (collector.isPrintAncestralSequnece()) {
                    collector.PrintAncestralSequence();
                }

                submatrix.set_matrix_parameters(collector.getModelstates());
                if (collector.isPrintSubstitutioMatrices()) {
                    submatrix.PrintSubstitutionMatrix();
                }
                        if (collector.isTEST_MODE()){
                            submatrix.PrintSubstitutionMatrix();
                        }
            } else if (collector.getSites()->getNumberOfSequences() == 1 && !collector.isModel_parameter_inited() ) {
                    throw std::logic_error( "Got 1 Sequence but no modelparameters. For parameter estimation more than one sequence is needed");
            } else {
                        if (collector.isTEST_MODE()){
                            std::cout << "do estimation..." << std::endl;
                        }
                estimate();
                        if (collector.isTEST_MODE()){
                            collector.getModelstates().print_modelparameter();
                            submatrix.PrintSubstitutionMatrix();
                        }
            } // end if else
        } else {
            if ( collector.isModel_parameter_inited() ){
                        if (collector.isTEST_MODE()){
                            std::cout << "Random SEQUENCE and MODEL PARAMETER found" << std::endl;
                        }
                submatrix.set_matrix_parameters(collector.getModelstates());
                if (collector.isPrintSubstitutioMatrices()) {
                    submatrix.PrintSubstitutionMatrix();
                }
                        if (collector.isTEST_MODE()){
                            submatrix.PrintSubstitutionMatrix();
                        }
            } else {
                throw std::logic_error("Expected to retrieve parameter for substitution models but got non");
            } // end if else
        } // end if else
    } // end need of estimation
    /*
     * estimates modelparametere and reconstruct ancestral sequence from alignment data
     */
    void estimate() {
        count_frequencies();
        collector.init_SubstitutionModel();

        seq_estimation.estimate_model_parameter(collector.getAlpha(), collector.getTree(), collector.getCompleteSites(),
                                                collector.getModel(), collector.isModelOptimizationMethod());
        if (collector.isRandom_ancester_secquence()){
            collector.setAncestralSequence(collector.create_random_ancester_sequence());
        } else {
            collector.setAncestralSequence( seq_estimation.reconstruct_ancestral(collector.getTree(), collector.getCompleteSites(), collector.getModel(), collector.isSeqRecoMeth() ) );
        }
        if (collector.isPrintAncestralSequnece()) {
            collector.PrintAncestralSequence();
        }
        // sets estimated parameters in data_collector for next usage
        collector.set_model_parameter_in_modelstates();
        submatrix.set_matrix_parameters(collector.getModelstates());
        if (collector.isPrintSubstitutioMatrices()) {
            submatrix.PrintSubstitutionMatrix();
        }
    } // end estimate

     void init_simulation(){
         seq_length = collector.getAncestralSequence()->size();
         nk = collector.getStructure().getNk();
         Qk_sk_sk_values.resize(collector.getAncestralSequence()->size());
         retrieve_Qk_sk_sk_values(collector.getAncestralSequence(), collector.getStructure());
         P_k_.resize(Qk_sk_sk_values.size());
         sum_Qk_sk_sk_values();
         calculate_Pk_values(Qk_sk_sk_values);

         dc = dr = draw_from_exponential_distribution(q_x_);

                        if (collector.isTEST_MODE()){
                            std::cout << std::setprecision(3) << std::fixed;
                            std::cout << "Ancestral Sequence: " << collector.getAncestralSequence()->toString() << std::endl
                                      << "Sequence lenght: " << seq_length << std::endl
                                      << "dr: " << dr << std::endl
                                      << "q(x): " << q_x_ << std::endl
                                      << "Qk_sk_sk values: " << std::endl;
                            for (int i = 0; i < Qk_sk_sk_values.size(); i++){
                                std::cout << Qk_sk_sk_values[i] << '\t';
                            } std::cout << std::endl;
                            std::cout << "P(k): " << std::endl;
                            for (int i = 0; i < P_k_.size(); i++){
                                std::cout << P_k_[i] << '\t';
                            } std::cout << std::endl << std::defaultfloat;
                        }
    } // end init_simulation

    bpp::Sequence *simulate(const double distance, const bpp::Sequence *sequence) {
        int substitution_counter = 0;
        bpp::Sequence *newsequence = sequence->clone();
        //bpp::Sequence *beforemutation = sequence->clone();

        while (dc < distance ){
            substitution_counter ++;
            draw_site();
            replace_xk_y0( newsequence );
//                        comment out because output is to huge
//                        if(collector.isTEST_MODE() ){
//                            std::cout << "site of mutation: " << site_k << '\t'
//                                      << "q(x) " << q_x_  << '\t'
//                                      << "dr " << dr << '\t'
//                                      << "Seq Char " << beforemutation->getChar(site_k) << '\t'
//                                      << "New Char " << newsequence->getChar(site_k) << '\t'
//                                       << "Nk[site] " << nk[site_k] << "\t" << "Neighbour "
//                                      << collector.getStructure().getValue(site_k);
//                            if (beforemutation->getChar(site_k) != newsequence->getChar(site_k) ) {
//                                std::cout << std::endl;
//                            } else {
//                                std::cout << "<<==" << std::endl;
//                            }
//                                      << "P(xk->y0): "
//                                      << std::endl;
//                            for (auto it = P_xk_y0_.begin(); it != P_xk_y0_.end(); ++it){
//                                std::cout << it->second << '\t';
//                            } std::cout << std::endl;
//                        beforemutation = newsequence->clone();
//                        } // end if TESTMODE
            update_data(newsequence, collector.getStructure());
        } // end while

        //reset data
        dc = dr;

                         if(collector.isTEST_MODE() || collector.isPrintSubstitution() ) {
                             std::cout << "Number of substitutions: " << substitution_counter << '\t'
                                       << "Substitions per site: "
                                       << static_cast<double>(substitution_counter)/sequence->size() << std::endl;
                             int counter = 0;
                             for (auto index =0; index < sequence->size(); index++) {
                                 if (sequence->getValue(index) != newsequence->getValue(index) )
                                     counter++;
                             }
                             std::cout << "Hammingdistance: " << counter << '\t'
                                       << "normalized to sequence length: "
                                       << static_cast<double>(counter)/sequence->size() << std::endl;
                         }
        //delete beforemutation;
        return newsequence;
    } // end Simulate

    void simulate_childs_along_tree(const int fathers_node_ID, const bpp::Sequence *fatherssequence){
        for ( auto child : collector.getTree()->getSonsId(fathers_node_ID) ) {
            bpp::Sequence *childsequence = simulate( collector.getTree()->getDistanceToFather(child), fatherssequence );
            // if print internal add all else select leaves and add just them
            if (collector.isPrintInternalNodes() ) {
                //correct Sequencename
                if (collector.getTree()->isLeaf(child) ){
                    childsequence->setName( collector.getTree()->getNodeName(child) );
                    collector.addSimulatedSequence(childsequence);
                } else {
                    childsequence->setName("InnerNodeID-"+std::to_string(child) );
                    collector.addSimulatedSequence(childsequence);
                }// end if
            } else {
                if (collector.getTree()->isLeaf(child) ){
                    childsequence->setName( collector.getTree()->getNodeName(child) );
                    collector.addSimulatedSequence(childsequence);
                } // end if
            } // end if / else
            // recusrion call
            simulate_childs_along_tree( child, childsequence);
            delete childsequence;
        } // end for
    } // end simulate_along_tree

    void simulate_all() {
        if ( collector.isPrintAncestralSequnece() ){
            collector.addSimulatedSequence(collector.getAncestralSequence() );
        }
        //start_point for recursion
        simulate_childs_along_tree(collector.getTree()->getRootId(), collector.getAncestralSequence() );
    } // end simulate_all

}; // end class SIMULATE


#endif //SISSI_2_0_SIMULATE_H
