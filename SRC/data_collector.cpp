
#include "data_collector.h"

void DATA_COLLECTOR::init_frequencies(ARGUMENTS &argues) {
    //single_frequencies
    if ( !argues.getVm()["single"].empty() ){
        modelstates.setPiSingleFrequencies(argues.get_Nukleotidfrequencies("single", 4) );
    } else {
        modelstates.setPiSingleFrequencies( {0.25, 0.25, 0.25, 0.25} );
    } // end if single
    //double_frequencies
    if ( !argues.getVm()["double"].empty() ){
        modelstates.setPiDoubleFrequencies(argues.get_Nukleotidfrequencies("double", 16) );
    } else {
        modelstates.setPiDoubleFrequencies( {0.0625, 0.0625, 0.0625, 0.0625,
                                             0.0625, 0.0625, 0.0625, 0.0625,
                                             0.0625, 0.0625, 0.0625, 0.0625,
                                             0.0625, 0.0625, 0.0625, 0.0625} );
    } // end if double
    //triple_frequencies
    if ( !argues.getVm()["triple"].empty() ){
        modelstates.setPiTripleFrequencies(argues.get_Nukleotidfrequencies("triple", 64) );
    } else {
        modelstates.setPiTripleFrequencies( {0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                                             0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                                             0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                                             0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                                             0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                                             0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                                             0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                                             0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625} );
    } // end if triple
} // end init_frequencies

bool DATA_COLLECTOR::init_model_parameter(ARGUMENTS &argues){
    if (modelstates.getName() == "JC69"){
        //do nothing
        return true;
    }else if (modelstates.getName() == "K80"){
        if (!argues.getVm()["kappa1"].empty() && !argues.getVm()["kappa2"].empty() ) {
            modelstates.setKappa( argues.getdouble("kappa1") + argues.getdouble("kappa2") );
            return true;
        } else if ( argues.getVm()["kappa1"].empty() && !argues.getVm()["kappa2"].empty() ) {
            modelstates.setKappa( argues.getdouble("kappa2") );
            return true;
        } else if ( !argues.getVm()["kappa1"].empty() && argues.getVm()["kappa2"].empty() ) {
            modelstates.setKappa( argues.getdouble("kappa1") );
            return true;
        } else {
            throw std::logic_error(modelstates.getName()+" needs parameter kappa1 and / or kappa2");
        }
    } else if (modelstates.getName() == "F81"){
        //do nothing
        return true;
    } else if (modelstates.getName() == "HKY85") {
        if (!argues.getVm()["kappa1"].empty() && !argues.getVm()["kappa2"].empty() ) {
            modelstates.setKappa( argues.getdouble("kappa1") + argues.getdouble("kappa2") );
            return true;
        } else if ( argues.getVm()["kappa1"].empty() && !argues.getVm()["kappa2"].empty() ) {
            modelstates.setKappa( argues.getdouble("kappa2") );
            return true;
        } else if ( !argues.getVm()["kappa1"].empty() && argues.getVm()["kappa2"].empty() ) {
            modelstates.setKappa( argues.getdouble("kappa1") );
            return true;
        } else {
            throw std::logic_error(modelstates.getName()+" needs parameter kappa1 and / or kappa2");
        }
    } else if (modelstates.getName() == "T92") {
        if (!argues.getVm()["kappa1"].empty() && !argues.getVm()["kappa2"].empty() ) {
            modelstates.setKappa( argues.getdouble("kappa1") + argues.getdouble("kappa2") );
            return true;
        } else if ( argues.getVm()["kappa1"].empty() && !argues.getVm()["kappa2"].empty() ) {
            modelstates.setKappa( argues.getdouble("kappa2") );
            return true;
        } else if ( !argues.getVm()["kappa1"].empty() && argues.getVm()["kappa2"].empty() ) {
            modelstates.setKappa( argues.getdouble("kappa1") );
            return true;
        } else {
            throw std::logic_error(modelstates.getName()+" needs parameter kappa1 and / or kappa2");
        }
    } else if (modelstates.getName() == "TN93") {
        if (!argues.getVm()["kappa1"].empty() && !argues.getVm()["kappa2"].empty() ) {
            modelstates.setKappa1( argues.getdouble("kappa1") );
            modelstates.setKappa2( argues.getdouble("kappa2") );
            return true;
        } else {
            throw std::logic_error(modelstates.getName()+" needs parameter kappa1 and kappa2");
        }
    } else if (modelstates.getName() == "GTR") {
        if (!argues.getVm()["GTRa"].empty() && !argues.getVm()["GTRb"].empty() && !argues.getVm()["GTRc"].empty()  &&
                !argues.getVm()["GTRd"].empty() && !argues.getVm()["GTRe"].empty() && !argues.getVm()["GTRf"].empty() ) {
            //normalize GTRf to 1.0
            if (argues.getdouble("GTRf") != 1.0 ){
                modelstates.setGTRa( argues.getdouble("GTRa") / argues.getdouble("GTRf") );
                modelstates.setGTRb( argues.getdouble("GTRb") / argues.getdouble("GTRf") );
                modelstates.setGTRc( argues.getdouble("GTRc") / argues.getdouble("GTRf") );
                modelstates.setGTRd( argues.getdouble("GTRd") / argues.getdouble("GTRf") );
                modelstates.setGTRe( argues.getdouble("GTRe") / argues.getdouble("GTRf") );
            } else {
                modelstates.setGTRa( argues.getdouble("GTRa") );
                modelstates.setGTRb( argues.getdouble("GTRb") );
                modelstates.setGTRc( argues.getdouble("GTRc") );
                modelstates.setGTRd( argues.getdouble("GTRd") );
                modelstates.setGTRe( argues.getdouble("GTRe") );
            }
            return true;
        } else {
            throw std::logic_error(modelstates.getName()+" needs parameter GTRa, GTRb, GTRc, GTRd, GTRe and GTRf");
        }
    } else {
        throw std::logic_error("Model does not exist - Please check manual for correct use.");
    }

} // end set_parsed_model_parameter

void DATA_COLLECTOR::init_alphabet() {
    switch (alpha_state_int) {
        case 1: // ==>> default value as set in command line parser
            alpha = new bpp::RNA(); //bpp::NucleicAlphabet
            break;
        case 2:
            alpha = new bpp::DNA(); //bpp::NucleicAlphabet
            break;
        case 3:
            // todo implement logic for models - commented out as no Substitutionmatrix and so on are actually implemented
            throw std::runtime_error("Protein Models are actually not supported / implemented");
            alpha = new bpp::ProteicAlphabet; //bpp::ProteicAlphabet
            break;
        case 4:
            // todo implement logic for models - commented out as no Substitutionmatrix and so on are actually implemented
            throw std::logic_error("For Binary Alphabet are actually no models supported / implemented");
            alpha = new bpp::BinaryAlphabet; //bpp::BinaryAlphabet
            break;
        case 5:
            // todo implement logic for models - commented out as no Substitutionmatrix and so on are actually implemented
            throw std::runtime_error("Codon Models are actually not supported / implemented");
            alpha = new bpp::CodonAlphabet(&bpp::AlphabetTools::RNA_ALPHABET);
            break;
        default:
            throw std::logic_error("Can not initialize the alphabet state");
    }

    sites = new bpp::VectorSiteContainer( alpha );
    simulatedsites = new bpp::VectorSiteContainer( alpha );

}; //end init_alphabet

void DATA_COLLECTOR::init_neighborhood(const std::string &neighpath){
    std::string file_format = neighpath.substr(neighpath.find_last_of('.') + 1);

    int index_of_parsed_element = 0;

    try {
        //if inputfile is a DOT Bracket file
        if (file_format == "dbn") {
            DOTBRACKED_READER dbnreader(alpha);
            dbnreader.read_dbn_file(neighpath);

            structure = dbnreader.getStructure(index_of_parsed_element);
            if ( structure.get_size() != 0 ){
                neighborhood_inited = true;
            } else if (structure.getParsed_lenght() != 0) {
                neighborhood_inited = true;
            } else {
                throw std::logic_error("There was no structure to parse");
            }
            // initialize sequence from file
            if ( dbnreader.getSequence_size() != 0 ){
                for (int i=0; i<dbnreader.getSequence_size(); i++ ){
                    bpp::BasicSequence sequence(dbnreader.getComment(i), dbnreader.getSequence(i), alpha);
                    sites->addSequence(sequence, false);
                }
                sequence_inited = true ;
            } // end if sequences
        }
        //if inputfile is a CT file
        else if (file_format == "ct" || file_format == "ct2") {
            CT_READER ctreader;
            ctreader.read_ct_file(neighpath);
            structure = ctreader.getStructure(index_of_parsed_element);
            if ( structure.get_size() != 0 ){
                neighborhood_inited = true;
            } else if (structure.getParsed_lenght() != 0) {
                neighborhood_inited = true;
            } else {
                throw std::logic_error("There was no structure to parse");
            }
            // initialize sequence from file
            if ( ctreader.get_int_found_seqs() != 0 ){
                for (int i=0; i<ctreader.get_int_found_seqs(); i++ ){
                    bpp::BasicSequence sequence(ctreader.getComment(i), ctreader.getSequence(i), alpha);
                    sites->addSequence(sequence, false);
                }
                sequence_inited = true ;
            }
        }
        //if inputfile is a CNEI file
        else if (file_format == "cnei") {
            NEI_CNEI_READER nei_cnei_reader;
            nei_cnei_reader.setCnei(true); // set format of cnei true
            nei_cnei_reader.read_nei_file(neighpath);
            structure = nei_cnei_reader.getStructure();
            if ( structure.get_size() != 0 ){
                neighborhood_inited = true;
            } else if (structure.getParsed_lenght() != 0) {
                neighborhood_inited = true;
            } else {
                throw std::logic_error("There was no structure to parse");
            }
        }
        //if inputfile is a NEI file
        else if (file_format == "nei") {
            NEI_CNEI_READER nei_cnei_reader;
            nei_cnei_reader.read_nei_file(neighpath);
            structure = nei_cnei_reader.getStructure();
            if ( structure.get_size() != 0 ){
                neighborhood_inited = true;
            } else if (structure.getParsed_lenght() != 0) {
                neighborhood_inited = true;
            } else {
                throw std::logic_error("There was no structure to parse");
            }
        }

        //if inputfile is a STOCKHOLM file
        else if (file_format == "stockholm") {
            STOCKHOLM_READER stock_reader;

            auto stock_sequences = new bpp::VectorSiteContainer( alpha );
            stock_reader.setSiteContainer(stock_sequences);
            stock_reader.setAlpha(alpha);

            stock_reader.read_file(neighpath);

            structure = stock_reader.getStructure();
            if ( structure.get_size() != 0 ){
                neighborhood_inited = true;
            } else if (structure.getParsed_lenght() != 0) {
                neighborhood_inited = true;
            } else {
                throw std::logic_error("There was no structure to parse");
            }

            if (stock_reader.isFound_sequences()){
                sites = stock_reader.getSiteContainer();
                sequence_inited = true;
            }

            if(stock_reader.isFound_tree()) {
                tree = stock_reader.getTree();
                tree_inited = true;
            }
        }

        else if ( !neighborhood_inited ) {
            throw std::logic_error("Can not initialize the neighborhood");
        } else {
            throw std::logic_error("Can not find valid file format for initialize the neighborhood from file");
        }

    } catch (bpp::Exception & e) {
        std::cerr << "ERROR! " << e.what() << std::endl;
        std::exit(1);
    } catch(std::exception& e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    } catch (...) {
        std::cerr << "undefined errors occured while neighborhood input" << std::endl;
        std::exit(1);
    } // end try
}; // end init_neighborhood

void DATA_COLLECTOR::init_tree(const std::string &path){
    std::string file_format = path.substr(path.find_last_of('.') + 1);
    try {
        //if inputfile is a newick
        if (file_format == "tree" || file_format == "dnd") {
            bpp::Newick *newickReader = new bpp::Newick(true); //comments in tree are allowed
            tree = newickReader->read(path); // Tree in file
            if (tree->getNumberOfNodes() > 0)
                tree_inited = true;
        }
        //if inputfile is a NEW HAMPSHIRE X format
        else if (file_format == "nhx") {
            bpp::Nhx *NhxReader = new bpp::Nhx();
            tree = NhxReader->read(path); // Tree in file
            if (tree->getNumberOfNodes() > 0)
                tree_inited = true;
        }
        //if inputfile is a nexus
        else if (file_format == "nex" || file_format == "nxs" ) {
            bpp::NexusIOTree *NexusTreeReader = new bpp::NexusIOTree;
            tree = NexusTreeReader->read(path); // Tree in file
            if (tree->getNumberOfNodes() > 0)
                tree_inited = true;
            if (!sequence_inited){
                try {
                    bpp::NexusIOSequence *NexusSEQ = new bpp::NexusIOSequence;
                    bpp::SequenceContainer *sequenceContainer = NexusSEQ->readSequences(path, alpha);
                    if (sequenceContainer->getNumberOfSequences() > 0){
                        for (int i=0; i<sequenceContainer->getNumberOfSequences(); i++ ){
                            sites->addSequence(sequenceContainer->getSequence( sequenceContainer->getSequencesNames()[i] ), true);
                        }
                        sequence_inited = true;
                    } // end if number Sequences
                } catch (bpp::Exception & e) {
                    std::cerr << "Can not get sequence(s) from nexus file!" << std::endl;
                } // end try catch
            } // end if read nexus sequence
        } // end if Nexus format

        else if (!tree_inited) {
            throw std::logic_error("Can not initialize a phylogenetic tree for processing");
        } else {
            throw std::logic_error("Can not find valid file format for initialize a phylogenetic tree for processing");
        }
    } catch (bpp::Exception & e) {
        std::cerr << "ERROR! " << e.what() << std::endl;
        std::exit(1);
    } catch(std::exception& e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    } catch (...) {
        std::cerr << "undefined errors occured while tree input" << std::endl;
        std::exit(1);
    } // end try
}; // end init_tree

bool DATA_COLLECTOR::init_sequence(const std::string &i_input_string) {
    std::string file_format = i_input_string.substr(i_input_string.find_last_of('.') + 1);
    try {
        //if inputfile is a fastafile
        if (file_format == "fasta" || file_format == "fa") {
            bpp::Fasta fasta;
            bpp::VectorSequenceContainer *sequenceContainer = fasta.readSequences(i_input_string, alpha);
            if (sequenceContainer->getNumberOfSequences() > 0 ){
                for (size_t i=0; i<sequenceContainer->getNumberOfSequences(); i++){
                    sites->addSequence(sequenceContainer->getSequence(i), true);
                }
                return true;
            }
        }
        //if inputfile is a mase file
        else if (file_format == "mase") {
            bpp::Mase mase;
            bpp::VectorSequenceContainer *sequenceContainer =  mase.readSequences(i_input_string, alpha);
            if (sequenceContainer->getNumberOfSequences() > 0) {
                for (size_t i=0; i<sequenceContainer->getNumberOfSequences(); i++){
                    sites->addSequence(sequenceContainer->getSequence(i), true);
                }
                return true;
            }
        }
        //if inputfile is a clustal file
        else if (file_format == "aln" || file_format == "clu" || file_format == "clustal") {
            bpp::Clustal clustal;
            bpp::SequenceContainer *sequenceContainer = clustal.readSequences(i_input_string, alpha);
            std::cout << sequenceContainer->getNumberOfSequences() << std::endl;
            if (sequenceContainer->getNumberOfSequences() > 0) {
                for (int i=0; i<sequenceContainer->getNumberOfSequences(); i++){
                    sites->addSequence(sequenceContainer->getSequence( sequenceContainer->getSequencesNames()[i] ), true);
                }
                return true;
            }
        }
        //if inputfile is a phylip file
        else if (file_format == "ph" || file_format == "phylip") {
            bpp::Phylip phylip(true, false);
            bpp::SequenceContainer *sequenceContainer =  phylip.readSequences(i_input_string, alpha);
            if (sequenceContainer->getNumberOfSequences() > 0) {
                for (int i=0; i<sequenceContainer->getNumberOfSequences(); i++){
                    sites->addSequence(sequenceContainer->getSequence( sequenceContainer->getSequencesNames()[i] ), true);
                }
                return true;
            }
        }
        //if inputfile is a phylib3 file
        else if (file_format == "ph3") {
            bpp::Phylip phylip3(true, true);
            bpp::SequenceContainer *sequenceContainer =  phylip3.readSequences(i_input_string, alpha);
            if (sequenceContainer->getNumberOfSequences() > 0) {
                for (int i=0; i<sequenceContainer->getNumberOfSequences(); i++){
                    sites->addSequence(sequenceContainer->getSequence( sequenceContainer->getSequencesNames()[i] ), true);
                }
                return true;
            }
        }
        //if inputfile is a genbank file
        else if (file_format == "gb" || file_format == "genbank") {
            bpp::GenBank genbank;
            bpp::VectorSequenceContainer *sequenceContainer =  genbank.readSequences(i_input_string, alpha);
            if (sequenceContainer->getNumberOfSequences() > 0) {
                for (int i=0; i<sequenceContainer->getNumberOfSequences(); i++){
                    sites->addSequence(sequenceContainer->getSequence( sequenceContainer->getSequencesNames()[i] ), true);
                }
                return true;
            }
        }
        //if inputfile is a nexus file
        else if (file_format == "nex" || file_format == "nxs" ) {
            if (!tree_inited){
                bpp::NexusIOTree *NexusTREE = new bpp::NexusIOTree;
                bpp::Tree *nxstree = NexusTREE->read(i_input_string); // Tree in file
                if (nxstree->getNumberOfLeaves() > 0 ) {
                    tree_inited = true;
                    tree = nxstree;
                } else if (nxstree->getNumberOfLeaves() == 0 ) {
                    throw std::runtime_error("No tree was handeld for initiating");
                } else {
                    throw std::runtime_error("Problems while initiating Sequence and or Tree from Nexus file");
                }
            } // end if

            bpp::NexusIOSequence *NexusReader = new bpp::NexusIOSequence;
            bpp::SequenceContainer *sequenceContainer = NexusReader->readSequences(i_input_string, alpha);
            if (sequenceContainer->getNumberOfSequences() > 0) {
                for (int i=0; i<sequenceContainer->getNumberOfSequences(); i++){
                    sites->addSequence(sequenceContainer->getSequence( sequenceContainer->getSequencesNames()[i] ), true);
                }
                return true;
            } // end if
        } // end if nexus file
        else {
            throw std::logic_error("Can not initialize a sequence from file for processing" );
        }

    } catch (bpp::Exception & e) {
        std::cerr << "ERROR! " << e.what() << std::endl;
        std::exit(1);
    } catch(std::exception& e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    } catch (...) {
        std::cerr << "errors occured while sequence input" << std::endl;
        std::exit(1);
    } // end try
} // end init_sequence

void DATA_COLLECTOR::init_random_factory(uint64_t seed){
    if (seed != 0){
        if (randomname == "JKISS") {
            randomfactory = new JKISS2010(seed);
        } else if (randomname == "MT19937") {
            randomfactory = new MT19937(seed);
        }
    } else {
        if (randomname == "JKISS") {
            randomfactory = new JKISS2010();
        } else if (randomname == "MT19937") {
            randomfactory = new MT19937();
        }
    } // end if else

    if ( PrintSeedValue ){
        randomfactory->PrintSeedValue();
    }
} //end init_random_factory

void DATA_COLLECTOR::init_SubstitutionModel(){
    try {
        switch (alpha_state_int) {
            case 1:{// ==>> default value as set in command line parser
                auto *RNAalphabet = new bpp::RNA(); //bpp::NucleicAlphabet
                if (modelstates.getName() == "JC69" && !model_inited){
                    model = new bpp::JCnuc(RNAalphabet);
                    model_inited = true;
                } else if (modelstates.getName() == "K80" && !model_inited){
                    model = new bpp::K80(RNAalphabet, modelstates.getKappa() );
                    model_inited = true;
                } else  if (modelstates.getName() == "F81" && !model_inited){
                    //TODO use GTR with all parameter 1 ? to reconstruct ancestral seq????
                    //used to transfer pi values - NOTE kappa is not used or touched - as not needed for F81 model
                    model = new bpp::F84(RNAalphabet, modelstates.getKappa(), modelstates.getPiA(),
                                                      modelstates.getPiC(), modelstates.getPiG(),
                                                      modelstates.getPiT() );
                    model_inited = true;
                } else  if (modelstates.getName() == "HKY85" && !model_inited) {
                    model = new bpp::HKY85(RNAalphabet, modelstates.getKappa(), modelstates.getPiA(),
                                                        modelstates.getPiC(), modelstates.getPiG(),
                                                        modelstates.getPiT() );
                    model_inited = true;
                } else  if (modelstates.getName() == "T92" && !model_inited) {
                    model = new bpp::T92(RNAalphabet, modelstates.getKappa(), modelstates.getTheta() );
                    model_inited = true;
                } else  if (modelstates.getName() == "TN93" && !model_inited) {
                    model = new bpp::TN93(RNAalphabet, modelstates.getKappa1() , modelstates.getKappa1(),
                                                       modelstates.getPiA(), modelstates.getPiC(),
                                                       modelstates.getPiG(), modelstates.getPiT() );
                    model_inited = true;
                } else  if (modelstates.getName() == "GTR" && !model_inited) {
                    model = new bpp::GTR(RNAalphabet, modelstates.getGTRa(), modelstates.getGTRb(),
                                                      modelstates.getGTRc(), modelstates.getGTRd(),
                                                      modelstates.getGTRe(),
                                                      modelstates.getPiA(), modelstates.getPiC(),
                                                      modelstates.getPiG(), modelstates.getPiT() );
                    model_inited = true;
                } else if (!model_inited) {
                    throw std::logic_error("Can not initialize model - propably wrong alphabet choosen?");
                }
            } break;
            case 2:{
                auto *DNAalphabet = new bpp::DNA(); //bpp::NucleicAlphabet
                if (modelstates.getName() == "JC69" && !model_inited){
                    model = new bpp::JCnuc(DNAalphabet);
                    model_inited = true;
                } else if (modelstates.getName() == "K80" && !model_inited){
                    model = new bpp::K80(DNAalphabet, modelstates.getKappa() );
                    model_inited = true;
                } else  if (modelstates.getName() == "F81" && !model_inited){
                    //used to transfer pi values - NOTE kappa is not used or touched - as not needed for F81 model
                    model = new bpp::F84(DNAalphabet, modelstates.getKappa(), modelstates.getPiA(),
                                         modelstates.getPiC(), modelstates.getPiG(),
                                         modelstates.getPiT() );
                    model_inited = true;
                } else  if (modelstates.getName() == "HKY85" && !model_inited) {
                    model = new bpp::HKY85(DNAalphabet, modelstates.getKappa(), modelstates.getPiA(),
                                           modelstates.getPiC(), modelstates.getPiG(),
                                           modelstates.getPiT() );
                    model_inited = true;
                } else  if (modelstates.getName() == "T92" && !model_inited) {
                    model = new bpp::T92(DNAalphabet, modelstates.getKappa(), modelstates.getTheta() );
                    model_inited = true;
                } else  if (modelstates.getName() == "TN93" && !model_inited) {
                    model = new bpp::TN93(DNAalphabet, modelstates.getKappa1() , modelstates.getKappa1(),
                                          modelstates.getPiA(), modelstates.getPiC(),
                                          modelstates.getPiG(), modelstates.getPiT() );
                    model_inited = true;
                } else  if (modelstates.getName() == "GTR" && !model_inited) {
                    model = new bpp::GTR(DNAalphabet, modelstates.getGTRa(), modelstates.getGTRb(),
                                         modelstates.getGTRc(), modelstates.getGTRd(),
                                         modelstates.getGTRe(),
                                         modelstates.getPiA(), modelstates.getPiC(),
                                         modelstates.getPiG(), modelstates.getPiT() );
                    model_inited = true;
                } else if (!model_inited) {
                    throw std::logic_error("Can not initialize model - probably wrong alphabet choosen?");
                }
            } break;
            case 3:{
                // todo implement logic for models - commented out as no Substitutionmatrix and so on are actually implemented
                throw std::runtime_error("Protein Models are actually not supported / complete implemented");
//                auto *PROTEICalphabet = new bpp::ProteicAlphabet; //bpp::ProteicAlphabet
//                if (modelstates.getName() == "JC69" && !model_inited) {
//                    model = new bpp::JCprot(PROTEICalphabet);
//                    model_inited = true;
//                } if (modelstates.getName() == "DS078" && !model_inited) {
//                    model = new bpp::DSO78(PROTEICalphabet);
//                    model_inited = true;
//                } if (modelstates.getName() == "JTT92" && !model_inited) {
//                    model = new bpp::JTT92(PROTEICalphabet);
//                    model_inited = true;
//                } if (modelstates.getName() == "WAG01" && !model_inited) {
//                    model = new bpp::WAG01(PROTEICalphabet);
//                    model_inited = true;
//                } else if (!model_inited) {
//                    throw std::logic_error("Can not initialize model");
//                }
            } break;
            case 4:{
                // todo implement logic for models - commented out as no Substitutionmatrix and so on are actually implemented
                throw std::logic_error("For Binary Alphabet are actually no models supported / complete implemented");
//                auto *BINARYalphabet = new bpp::BinaryAlphabet; //bpp::BinaryAlphabet
//                model = new bpp::BinarySubstitutionModel(BINARYalphabet);
//                model_inited = true;
            } break;
            case 5:{
                // todo implement logic for models - commented out as no Substitutionmatrix and so on are actually implemented
                throw std::runtime_error("Codon Models are actually not supported / complete implemented");
//                bpp::CodonAlphabet *CODONalphabet = new bpp::CodonAlphabet(&bpp::AlphabetTools::RNA_ALPHABET);
//                model = new bpp::GY94(*CODONalphabet);
            } break;
            default:
                throw std::logic_error("Can not initialize model - may non given");
        } // end switch

    } catch (bpp::Exception & e) {
        std::cerr << "ERROR! " << e.what() << std::endl;
        std::exit(1);
    } catch(std::exception& e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    } catch (...) {
        std::cerr << "errors occurred while initialising substitiom model" << std::endl;
        std::exit(1);
    } // end try

} // end SubstitutionModel

bpp::Sequence *DATA_COLLECTOR::create_random_ancester_sequence() {

    std::vector<int> sequence;
    sequence.resize(structure.getParsed_lenght(), -1 ); // resize to structural length and fill with illegal alphabet int

    double dummy=0;
    int counter = 0;
    double randomnumber = 0;

    int temp_index = 0;
    int first_letter = 0;
    int second_letter = 0;
    int third_letter = 0;

    //fill sequence with positions according to their nk drawn from discrete distribution
    for (auto position = 0; position < structure.getParsed_lenght(); position++ ){
        randomnumber = randomfactory->drawNumber();
        dummy = 0;
        counter = 0;

        if (sequence[position] == -1 ) {
            if (structure.getNk(position) == 0) {
                do {
                    dummy += modelstates.getPiSingleFrequencies()[counter];
                    temp_index = counter++;
                } while(randomnumber > dummy);
                sequence[position] = temp_index;

            } else if (structure.getNk(position) == 1) {
                do {
                    dummy += modelstates.getPiDoubleFrequencies()[counter];
                    temp_index = counter++;
                } while(randomnumber > dummy);

                first_letter =  (((temp_index+4)/4) -1)%4;
                second_letter = temp_index - (4*first_letter);
                sequence[position] = first_letter;
                sequence[structure.getValue(position)] = second_letter;

            } else if (structure.getNk(position) == 2) {
                do {
                    dummy += modelstates.getPiTripleFrequencies()[counter];
                    temp_index = counter++;
                } while(randomnumber > dummy);

                first_letter =  (((temp_index+16)/16) -1)%4;
                second_letter =  (((temp_index+4)/4) -1)%4;
                third_letter = temp_index - ( (16*first_letter) + (4*second_letter) );
                sequence[position] = first_letter;
                sequence[structure.getValue(position)] = second_letter;
                sequence[structure.getValue(position), 2] = third_letter;
            } else {
                throw std::overflow_error("Ancestral sequence can not be processed with more than 2 neighbours (Nk=2).");
            } // end if else
        } // end if position
    } // end for length

    return new bpp::BasicSequence("RandomAncestralSequence", sequence, alpha );
} // end create_random_ancester_sequence

void DATA_COLLECTOR::init_output_writeable(const std::string &outpath){
    try {
        std::string file_format = outpath.substr(outpath.find_last_of('.') + 1);
        if (file_format.empty()) {
            outputpath.clear(); // write to std::cout
        } else  if (file_format == "fasta" || file_format == "fa" ||
                file_format == "stockholm" || file_format == "ph" ||
                file_format == "aln" || file_format == "clu" ||
                file_format == "clustal" || file_format == "mase" ) {

            //check if path is writeable - open file for output and create it if it doesn't exist
            std::ofstream file(outpath);
            if(!file.is_open()) {
                //error: could not open/create file
                throw std::runtime_error("output-path is not writeable or file can't be generated");
            }
            //useable filepath
            file.close();
            // set path to environment
            outputpath = outpath;
        } else {
            throw std::runtime_error("Where is no valid writer for given fileformat " + file_format + ".");
        }

    } catch(std::exception& e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    } catch (...) {
        std::cerr << "errors occured while initializing output" << std::endl;
        std::exit(1);
    } // end try
}; // end init_output

void DATA_COLLECTOR::set_model_parameter_in_modelstates(){
    if (modelstates.getName() == "JC69"){
        //do nothing
    } else if (modelstates.getName() == "K80"){
        modelstates.setKappa( model->getParameter("kappa").getValue() );
    } else if (modelstates.getName() == "F81"){
        modelstates.setKappa( model->getParameter("kappa").getValue() );
    } else if (modelstates.getName() == "HKY85") {
        modelstates.setKappa( model->getParameter("kappa").getValue() );
    } else if (modelstates.getName() == "T92") {
        modelstates.setKappa( model->getParameter("kappa").getValue() );
    } else if (modelstates.getName() == "TN93") {
        modelstates.setKappa1( model->getParameter("kappa1").getValue() );
        modelstates.setKappa2( model->getParameter("kappa2").getValue() );
    } else if (modelstates.getName() == "GTR") {
        modelstates.setGTRa( model->getParameter("a").getValue() );
        modelstates.setGTRb( model->getParameter("b").getValue() );
        modelstates.setGTRc( model->getParameter("c").getValue() );
        modelstates.setGTRd( model->getParameter("d").getValue() );
        modelstates.setGTRe( model->getParameter("e").getValue() );
    }
}; //end set_model_parameter_in_modelstates

void DATA_COLLECTOR::write_output() {
    std::string file_format = outputpath.substr(outputpath.find_last_of('.') + 1);

    if (file_format == "fasta" || file_format == "fa") {
        bpp::Fasta fastawriter;
        fastawriter.writeAlignment(outputpath, *simulatedsites);
    } else if (file_format == "stockholm") {
        bpp::Stockholm writer;
        writer.writeAlignment(outputpath, *simulatedsites);
    } else if (file_format == "ph") {
        bpp::Phylip writer(true, true);
        writer.writeAlignment(outputpath, *simulatedsites, true);
    } else if (file_format == "aln" || file_format == "clu" || file_format == "clustal") {
        bpp::Clustal writer;
        writer.writeAlignment(outputpath, *simulatedsites);
    } else if (file_format == "mase") {
        bpp::Mase writer;
        writer.writeAlignment(outputpath, *simulatedsites);
    } else if ( outputpath.empty() ) {
        for (size_t index = 0; index < simulatedsites->getNumberOfSequences(); index++ ) {
            std::cout << simulatedsites->getName(index) << '\t'
                      << simulatedsites->getSequence(index).toString() << std::endl;
        }
    }
}; // end write_output

