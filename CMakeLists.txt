# adopted project-template

################################### BASIC ####################################
# This is a basic configuration for users with Bio++ libraries installed in a standard system place (/usr/local/).
cmake_minimum_required(VERSION 3.0)
project(SISSI_2_0)

#set used CXX standard to Cpp11
set (CMAKE_CXX_FLAGS "-std=c++11")
#set(CMAKE_CXX_FLAGS "-lboost_program_options")               #may be needed for program_options

set(Boost_USE_STATIC_LIBS   ON)                              #SETS BOOST STATIC...

#set include directories for LINKER
#include_directories (${Boost_INCLUDE_DIRS})                  #may be needed for program_options
include_directories ( /usr/local/include/ViennaRNA )

# Find Bio++ libraries and import their configurations.
find_package (bpp-core)
find_package (bpp-seq)
find_package (bpp-phyl)
#find boosts program_options
find_package (Boost COMPONENTS program_options REQUIRED)


set(SOURCE_FILES
        main.cpp
        SRC/Required_Bio++_Headers.h
        SRC/data_collector.cpp SRC/data_collector.h
        SRC/commandline_parser.cpp SRC/commandline_parser.h
        SRC/arguments.h
        SRC/structure.h
        SRC/file_parser/dot_bracket_reader.cpp SRC/file_parser/dot_bracket_reader.h
        SRC/file_parser/ct_reader.cpp SRC/file_parser/ct_reader.h
        SRC/file_parser/nei_cnei_reader.cpp SRC/file_parser/nei_cnei_reader.h
        SRC/file_parser/stockholm_reader.cpp SRC/file_parser/stockholm_reader.h
        SRC/models/estimation_reconstruction.cpp SRC/models/estimation_reconstruction.h
        SRC/numeric/matrix.h
        SRC/simulate.cpp SRC/simulate.h SRC/models/modelstates.h
        SRC/models/substitutionmatrices.cpp SRC/models/substitutionmatrices.h
        SRC/numeric/rng_jkiss.cpp SRC/numeric/rng_jkiss.h
        SRC/numeric/abstract_prng.h
        SRC/numeric/rng_mt19337_64.h
        )

add_executable(SISSI_2_0 ${SOURCE_FILES})

# Link with the shared libraries of Bio++
target_link_libraries (SISSI_2_0 ${Boost_LIBRARIES})        #BOOST PO Library


#target_link_libraries (SISSI_2_0 ${BPP_LIBS_SHARED})        #Bio++ must be installed local
target_link_libraries (SISSI_2_0 ${BPP_LIBS_STATIC})       #Bio++ must is linked in the build so no local installation needed

# You can then compile your program with:
# $ cmake .
# $ make



################################# ADVANCED ###################################
# Here we give some more advanced details

### Out of tree advice:
# CMake supports what is called "out of tree build":
# $ mkdir build
# $ cd build
# $ cmake <options> ..
# $ make
# In this form, you can easily delete all CMake stuff and generated files by deleting the build/ dir.
# It can be useful if you want to be sure to recompile from a clean state.

### find_package(bpp-<module>):
#
# If Bio++ is installed in a non standard path, CMake MUST be told to look there for it to find the Bio++ libraries.
# This can be done with: $ cmake -DCMAKE_PREFIX_PATH=<bpp-location> .
#
# find_package(bpp-<module>) defines, if successful:
# - a bpp-<module>-static symbol to link with the static library
# - a bpp-<module>-shared symbol to link with the shared library
# - variables BPP_LIBS_SHARED and BPP_LIBS_STATIC that includes the related symbols and symbols of Bio++ dependencies.
# The bpp-<module>-{static/shared} symbols also force CMake to link to their Bio++ dependencies.
# find_package of a a module also does a find_package of dependency Bio++ modules.
#
# So, for bpp-phyl (which depends on bpp-seq and bpp-core):
#find_package (bpp-phyl)
# Is equivalent to:
#find_package (bpp-core)
#find_package (bpp-seq)
#find_package (bpp-phyl)
# And the following target_link_libraries commands are equivalent:
#target_link_libraries (myprogram bpp-phyl-shared)
#target_link_libraries (myprogram bpp-core-shared bpp-seq-shared bpp-phyl-shared)
#target_link_libraries (myprogram ${BPP_LIBS_SHARED})
# The short form relies on Bio++ inter module dependencies, but they are unlikely to change.

### Static libraries can be linked by using either:
#target_link_libraries (myprogram bpp-phyl-static)
#target_link_libraries (myprogram ${BPP_LIBS_STATIC})
# They make your binary program self contained (all Bio++ code is included with it).
# It can be given to people without having them install Bio++.
# However, the binary will be huge (10 to 100MB depending of which modules are used).
# Using `$ strip myprogram` can reduce the binary size.

### Shared libraries, RPATH and LD_LIBRARY_PATH:
# If you installed Bio++ to a NON STANDARD path and use the shared libraries, you might run into problems like:
# ./myprog: error while loading shared libraries: libbpp-seq.so.9: cannot open shared object file: No such file or directory
#
# The problem is that your executable must find the Bio++ shared library before starting.
# Libraries are normally searched in standard paths (/usr/lib).
# Two options exist to add a custom search path (to you Bio++ custom install path):
# - adding the path to the LD_LIBRARY_PATH environment variable.
# - adding the path as an RPATH entry in the executable 'myprog' itself.
#
# LD_LIBRARY_PATH:
# - does not require modifying the executable.
# - the executable can be used everywhere as long as the local Bio++ libs are in /usr/lib or in LD_LIBRARY_PATH.
# - LD_LIBRARY_PATH paths are used by all program to search for their libs (i.e. other commands in the shell).
#
# RPATH:
# - the executable runs without having to tweak LD_LIBRARY_PATH or anything else.
# - the executable includes hard-coded paths, so it CANNOT BE PUBLISHED as the paths are local to your system.
# - by default, the executable in the CMake build directory (after 'make') uses RPATH.
# - by default, "installed" executables (after 'make install') have no RPATH.
# - You can force the use of RPATH in installed executables with "-DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE"
# - if using RPATHs, consider compiling Bio++ with them by adding "-DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE" during installation.
# - if you need an installed executable (to publish it without RPATH), add an install command:
#install (TARGETS myprogram RUNTIME DESTINATION bin)
# And run `$ make install` to install it.
# You can select the location at the configuration step with -DCMAKE_INSTALL_PREFIX=<path>
# Or at install step with `$ make DESTDIR=<prefix> install`
