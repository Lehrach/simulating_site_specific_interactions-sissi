/*
 * * From StdLib:
 * */
#include <iostream>     //std in and out library
#include <chrono>       // used for stopping runtime
#include <iterator>

/*
 * * From BOOST:
 * */
#include <boost/program_options.hpp>

/*
 * * From ViennaRNA Package:
 * */
//#include <ViennaRNA/data_structures.h>

/*
 * * From Project_SRC:
 * */
#include "SRC/Required_Bio++_Headers.h"     // contains all includes needed from Bio++ Library
#include "SRC/commandline_parser.h"
#include "SRC/arguments.h"
#include "SRC/data_collector.h"
#include "SRC/simulate.h"

/******************************************************************************/
namespace po = boost::program_options;
/******************************************************************************/
int main(int argc, char* argv[]) {
    auto start = std::chrono::steady_clock::now(); // remember start time
/******************************************************************************/
    try {
        ARGUMENTS argue_container; // init container with all parsed arguments
        parse_arguements(argc, argv, argue_container); // defined in commandline_parser
        argue_container.check_all();

        DATA_COLLECTOR collector;
        collector.init_variablestates(argue_container);
        SIMULATE simulate(&collector);
        simulate.check_matter_of_estimiation();
        simulate.init_simulation();
        simulate.simulate_all();
        collector.write_output();

        /*
         * Calculate runtime resulting from start and end time - print out to stdout
         * in milliseconds - a option for seconds is prepared but comment out
         * */
        auto end = std::chrono::steady_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start); //milliseconds
        //auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(end - start); //seconds
        if (collector.isPrintTime()) {
            std::cout << "DONE! in :" << elapsed.count() << "milliseconds" << std::endl;
        }


    } catch (bpp::Exception & e) {
        std::cerr << "ERROR! " << e.what() << std::endl;
        std::exit(1);
    } catch(std::exception &e) {
        std::cerr << e.what() << std::endl;
        std::exit(1);
    } catch (...) {
        std::cerr << "I'm SORRY but undefined errors occurred while running" << std::endl;
        std::exit(1);
    } // end try


    return 0;
} // end main


/* TODO in general
 * TODO add pathway of random anc_seq from alignment
 * TODO wann kann welches object zertoert werden? delete it after use save Memory!!!
 * stockholm format for tree and seqeunce (add option that stockholm can be parsed by this and that option)
 *
 * TODO check Theta in dinukl model (T92)
 *
 * TODO Known BUGS
 * */