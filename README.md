#Simulate Site Specific Interactions (SISSI)

##Installation
This software needs cmake >= 2.8.11 and a C++11 capable compiler to build

After installing cmake, run it with the following command:
```commandline
cmake -DCMAKE_INSTALL_PREFIX=[where to install, for instance /usr/local or $HOME/.local] .
```
If available, you can also use ccmake instead of cmake for a more user-friendly interface.

Then compile and install the software with:
```commandline
make install
```

###cmake building system
CMake is cross-platform free and open-source software for managing the build process of software using a 
compiler-independent method. It is used in conjunction with native build environments and has has minimal dependencies, 
requiring only a C++ compiler on its own build system. The installation is as easy as possible for each OS.

App website: http://www.cmake.org/

####Linux
Cmake consists of 
-  cmake
-  cmake-qt-gui
both can be installed using apt-get:
```commandline
sudo apt-get install cmake cmake-qt-gui 
```

####Mac
On Mac it is as simple as for linux systems, and cmake is installed using homebrew environment entering following lines 
into the terminal
```commandline
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null
```
and press enter/return key. Wait for the command to finish.
Run:
```commandline
brew install cmake
```
Done! You can now use cmake.

On the homepage of cmake is also a .dmg file including a GUI interface available

####Windows

For Windows systems simply go to the app homepage and download the .msi for windows. After installing cmake Gui can be 
started and used.

##Depencies

####Bio++ - suite